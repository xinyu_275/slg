export default class EventManager extends Laya.EventDispatcher {
    private static _instance:EventManager

    public static get Instance():EventManager {
        if(!this._instance) {
            this._instance = new EventManager();
        }
        return this._instance;
    }
}

