export default class Util {
    /**
     * 获取2个值的最大值
     * @param a number
     * @param b number
     */
    public static max(a, b: number): number {
        if (a >= b) return a;
        return b;
    }

    /**
    * 获取2个值的最小值
    * @param a number
    * @param b number
    */
    public static min(a, b: number): number {
        if (a <= b) return a;
        return b;
    }

    /**
    * 判断2个坐标位置是否相等
    */
    public static checkPointEqual(a: Laya.Point, b: Laya.Point): boolean {
        return a.x === b.x && a.y === b.y;
    }

    /**
    * 获取两点之间的距离
    * @param point1        第一个坐标点
    * @param point2        第二个坐标点
    * @param distance      两点之间的距离
    */
    public static getDistance(p1: Laya.Point, p2: Laya.Point): number {
        let dx: number = p1.x - p2.x;
        let dy: number = p1.y - p2.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    /**
     * 获取从第一个位置作为起始点到第二个位置形成的角度
     * @param point1        起始点坐标
     * @param point2        终点坐标
     * @param angle         角度
     */
    public static getAngle(point1: Laya.Point, point2: Laya.Point): number {
        return Math.atan2(point2.y - point1.y, point2.x - point1.x) * (180 / Math.PI);
    }

    //==========================字符串工具函数============================
    /**
    * 判断一个字符串是否包含中文
    * @param {string} str
    * @returns {boolean}
    */
    public static isChinese(str: string): boolean {
        let reg: RegExp = /^.*[\u4E00-\u9FA5]+.*$/;
        return reg.test(str);
    }

    /**
   * 字符串转butes
   * @param str 
   */
    public static string2Byte(str: string): Uint8Array {
        let by = new Laya.Byte();
        by.writeUTFBytes(str);
        return new Uint8Array(by.buffer);
    }

    /**
  * uint8Array转化为string
  */
    public static byteToString(array) {
        if(!array || array.length === 0) {
            return "";
        }
        var out, i, len, c;
        var char2, char3;

        out = "";
        len = array.length;
        i = 0;
        while (i < len) {
            c = array[i++];
            switch (c >> 4) {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                    // 0xxxxxxx
                    out += String.fromCharCode(c);
                    break;
                case 12: case 13:
                    // 110x xxxx   10xx xxxx
                    char2 = array[i++];
                    out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                    break;
                case 14:
                    // 1110 xxxx  10xx xxxx  10xx xxxx
                    char2 = array[i++];
                    char3 = array[i++];
                    out += String.fromCharCode(((c & 0x0F) << 12) |
                        ((char2 & 0x3F) << 6) |
                        ((char3 & 0x3F) << 0));
                    break;
            }
        }
        return out;
    }


    /**
     * 获取字符串长度，中文为2
     * @param {string} str
     * @returns {number}
     */
    public static getLength(str: string): number {
        let char: string;
        let length: number = 0;
        for (let i = 0; i < str.length; i++) {
            char = str[i];
            if (this.isChinese(char)) {
                length += 2;
            } else {
                length++;
            }
        }
        return length;
    }

    /**
     * 截取字符串(中文为2 其他为1)
     * @param str 
     */
    public static cutOutString(str: string, cutOutNum: number): string {
        if (this.getLength(str) <= cutOutNum) {
            return str;
        }
        let newStr: string = "";
        let length: number = 0;
        let char: string;
        for (let i = 0; i < str.length; i++) {
            char = str[i];
            if (this.isChinese(char)) {
                length += 2;
            } else {
                length++;
            }
            newStr += char;
            if (length >= cutOutNum) {
                break;
            }
        }
        return newStr;
    }

    //=========================随机数================================
    /**
    * 返回一个给定的区间[min, max)内的伪随机数
    * @param a 区间的最小值
    * @param b 区间的最大值
    */
    public static random(min: number, max: number): number {
        if (min >= max) {
            console.error("Random  区间不合法，min必须小于max");
            return 0;
        }
        let num: number = max - min;
        //Math.random():返回介于 0（包含） ~ 1（不包含） 之间的一个随机数：
        return Math.random() * num + min;
    }

    /**
     * 返回一个给定的区间[min, max]内的随机整数
     * @param a 区间的最小值(整数)
     * @param b 区间的最大值（整数）
     */
    public static randomInt(min: number, max: number): number {
        if (min > max) {
            console.error("Random  区间不合法，min必须小于max");
            return 0;
        } else if (min === max) {
            return min;
        }
        return this.random(min, max + 1) | 0;
    }

    /**
     * 返回一个给定的区间[min, max]内的随机整数
     */
    /**
     * 获取数组中随机一个单元
     * @param arr 数组数据源
     */
    public static randomArray(arr: Array<any>): any {
        const index: number = this.random(0, arr.length) | 0;
        return arr[index];
    }


    //==========================时间工具函数============================
    /**
 * 格式化时间获取：时分秒 00:00:00
 * @param {number} 时间戳差值（s）
 */
    public static formatDiffTime(difftime: number): string {
        let str: string = "";
        let h: number = difftime / 3600;
        h = parseInt(h + "");
        let m: number = (difftime - h * 3600) / 60;
        m = parseInt(m + "");
        let s: number = difftime - h * 3600 - m * 60;
        s = parseInt(s + "");
        if (h > 9) {
            str += h + ":";
        } else {
            str += "0" + h + ":";
        }
        if (m > 9) {
            str += m + ":";
        }
        else {
            str += "0" + m + ":";
        }
        if (s > 9) {
            str += s + "";
        }
        else {
            str += "0" + s;
        }
        return str;
    }

    /**
     * 转化时间戳为日期
     * @param {timestamp} 时间戳(s)
     * @return  yyyy年MM月dd日(例如2019年5月1日)
     */
    public static secondToDate(timestamp: number) {
        const d: Date = new Date(timestamp * 1000);
        let year: number = d.getFullYear();
        let month = (d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1);
        var day = (d.getDate() < 10 ? '0' + (d.getDate()) : d.getDate());
        return year + "年" + month + "月" + d.getDate() + "日";
    }

    /**
    * 转化时间戳为时间
    * @param {timestamp} 时间戳(s)
    * @param withYear: 是否需要年
    * @return  yyyy年MM月dd日 H:i:s(例如(2019年5月1日 12:00:00 or 5月1日 12:00:00)
    */
    public static secondToDateTime(timestamp: number, withYear:boolean = true) {
        const d: Date = new Date(timestamp * 1000);
        let year: number = d.getFullYear();
        let month = (d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1);
        var day = (d.getDate() < 10 ? '0' + (d.getDate()) : d.getDate());

        var hour = (d.getHours() < 10 ? '0' + (d.getHours()) : d.getHours());
        var min = (d.getMinutes() < 10 ? '0' + (d.getMinutes()) : d.getMinutes());
        var sec = (d.getSeconds() < 10 ? '0' + (d.getSeconds()) : d.getSeconds());

        let date = withYear?year + "年":"" +  month + "月" + d.getDate() + "日";
        return date + " " + hour + ":" + min + ":" + sec;
    }

    //==========================数组工具函数============================
    /**
   * 数组交集(多个数组中的共同元素的一个数组)
   * @param a 数组a
   * @param b 数组b
   */
    public static intersection(a, b: any[]): any[] {
        let result: any[] = [];
        for (let e of a) {
            if (b.indexOf(e) >= 0) {
                result.push(e);
            }
        }
        return result;
    }

    /**
     * 数组的并集(ArrayUtil:union([1, 2], [2, 3]) == [1, 2, 3])
     * @param a 数组a
     * @param b 数组b
     */
    public static union(...b: any[][]): any[] {
        let result: any[] = [];
        for (let arr of b) {
            for (let e of arr) {
                if (result.indexOf(e) < 0) {
                    result.push(e);
                }
            }
        }
        return result;
    }

    /**
     * 数组差集(由数组A中所有不属于数组B的元素所组成的一个数组。)
     * @param a 数组a
     * @param b 数组b
     */
    public static difference(a, b: any[]): any[] {
        let result: any[] = [];
        for (let e of a) {
            if (b.indexOf(e) < 0) {
                result.push(e);
            }
        }
        return result;
    }
}