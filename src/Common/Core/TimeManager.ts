import IRefresh from "./RefreshManager";

export default class TimeManager implements IRefresh {
    // 单例
    private static _instance:TimeManager = null;

    // 服务器时间（ms）
    private _serverMillTime:number;
    // 服务器时间（s）
    private _serverTime:number;
    //服务器和客户端时间差（ms）
    private _diffMillTime:number;
    //是否同步过时间
    private _isSyncTime:boolean;

    public static get Instance():TimeManager {
        if(!this._instance) {
            this._instance = new TimeManager();
        }
        return this._instance;
    }

    /**
     * 初始化服务器时间（ms）
     * param time 服务器是时间（ms）
     */
    initServerTime(time:number):void {
        this._serverMillTime = time;
        this._serverTime = (this._serverMillTime / 1000) | 0;
        this._diffMillTime = this._serverMillTime - this.localMillTime;
        this._isSyncTime = true;
    }

    /**
     * 更新时间
     * @param dt 每帧时间间隔
     */
    public refresh(dt:number): void {
        if(!this._isSyncTime) return;
        this._serverMillTime = this.localMillTime + this._diffMillTime;
        this._serverTime = (this._serverMillTime / 1000) | 0;
    }

    /**
     * 获取服务器时间(s)
     */
    public get serverTime():number {
        if (this._serverTime === void 0) {
            console.warn("服务器时间还未同步，请先同步时间！");
            return null;
        }
        return this._serverTime
    }

       /**
     * 获取服务器毫秒时间(ms) 
     */
    public get serverMillTime():number {
        if(this._serverMillTime === void 0){
            console.warn("服务器时间还未同步，请先同步时间！");
            return null;
        }
        return this._serverMillTime;
    }

    /**
     * 获取本地时间（s）
     */
    public get localTime(): number {
        const time:number = (Date.now() / 1000) | 0;
        return time;
    }

    /**
     * 获取本地毫秒时间(ms)
     */
    public get localMillTime():number {
        return Date.now();
    }

    /**
     * 获取服务器下一个点整点时间戳(例如：hour = 5，如果当前hour<5,那就是今天5点，如果hour>=5,那就是明天5点)
     */
    public getServerNextHourTime(hour:number = 5):number {
        const d: Date = new Date(this.serverMillTime);
        let year: number = d.getFullYear();
        let month = d.getMonth() + 1;
        let day = d.getDate();
        let nowHour:number = d.getHours();
        let date = new Date("" + year + "-" + month + "-" + day + " 00:00:00");
        if(nowHour < hour) {
            return ((date.getTime() / 1000)|0) + 5 * 3600;
        }else {
            return ((date.getTime() / 1000)|0) + 86400 + 5 * 3600;
        }
    }
}