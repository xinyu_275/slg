//继承本类可以实现单例控制
export default class Singleton {
    // 单例变量
    private static _instance:Singleton = null;

    public static getInstance<T extends Singleton>():T {
        if(!this._instance) {
            this._instance = new this();
        }
        return this._instance as T;
    }
}