export default interface IRefresh{
    refresh(interval:number):void;
}
class ModRefresh {
    refresh:IRefresh;
    lastExecFrame:number;
    refreshFrameInterval: number;
    delta:number;
}
export class RefreshManager {
    private refresher:ModRefresh[] = [];

    // 单例
    private static _instance:RefreshManager = null;

    public static get Instance():RefreshManager {
        if(!this._instance) {
            this._instance = new RefreshManager()
        }
        return this._instance;
    }

    constructor(){
        // 每帧循环
        Laya.timer.frameLoop(1, this, this.refresh);
    }

    /**
     * 添加帧循环处理
     */
    add(refresh:IRefresh, refreshFrameInterval:number = 1) {
        this.remove(refresh);
        let modRefresh:ModRefresh = {
            refresh:refresh, 
            lastExecFrame:Laya.timer.currFrame, 
            refreshFrameInterval:refreshFrameInterval, 
            delta:0, 
        }
        this.refresher.push(modRefresh)
    }

    /**
     * 删除
     */
    remove(refresh:IRefresh):void {
        for(let index in this.refresher) {
            if (this.refresher[index].refresh === refresh) {
                this.refresher.splice(Number(index), 1);
                break;
            }
        }
    }

    /**
     * 帧执行
     */
    refresh():void {
        let curFrame = Laya.timer.currFrame;
        let delta = Laya.timer.delta;
        for(let m of this.refresher) {
            m.delta += delta;
            if(curFrame - m.lastExecFrame >= m.refreshFrameInterval) {
                m.refresh.refresh(m.delta);
                m.lastExecFrame = curFrame;
                m.delta = 0;
            }
           
        }
    }
}