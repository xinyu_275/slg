export default class TiledUtil {
    /**
     * 计算Laya.event 2点间的距离s
     * @param points 2个位置点
     */
    public static getEventDistance2(points: Array<any>): number {
        let distance: number = 0;
        if (points && points.length == 2) {
            let dx: number = points[0].stageX - points[1].stageX;
            let dy: number = points[0].stageY - points[1].stageY;
            distance = Math.sqrt(dx * dx + dy * dy);
        }
        return distance;
    }

    /**
     * 根据传入的地图格子坐标获取游戏中的逻辑格子坐标
     * @param gx  地图的格子坐标(或者格子x)
     * @returns Point     游戏中的逻辑格子坐标
     */
    public static getLogicPosByMapGridPos(gx: Laya.Point | number, gy?: number): Laya.Point {
        if (typeof gx !== "number") {
            gy = gx.y;
            gx = gx.x;
        }
        if (gy % 2 == 0) {
            gx = 2 * gx;
        } else {
            gx = 2 * gx + 1;
        }
        return new Laya.Point(gx, gy);
    }

    /**
     * 根据传入的逻辑格子坐标获取地图的格子坐标
     * @param lx      逻辑的格子坐标（或者坐标x）
     * @param ly     地图的格子坐标
     */
    public static getMapGridPosByLogicPos(lx: Laya.Point | number, ly?: number): Laya.Point {
        if (typeof lx !== "number") {
            ly = lx.y;
            lx = lx.x;
        }
        if (ly % 2 == 0) {
            lx = Math.floor(lx / 2);
        } else {
            lx = Math.floor((lx - 1) / 2);
        }
        return new Laya.Point(lx, ly);
    }

}