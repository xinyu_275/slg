import Common from "../Config/Common";

export enum ELogType {
    DEBUG, 
    INFO, 
    WARN, 
    ERROR, 
}
export default class LogM {
    //调试模式下，都需要显示，否则只显示Info及以上的日志等级
    private static _defaultLogType =  ELogType.INFO;

    public static set defaultLogType(logType:ELogType) {
        this._defaultLogType = logType;
    }

    /**
     * 调试日志
     * @param messgge 要输出的内容
     * @param params:可选参数
     */
    public static debug(messgge:any, ...params:any[]):void {
        this.doLog(ELogType.DEBUG, messgge, ...params);
    }

    /**
     * 提示日志
     * @param messgge 要输出的内容
     * @param params:可选参数
     */
    public static info(messgge:any, ...params:any[]):void {
        this.doLog(ELogType.INFO, messgge, ...params);
    }

    /**
     * 警告日志
     * @param messgge 要输出的内容
     * @param params:可选参数
     */
    public static warn(messgge:any, ...params:any[]):void {
        this.doLog(ELogType.WARN, messgge, ...params);
    }

     /**
     * 错误日志
     * @param messgge 要输出的内容
     * @param params:可选参数
     */
    public static error(messgge:any, ...params:any[]):void {
        this.doLog(ELogType.ERROR, messgge, ...params);
    }

    //显示日志
    private static doLog(logType: ELogType, message: any, ...params: any[]): void {
        // 非debug模式下，只显示 >= defaultLogType的日志
         if (!Common.DebugMode && logType <= this._defaultLogType) {
             return;
         }
         switch (logType) {
             case ELogType.DEBUG:
                 console.info(message, ...params);
                 break;
             case ELogType.INFO:
                 console.log(message, ...params);
                 break;
             case ELogType.WARN:
                 console.warn(message, ...params);
                 break;
             case ELogType.ERROR:
                 console.error(message, ...params);
                 break;
         }
     }

}