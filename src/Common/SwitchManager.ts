import LogM from "./LogM";
import Singleton from "./Core/Singleton";

//场景切换显示
export default class SwitchManager extends Singleton {
    private handler: Laya.Handler = null;
    private sk: Laya.Skeleton;
    private skPath: string = "res/yunwu/a_yunwu.sk";
    private templet: Laya.Templet = null;
    private skIndex: number = 0;

    public static get Instance(): SwitchManager {
        return SwitchManager.getInstance<SwitchManager>();
    }

    /**
     * 打开动画
     */
    openView(handler: Laya.Handler = null): void {
        this.handler = handler;
        if (!this.templet) {
            let templet = new Laya.Templet();
            templet.on(Laya.Event.COMPLETE, this, this.completeTemplet,
                [templet]);
            templet.loadAni(this.skPath);
        } else {
            this.createSK();
        }
    }

    //关闭
    closeView(): void {
        LogM.debug("云雾散开--------------------->1");
        if (!this.sk) {
            return;
        }
        LogM.debug("云雾散开--------------------->2");
        //云雾散开
        this.sk.on(Laya.Event.STOPPED, this, this.completePlayerHandler1);
        this.sk.play("open", false);
    }
    private completePlayerHandler1(): void {
        this.clearSK();
    }

    private completeTemplet(templet: Laya.Templet): void {
        this.templet = templet;
        this.createSK();
    }
    private createSK(): void {
        this.clearSK();
        //先把以前的destroy
        this.sk = this.templet.buildArmature(0);
        Laya.stage.addChild(this.sk);
        this.sk.zOrder = 1001;
        this.sk.pos(Laya.stage.width / 2, Laya.stage.height / 2);
        //云雾合拢
        this.sk.on(Laya.Event.STOPPED, this, this.completePlayerHandler);
        this.sk.play("close", false);
        //云雾散开前拦截点击事件
        Laya.stage.on(Laya.Event.CLICK, this, this._stopSwitchClick);
        //需要加载背景图片，说明这个场景资源加载会比较长时间，因此要先显示背景图片出来，下一帧才继续加载场景资源
        //防止阻塞导致背景图片显示不出来
        //Laya.timer.frameOnce(1, this, this.runHandler);
    }

    private _stopSwitchClick(e: Laya.Event) {
        e.stopPropagation();
        // Log.debug("舞台事件点击=====================================");
    }

    private completePlayerHandler(): void {
        this.runHandler();
    }
    private runHandler() {
        if (this.handler) {
            this.handler.run();
            this.handler = null;
        }
    }

    public clearSK(): void {
        //销毁以前的sk
        if (this.sk) {
            this.sk.removeSelf();
            this.sk.destroy(true);
        }
        this.sk = null;
        //云雾散开前拦截点击事件
        Laya.stage.off(Laya.Event.CLICK, this, this._stopSwitchClick);
    }

}