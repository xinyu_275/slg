import Singleton from "./Core/Singleton";
import TimeManager from "./Core/TimeManager";
import EventManager from "./Core/EventManager";
import { EEventType, ETipType } from "../Config/EnumConfig";
import TipManager from "../Module/Tip/TipManager";
import LogM from "./LogM";

class MsgCaller {
    public callBack: Function;
    public caller: any;
    constructor(caller, fun) {
        this.callBack = fun;
        this.caller = caller;
    }
}
export default class NetSocketManager extends Singleton{

    private webSocket: WebSocket;                               // websocket连接
    private MsgCallBackDic: { [key: number]: MsgCaller[] } = {};     // 注册回调映射
    private MsgCallerDic: { [key: number]: Function[] } = {};        // 注册回调映射

    public NetConnect: Boolean;
    private RecvByte: Laya.Byte;     // 读取协议字节流
    private SendByte: Laya.Byte;     //接收协议字节流

    private serverip: string;        // 缓存服务器id
    private RecvID: number;          //接收协议ID
    private NetHeartNum: number = 0;
    public Ping: number = 0;
    private PingTest: number;
    private InitNet: boolean = false;
    public reconnect = false;
    private check = false;

    public static get Instance(): NetSocketManager {
        return  NetSocketManager.getInstance<NetSocketManager>();
    }

    constructor() {
        super();
        this.RecvByte = new Laya.Byte();
        this.RecvByte.endian = "LITTLE_ENDIAN";

        this.SendByte = new Laya.Byte();
        this.SendByte.endian = "LITTLE_ENDIAN";
    }

    /**
     * 初始化服务器和协议
     */
    Init(server: string, proto?: string): void {
        // console.log("NetSocketManager Init");
        LogM.info("NetSocketManager:"+server)
        this.serverip = server;
        //注册心跳包
        this.RegisterNet(awesomepackage.serviceNo.s_heartbeat, this, this.s_heartbeat);
        this.connect();
        //每2秒心跳执行
        Laya.timer.loop(2000, this, this.NetHeart);
    }

    //连接服务器
    public connect(): void {
        // console.log("begin connect:", this.serverip);
        //面板提示 
        this.setTips("正在获取服务器信息...");
        this.webSocket = new WebSocket(this.serverip);
        this.webSocket.binaryType = "arraybuffer";

        //this.webSocket.setTimeout
        this.webSocket.onopen = () => { this.onSocketOpen(); };
        this.webSocket.onclose = () => { this.onSocketClose(); };
        this.webSocket.onmessage = (obj) => { this.onMessageReveived(obj); };
        this.webSocket.onerror = (err) => { this.onConnectError(err); };
    }
    //关闭socket
    public close():void
    {
        if (this.webSocket == null) {
            return;
        }
        this.webSocket.close();
        this.webSocket = null;
        this.NetConnect = false;
        this.reconnect = false;
    }
    sleep(delay) {
        var start = (new Date()).getTime();
        while ((new Date()).getTime() - start < delay) {
            continue;
        }
    }

    //1.websocket成功建立连接
    private onSocketOpen(): void {
        LogM.info("onSocketOpen");
        // console.log("onSocketOpen");
        this.InitNet = true;
        this.NetConnect = true;
        // if (this.reconnect) {
        //     EventManager.Instance.event(EventType.ReConnectSuccess);
        // }
        this.reconnect = false;
        // //显示提示框
        EventManager.Instance.event(EEventType.OpenSocketSuccess);
        // ApiManager.instance.mKickReason = -1;
    }

    //2.连接关闭
    private onSocketClose(): void {
        LogM.info("onSocketClose");
        this.InitNet = true;
        this.NetConnect = false;
        this.reconnect = false;
        this.setTips("与服务器断开连接...");
    }

    //3.接受到数据
    private onMessageReveived(message: any): void {
        if (message != null) {
            this.Recv(message.data);
        }
    }

    //4.报错
    private onConnectError(e) {
        // console.log("onConnectError", e);
        this.InitNet = true;
        this.NetConnect = false;
        this.reconnect = false;
        //面板提示
        this.setTips("与服务器断开连接...");
    }

    //发送消息
    public SendMsg(name: string, data: any) {
        var MsgData = awesomepackage[name];
        if (MsgData == null) {
            console.warn("NetSocketManager SendMsg:" + name + " not found");
            return;
        }
        this.doSend(awesomepackage.serviceNo[name], MsgData, data);
    }

    private doSend(id: number, msgData: any, data: any) {
        var buf = msgData.encode(data).finish();
        if (this.SendByte != null) {
            this.SendByte.clear();
            this.SendByte.writeInt16(id);
            this.SendByte.writeArrayBuffer(buf, 0, buf.len);

            if (this.NetConnect && this.webSocket != null) {
                this.webSocket.send(this.SendByte.buffer);
            }
        }
    }

    //接受数据
    private Recv(data: any) {
        if (data == null) {
            return;
        }
        this.RecvByte.clear();
        this.RecvByte.writeArrayBuffer(data);
        if (this.RecvByte.length > 0) {
            this.RecvByte.pos = 0;
            this.RecvID = this.RecvByte.getInt16();
            let classType = awesomepackage.serviceNo[this.RecvID];
            if (!classType) {
                return;
            }
            var headData: any;
            headData = awesomepackage[classType].decode(this.RecvByte.getUint8Array(2, this.RecvByte.length));
            if (headData == null) return;

            var call: MsgCaller[] = this.MsgCallBackDic[this.RecvID];
            if (call) {
                for (var i = 0; i < call.length; i++) {
                    call[i].callBack.call(call[i].caller, headData)
                }
            }
        }
    }

    //注册消息回调
    public RegisterNet(id: number, caller: any, handler: Function) {
        var call: MsgCaller[] = this.MsgCallBackDic[id];
        if (!call) {
            call = [new MsgCaller(caller, handler)];
            this.MsgCallBackDic[id] = call;
            return;
        }
        //判断是否已经存在
        for (let index = 0; index < call.length; index++) {
            if (call[index].caller === caller && call[index].callBack === handler) {
                return;
            }
        }
        call[call.length] = new MsgCaller(caller, handler);
    }

    //取消注册消息
    public UnRegisterNet(id: number, handler: Function) {
        var call: MsgCaller[] = this.MsgCallBackDic[id]
        if (call != null) {
            for (var i = call.length - 1; i >= 0; i--) {
                if (call[i].callBack == handler) {
                    call.splice(i, 1);
                }
            }
        }
    }

    //重连
    ReConnect(): void {
        //TODO 如果在加载资源，那不要
        if (this.reconnect) {
            console.warn("正在重连中,不要重复重连...");
            return;
        }
        //提示
        this.setTips("正在重新连接服务器...");
        if (this.webSocket != null) {
            this.webSocket.close();
            this.NetConnect = false;
        }
        if (this.serverip != null && this.serverip != "") {
            // console.log("Start ReConnect " + this.serverip);
            this.connect();
            this.reconnect = true;
        }
    }

    //定时心跳
    NetHeart(): void {
        if (!this.InitNet) return;
        this.NetHeartNum = this.NetHeartNum + 1;
        this.PingTest = Laya.timer.currTimer;
        this.c_heartbeat();
        if (!this.NetConnect || this.NetHeartNum > 2) {
            console.log("NetHeart Reconect, NetHeartNum:" + this.NetHeartNum);
            this.NetHeartNum = 0;
            this.ReConnect();
        }
    }

    //发送心跳报
    c_heartbeat() {
        this.SendMsg("c_heartbeat", {});
    }

    //心跳包返回
    s_heartbeat(data) {
        this.NetHeartNum = this.NetHeartNum - 1;
        //更新服务器时间
        TimeManager.Instance.initServerTime(data.time);
    }

    //处理提示信息
    setTips(message: string) {
        TipManager.Instance.showMsgByType(ETipType.Normal, message);
    }
}


