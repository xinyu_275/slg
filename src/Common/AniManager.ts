import Singleton from "./Core/Singleton";
import { EPoolName } from "../Config/EnumConfig";

export default class AniManager extends Singleton {
    //实例
    public static get Instance(): AniManager {
        return AniManager.getInstance<AniManager>();
    }

    /**实例化Animation */
    newAnimation(loop:boolean = false):Laya.Animation {
        let ani:Laya.Animation = Laya.Pool.getItemByClass(EPoolName.Animation, Laya.Animation);
        ani.loop = loop;
        return ani;
    }

    /**回收 */
    recover(ani:Laya.Animation):void {
        if(!ani) return;
        ani.removeSelf();
        ani.clear();
        Laya.Pool.recover(EPoolName.Animation, ani);
    }
}