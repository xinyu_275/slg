import Singleton from "./Core/Singleton";
import TimeManager from "./Core/TimeManager";

//锁定系统，一般用在判断界面打开中时，不引起二次打开
export default class LockManager extends Singleton {
    public locks: { [key: string]: number } = {};

    //实例
    public static get Instance(): LockManager {
        return LockManager.getInstance<LockManager>();
    }

    /**
     * 判断界面是否锁定
     * @param key 类型
     * @param expireTime 锁定过期时间(ms)
     */
    public isLock(key: string, expireTime: number = 5000): boolean {
        let v = this.locks[key];
        let nowTime = TimeManager.Instance.localMillTime;
        if (!v || nowTime - v > expireTime) {
            this.locks[key] = nowTime;
            return false;
        }
        return true;
    }
}