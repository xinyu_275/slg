import Singleton from "./Core/Singleton";
import LockManager from "./LockManager";
import LogM from "./LogM";
import { SceneConfigMap, ESceneName, EEventType } from "../Config/EnumConfig";
import UI_RewardPopup from "../Module/Lobby/UI_RewardPopup";
import EventManager from "./Core/EventManager";

/**场景管理器 */
export default class SceneManager extends Singleton {
    public scenes: { [key: number]: Laya.Scene } = {};
    //实例
    public static get Instance(): SceneManager {
        return SceneManager.getInstance<SceneManager>();
    }

    /**
     * 通用打开场景(如果实例存在，如果没显示，那显就示 否则需要按照名字查找场景打开)
     * @param sceneName 场景名字名字
     * @param initEventParams 参数,传给EventManger.event(scene, initEventParams...)
     * @param closeOther 是否关闭其他场景，默认为false，【注意】如果设置autoDestroyAtRemoved=true，那就不适应本接口，本接口只适应缓存dialog页面
     * @param complete 打开后调用
     */
    public openScene(sceneName: ESceneName, initEventParams?: any[], closeOther: boolean = false, complete?: laya.utils.Handler) {
        let inst: Laya.Scene = this.scenes[sceneName];
        if (inst) {
            //场景显示出来
            this.showScene(inst, closeOther);
            //执行初始化事件
            EventManager.Instance.event(sceneName, initEventParams);
            complete && complete.runWith(inst);
            return;
        }
        //不能多次打开
        if (LockManager.Instance.isLock(sceneName)) {
            return;
        }
        let sceneConfig = SceneConfigMap[sceneName];
        if (!sceneConfig) {
            LogM.error("openDialog sceneName" + sceneName + "not config");
            return;
        }
        Laya.Scene.open(sceneConfig.sceneUrl, closeOther, null, Laya.Handler.create(this, this.openSceneCallback, [sceneName, initEventParams, complete]))
    }
    public openSceneCallback(sceneName: ESceneName, initEventParams: any[], complete: Laya.Handler, scene: Laya.Scene): void {
        if (scene.autoDestroyAtClosed) {
            LogM.error("openSceneCallback sceneName error, this scene cannot use autoDestroyAtClosed = true, sceneName:", sceneName);
        }
        let sceneConfig = SceneConfigMap[sceneName];
        this.scenes[sceneName] = scene;
        //执行初始化事件
        EventManager.Instance.event(sceneName, initEventParams);
        //执行handler
        complete && complete.runWith(scene);
        //设置dialog title image
        let header = scene.getChildByName("headers");
        if (sceneConfig.titleUrl && header && header.getChildByName("img_title")) {
            (header.getChildByName("img_title") as Laya.Image).skin = sceneConfig.titleUrl;
        } else if (sceneConfig.titleName && header && header.getChildByName("label_title")) {
            (header.getChildByName("label_title") as Laya.Label).text = sceneConfig.titleName;
        }
    }

    //================================打开常用界面============================
    /**
     * 打开奖励弹出框
     * rewardList:奖励列表信息 awesomepackage.Ip_goods_num[] | awesomepackage.Ip_reward[]
     */
    public openRewardPopup(rewardList: awesomepackage.Ip_goods_num[] | awesomepackage.Ip_reward[]) {
        this.openScene(ESceneName.UI_RewardPopup, [rewardList]);
    }

    /**
     * 确认框 | 弹框提示
     * @param title: dialog title
     * @param text: 描述
     * @param handlerYes: 点击确定处理(返回参数：是否本次登录有效)
     * @param type: 0:单个确认  1.有确认 和 取消
     * @param showTip: 是否显示"本次登录不显示"
     * @param handlerNo: 点击取消处理
     * @param yes: 确认按钮label
     * @param no: 取消按钮label
     */
    public openAlert(text: string, title: string = "提示", handlerYes: Laya.Handler = null, type: number = 0,
        showTip: boolean = false, handlerNo: Laya.Handler = null, yes: string = "确认", no: string = "取消"): void {
        let Params = [title, text, handlerYes, type, showTip, handlerNo, yes, no];
        SceneManager.Instance.openScene(ESceneName.UI_Alert, Params);
    }
    // /**
    //  * 通用打开场景(如果实例存在，如果没显示，那显就示 否则需要按照名字查找场景打开)
    //  * @param sceneName 场景名字名字
    //  * @param closeOther 是否关闭其他场景，默认为false，【注意】如果设置autoDestroyAtRemoved=true，那就不适应本接口，本接口只适应缓存dialog页面
    //  * @param param 参数,传给onOpened
    //  * @param complete 打开后调用
    //  */
    // public openScene(sceneName: string, closeOther: boolean = false, param?: any, complete?: laya.utils.Handler) {
    //     let inst: Laya.Scene = this.scenes[sceneName];
    //     if (inst) {
    //         //场景显示出来
    //         this.showScene(inst);
    //         complete && complete.runWith(inst);
    //         return;
    //     }
    //     //不能多次打开
    //     if (LockManager.Instance.isLock(sceneName)) {
    //         return;
    //     }
    //     let sceneConfig = SceneConfigMap[sceneName];
    //     if (!sceneConfig) {
    //         LogM.error("openDialog sceneName" + sceneName + "not config");
    //         return;
    //     }
    //     Laya.Scene.open(sceneConfig.sceneUrl, closeOther, param, Laya.Handler.create(this, this.openSceneCallback, [sceneName, complete]))
    // }
    // public openSceneCallback(sceneName: string, complete: Laya.Handler, scene: Laya.Scene): void {
    //     if (scene.autoDestroyAtClosed) {
    //         LogM.error("openSceneCallback sceneName error, this scene cannot use autoDestroyAtClosed = true, sceneName:", sceneName);
    //     }
    //     let sceneConfig = SceneConfigMap[sceneName];
    //     this.scenes[sceneName] = scene;
    //     complete && complete.runWith(scene);
    //     //设置dialog title image
    //     let header = scene.getChildByName("headers");
    //     if (sceneConfig.titleUrl && header && header.getChildByName("img_title")) {
    //         (header.getChildByName("img_title") as Laya.Image).skin = sceneConfig.titleUrl;
    //     } else if (sceneConfig.titleName && header && header.getChildByName("label_title")) {
    //         (header.getChildByName("label_title") as Laya.Label).text = sceneConfig.titleName;
    //     }
    // }

    //================================私有函数================================
    /**显示场景 */
    private showScene(inst: Laya.Scene, closeOther: boolean = false): void {
        //已经在显示
        if (inst.parent) {
            return;
        }
        if (inst instanceof Laya.Dialog) {
            if (inst.isModal) {
                console.log("=============", inst.isShowEffect);
                inst.popup(closeOther, inst.isShowEffect);
            } else {
                inst.show(closeOther, inst.isShowEffect);
            }
        } else {
            Laya.stage.addChild(inst);
        }
    }
}