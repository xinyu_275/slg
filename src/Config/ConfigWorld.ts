//野外城池类型
export enum EWorldCityType {
    Invalid = -1,       // 无效值
    Empty,              // 空地
    Useless,            // 不可用地
    Main_City_Normal,              // 玩家城（普通）
    Main_City_Appear,              // 玩家城（外观）
    Monster1,               // 野怪1
    Monster2,               // 野怪2
    Monster3,               // 野怪3
    EliteMonster,           // 精英怪1
    _Boss1,                 // Boss1
    _Resources1,            // 资源1
    _Resources2,            // 资源2
    _StaticCity,            // 分区中心城
    GuildLand,              // 联盟领地
    _CenterCity,            // 世界中心城
    RobResources,           //争夺物资 
    LongCheng,              //至尊龙城
    MaxCount,               // 最大值
}

export const MonsterMinLv:number = 5;
export const MonsterMaxLv:number = 70;