//Tip Type
export enum ETipType {
    Normal,     // 普通提示
    Flash,      // 跑马灯效果
}

//物品类型
export enum EGoodsType {
    stren = 7,              // 强化碎片
}

//邮件类型
export enum EMailType {
    SystemMail,     // 系统邮件
    BattleMail,     // 战报
    GuildMail,      // 血盟
}

//事件
export enum EEventType {
    //包含所有的ESceneName，打开界面的时候自动触发

    /** 玩家登录 */
    LoginSuccess = "LoginSuccess",                    // 玩家登录成功
    OpenSocketSuccess = "OpenSocketSuccess",          // socket open
    LoginLoadingProgress = "LoginLoadingProgress",    // 登录加载进度条
    CloseLoginLoadingView = "CloseLoginLoadingView",  // 关闭加载界面

    /**玩家 */
    PlayerResUpdate = "PlayerResUpdate",              // 玩家资源更新
    PlayerPosUpdate = "PlayerPosUpdate",              // 玩家坐标更新

    //大厅
    LobbyRefreshView = "LobbyRefreshView",            // 刷新整个大厅

    //背包
    BagItemListClick = "BagItemListClick",            // UI_BagItemList中的item被选中
    BagItemListUpdate = "BagItemListUpdate",           // 物品列表更新

    //tiledmap
    MapMouseClick = "MapMouseClick",                  // 地图点击事件
    MapMouseDown = "MapMouseDown",                    // 地图按下事件

    //邮件
    MailListUpdate = "MailListUpdate",                // 邮件列表更新
    MailMessageUpdate = "MailMessageUpdate",          // 邮件信息更新(例如：删除打开的邮件，那就要打开下一封邮件)
    MailRewardSuccess = "MailRewardSuccess",          // 邮件领奖成功
    MailRead = "MailRead",                            // 邮件已读
    MailPageUpdate = "MailPageUpdate",                // 邮件翻页
}

//pool名字
export enum EPoolName {
    WorldItem = "WorldItem",                    // class WorldItem
    WorldLine = "WorldLine",                    // class WorldLine
    Animation = "Animation",
}

//动画路径
export enum EAniPath {
    WorldFire = "res/atlas/image/worldMap/fireAni.atlas",       // 失火动画
    WorldProtect = "res/atlas/image/worldMap/protectAni.atlas", // 保护罩
    WorldFight = "res/atlas/image/worldMap/fightAni.atlas",     // 攻击动画
}
//=========================场景配置===========================
export class SceneConfig {
    public sceneUrl?: string = "";      // 场景路径 
    public titleUrl?: string = "";      // 场景标题图片路径
    public titleName?: string = "";      // 标题名字
}
export enum ESceneName {
    //scene
    UI_LoginLoading = "UI_LoginLoading",
    UI_Lobby = "UI_Lobby",

    //dialog
    UI_Tip = "UI_Tip",                  // 提示（普通提示 跑马灯提示）
    UI_Alert = "UI_Alert",              // 弹框提示，确认框
    UI_Bag = "UI_Bag",                  // 背包
    UI_BagDel = "UI_BagDel",            // 背包删除
    UI_GoodsDesc = "UI_GoodsDesc",      // 物品/奖励弹出框描述
    UI_Mail = "UI_Mail",                // 邮件
    UI_MailSystem = "UI_MailSystem",    // 系统邮件详细信息
    UI_RewardPopup = "UI_RewardPopup",  // 奖励弹出框
    UI_General = "UI_General",          // 武将
    UI_Battle = "UI_Battle",            // 战斗
    UI_WorldMonster = "UI_WorldMonster",        // 世界地图点击怪物弹框
    UI_WorldPlayerCity = "UI_WorldPlayerCity",  // 世界地图点击玩家城池
    UI_WorldRobCity = "UI_WorldRobCity",        // 世界地图点击物资
}
export const SceneConfigMap: { [key: string]: SceneConfig } = {
    //场景
    "UI_LoginLoading": { sceneUrl: "Panel/Loading/LoginLoading.scene" },
    "UI_Lobby": { sceneUrl: "Panel/Lobby/Lobby.scene" },

    //对话框
    "UI_Tip": { sceneUrl: "Panel/Tip/Tip.json" },
    "UI_Alert": { sceneUrl: "Panel/Tip/Alert.json" },
    "UI_Bag": { sceneUrl: "Panel/Bag/Bag.json", titleUrl: "image/bag/bag.png" },
    "UI_BagDel": { sceneUrl: "Panel/Bag/BagDel.json", titleName: "删除" },
    "UI_GoodsDesc": { sceneUrl: "Panel/Bag/GoodsDesc.json" },
    "UI_Mail": { sceneUrl: "Panel/Mail/Mail.json", titleUrl: "image/mail/mail.png" },
    "UI_MailSystem": { sceneUrl: "Panel/Mail/MailSystem.json", titleUrl: "image/mail/mail.png" },
    "UI_RewardPopup": { sceneUrl: "Panel/Lobby/RewardPopup.json" },
    "UI_General": { sceneUrl: "Panel/General/General.json", titleUrl: "image/general/general.png" },
    "UI_Battle": { sceneUrl: "Panel/Battle/Battle.json" },
    "UI_WorldMonster": { sceneUrl: "Panel/World/WorldMonster.json" },
    "UI_WorldPlayerCity": { sceneUrl: "Panel/World/WorldPlayerCity.json" },
    "UI_WorldRobCity": { sceneUrl: "Panel/World/WorldRobCity.json" },
};

//独立的prefab，没有嵌入在场景中
export enum EPrefabPath {
    "BoxSoldier" = "Panel/Battle/BoxSoldier.prefab",
    "BattleHp" = "Panel/Battle/BattleHp.prefab",
}