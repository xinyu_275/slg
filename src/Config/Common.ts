// 共有配置
export default class Common {
    //==========================debug begin =========================
    //debug总开关
    public static readonly DebugMode: boolean = true;
    // 是否开启测试帐号输入界面
    private static readonly _openTestAccountPanel = false;

    public static get OpenTestAccountPanel(): boolean {
        return this.DebugMode && this._openTestAccountPanel;
    }
    //========================debug end ==============================

    //========================野外地图=============================
    public static readonly TileWidth: number = 160;                 //格子宽度
    public static readonly TileHeight: number = 116;                //格子高度
    public static readonly WorldTileMapJson: string = "res/worldTiledMap/world.json";
    public static readonly DefaultMapScale: number = 1;
    public static readonly MaxMapScale: number = 2;
    public static readonly MinMapScale: number = 0.5;
    // Map block grid size define.
    public static readonly MapBlockGridWidth: number = 10;
    public static readonly MapBlockGridHeight: number = 10;
}
