//自己方在左边
export enum EBattleSide {
    Left = 1,       // 左边
    Right = 2,      // 左边
}

//结果，以左边为自己判断
export enum EBattleResult {
    Win = 1,       // 胜利
    Fail = 2,      // 失败 
}

const leftBaseX = 545;
const rightBaseX = 276;
const rightBaseY = 307;
export const BattleSidePosMap: { [key: string]: any } = {
    "pos_1_1": { x: 160, y: leftBaseX },
    "pos_1_2": { x: 240, y: leftBaseX + 60 },
    "pos_1_3": { x: 320, y: leftBaseX + 120 },
    "pos_1_4": { x: 80, y: leftBaseX + 60 },
    "pos_1_5": { x: 160, y: leftBaseX + 120 },
    "pos_1_6": { x: 240, y: leftBaseX + 180 },
    "pos_1_7": { x: 0, y: leftBaseX + 120 },
    "pos_1_8": { x: 80, y: leftBaseX + 180 },
    "pos_1_9": { x: 160, y: leftBaseX + 240 },

    "pos_2_1": { x: rightBaseX + 0, y: rightBaseY + 120 },
    "pos_2_2": { x: rightBaseX + 80, y: rightBaseY + 180 },
    "pos_2_3": { x: rightBaseX + 160, y: rightBaseY + 240 },
    "pos_2_4": { x: rightBaseX + 80, y: rightBaseY + 60 },
    "pos_2_5": { x: rightBaseX + 160, y: rightBaseY + 120 },
    "pos_2_6": { x: rightBaseX + 240, y: rightBaseY + 180 },
    "pos_2_7": { x: rightBaseX + 160, y: rightBaseY + 0 },
    "pos_2_8": { x: rightBaseX + 240, y: rightBaseY + 60 },
    "pos_2_9": { x: rightBaseX + 320, y: rightBaseY + 120 },

}
export const BattleAniDuration: number = 100;          // 动画播放时长(100ms)
export const BattleAniDelayTime: number = 1000;        // 每次动画结束后延迟时间