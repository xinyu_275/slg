import Singleton from "../../Common/Core/Singleton";
import ConfigDataManager from "./ConfigDataManager";
export class BaseGoods {
    id:number;
    name:string;
    icon?:string = "";
    quality?:number = 1;    // 品质（1-5:白绿蓝紫橙）
    tab?:number = 0;        // 页签（1-4：普通 士兵 武将 强化）
    type?:number = 1;       // 类型
    subtype?:number = 1;    // 子类型
    des?:string = "";      // 描述
    compose?:number = 0;    // 1.可合成
    use?:number = 0;        // 1.是否可使用
    lv?:number = 1;         // 等级
    attr_template_id?:number = 0;   // 属性模板id
    effect?:number[] = [];  // 道具效果
    exchange?:number = 0;   // 1.是否可交换（血盟交换）
}
export default class GoodsConfigManager extends Singleton{
    public mBaseGoods:{[key:number]:BaseGoods} = {};
    //实例
    public static get Instance(): GoodsConfigManager {
        return GoodsConfigManager.getInstance<GoodsConfigManager>();
    }

    /**解析配置 */
    parse():void {
        //goods.json
        let mgr:ConfigDataManager = ConfigDataManager.Instance;
        this.parseGoods(mgr["goods"]);
        delete mgr["goods"];
    }
    /**解析物品 */
    parseGoods(jsonData:any):void {
        for (let goodsBaseId in jsonData) {
            let intGoodsBaseId = Number(goodsBaseId);
            this.mBaseGoods[intGoodsBaseId] = jsonData[goodsBaseId];
        }
    }

    /**获取baseGoods */
    getBaseGoods(goodsBaseId:number | string):BaseGoods {
        return this.mBaseGoods[goodsBaseId];
    }
    /**获取物品的名字 */
    getGoodsName(goodsBaseId:number | string):string {
        return this.mBaseGoods[goodsBaseId].name;
    }
    /**获取物品头像 */
    getGoodsIcon(goodsBaseId:number | string):string {
        return this.mBaseGoods[goodsBaseId].icon;
    }
}