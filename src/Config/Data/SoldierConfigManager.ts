import Singleton from "../../Common/Core/Singleton";
import ConfigDataManager from "./ConfigDataManager";
export class BaseSoldier {
    base_id:number;
    name:string;
    type?:number = 1;                   // 类型
    lv?:number = 1;
    attr_template_id?:number = 0;       // 属性模版id
    nextId?:number = 0;                 // 下一级ID
    icon:string;                        // 资源名
    def_icon?:string = "";              // 守方资源名
    size?:number = 0;                   // 占用格子
    mPivotX?:number = 0;                // x重心
    mPivotY?:number = 0;                // y重心
    lv_up_des?:string = "";             // 升级描述
    skill_compose?:number = 0;          // 合成科技ID
    power?:number = 0;                  // 战斗力
}
export default class SoldierConfigManager extends Singleton {
    private mSoldiers:{[key:number]:BaseSoldier} = {};
    //实例
    public static get Instance(): SoldierConfigManager {
        return SoldierConfigManager.getInstance<SoldierConfigManager>();
    }

    /**解析配置 */
    parse():void {
        //soldier.json
        let mgr:ConfigDataManager = ConfigDataManager.Instance;
        this.parseSoldier(mgr["soldier"]);
        delete mgr["soldier"];
    }
    /**解析兵 */
    parseSoldier(jsonData:any):void {
        for (let baseId in jsonData) {
            let intBaseId= Number(baseId);
            this.mSoldiers[intBaseId] = jsonData[baseId];
        }
    }
    /**获取baseSoldier */
    getBaseSoldier(soldierBaseId:number | string):BaseSoldier {
        return this.mSoldiers[soldierBaseId];
    }
    /**获取兵种战力 */
    getSoldierPower(soldierBaseId:number, num:number = 1):number {
        return this.mSoldiers[soldierBaseId].power * num;
    }
}