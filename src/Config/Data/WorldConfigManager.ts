import Singleton from "../../Common/Core/Singleton";
import ConfigDataManager from "./ConfigDataManager";
import SoldierConfigManager from "./SoldierConfigManager";
export class BaseCityType {
    city_type: number;
    name: string;                    //城池名字
    size?: number = 1;               //城池xy方向占格子
    cost_phy?: number = 0;           //花费体力
    icon?: string = "";              //图标
    goods_id_list?: number[] = [];   // 
}
export class BasePvpArmy {
    soldier_base_id:number;         // 怪物id
    num:number;                     // 怪物数量
    reward:number[];                // 奖励列表
}

export default class WorldConfigManager extends Singleton {
    public mBaseCitys: { [key: number]: BaseCityType } = {};
    public mPvpArmys:{[key:number]:BasePvpArmy} = {};

    //实例
    public static get Instance(): WorldConfigManager {
        return WorldConfigManager.getInstance<WorldConfigManager>();
    }

    /**解析配置 */
    parse(): void {
        //city_type.json
        let mgr: ConfigDataManager = ConfigDataManager.Instance;
        this.parseCity(mgr["city_type"]);
        delete mgr["city_type"];

        //pvp_army.json
        this.parsePvpArmy(mgr['pvp_army']);
        delete mgr['pvp_army'];
    }
    //------------------------city_type-------------------------
    /**解析城池 */
    parseCity(jsonData): void {
        for (let city_type in jsonData) {
            let intCityType = Number(city_type);
            this.mBaseCitys[intCityType] = jsonData[city_type];
        }
    }
    /**获取city_type */
    getBaseCityTypeInfo(cityType: number | string): BaseCityType {
        return this.mBaseCitys[cityType];
    }
    /**获取城池大小 */
    getCitySize(cityType: number | string): number {
        return this.mBaseCitys[cityType].size;
    }
    /**获取城池名字 */
    getCityName(cityType: number | string): string {
        return this.mBaseCitys[cityType].name;
    }
    /**获取城池icon  */
    getCityIcon(cityType: number | string): string {
        return this.mBaseCitys[cityType].icon;
    }

    //------------------------pvp_army-------------------
    /**解析pvp_army.json */
    parsePvpArmy(jsonData):void {
        for (let k in jsonData) {
            let intK = Number(k);
            let item = jsonData[intK];
            let vid = this.makeVirtualPVPArmyId(item.city_type, item.lv);
            this.mPvpArmys[vid] = item;
        }
    }
    //虚拟id
    private makeVirtualPVPArmyId(type: number, lv: number): number {
        return type * 10000 + lv;
    }
    /**根据类型和lv获取信息 */
    getBasePvpArmy(type:number, lv:number):BasePvpArmy {
        let vid = this.makeVirtualPVPArmyId(type, lv);
        return this.mPvpArmys[vid];
    }
    /**计算战力 */
    getPvpArmyPower(type:number, lv:number):number {
        let basePvpArmy = this.getBasePvpArmy(type, lv);
        return SoldierConfigManager.Instance.getSoldierPower(basePvpArmy.soldier_base_id, basePvpArmy.num);
    }
    /**获取奖励 */
    getPvpArmyRewards(type:number, lv:number):number[] {
        return this.getBasePvpArmy(type, lv).reward;
    }
}