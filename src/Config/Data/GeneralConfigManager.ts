import Singleton from "../../Common/Core/Singleton";
import ConfigDataManager from "./ConfigDataManager";

export class BaseGeneral {
    id: number;
    name: string;
    quality?: number = 1;
    type?: number = 0;
    des: string;
    painting?: string = "";          // 立绘
    skill_id?: number = 0;           // 技能id
    init_force?: number = 0;         // 初始武力
    init_def?: number = 0;           // 初始防御
    init_soldiers?: number = 0;      // 初始统帅值       
    icon: string;                    // 头像资源
    composition?: number = 0;        // 武将碎片id
    attr_template_id?: number = 0;   // 属性模版id
}

export default class GeneralConfigManager extends Singleton {
    public mBaseGenerals: { [key: number]: BaseGeneral } = {};
    public mGeneralBaseIds:number[] = [];

    //实例
    public static get Instance(): GeneralConfigManager {
        return GeneralConfigManager.getInstance<GeneralConfigManager>();
    }

    /**解析配置 */
    parse(): void {
        //general.json
        let mgr: ConfigDataManager = ConfigDataManager.Instance;
        this.parseGeneral(mgr["general"]);
        delete mgr["general"];
    }
    /**解析物品 */
    parseGeneral(jsonData: any): void {
        for (let id in jsonData) {
            let generalBaseId = Number(id);
            this.mBaseGenerals[generalBaseId] = jsonData[id];
            this.mGeneralBaseIds.push(generalBaseId);
        }
    }
    /**获取baseGeneral */
    getBaseGeneral(generalBaseId: number | string): BaseGeneral {
        return this.mBaseGenerals[generalBaseId];
    }
    /**获取武将id列表 */
    getAllGeneralBaseIds():number[]  {
        return this.mGeneralBaseIds;
    }
}