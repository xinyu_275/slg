import Singleton from "../../Common/Core/Singleton";
import ConfigDataManager from "./ConfigDataManager";

export class BaseMail {
    id: number;
    desc?: string = "";
    type?: number = 0;
    event?: string = "";
    title?: string = "";
    content?: string = "";
}
export default class MailConfigManager extends Singleton {
    public mBaseMails: { [key: number]: BaseMail } = {};
    //实例
    public static get Instance(): MailConfigManager {
        return MailConfigManager.getInstance<MailConfigManager>();
    }

    /**解析配置 */
    parse(): void {
        //goods.json
        let mgr: ConfigDataManager = ConfigDataManager.Instance;
        this.parseMailTemplate(mgr["mail_template"]);
        delete mgr["mail_template"];
    }

    /**解析邮件模板 */
    parseMailTemplate(jsonData: any): void {
        for (let templateId in jsonData) {
            let intTemplateId = Number(templateId);
            this.mBaseMails[intTemplateId] = jsonData[intTemplateId];
        }
    }

    /**获取邮件配置Title */
    getMailTitle(templateId: number | string): string {
        return this.mBaseMails[templateId].title;
    }

    /**获取邮件配置内容 */
    getMailContent(templateId: number | string): string {
        return this.mBaseMails[templateId].content;
    }

    /**获取邮件类型 */
    getMailType(templateId: number | string): number {
        return this.mBaseMails[templateId].type;
    }
}