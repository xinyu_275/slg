import Singleton from "../../Common/Core/Singleton";
import GoodsConfigManager from "./GoodsConfigManager";
import WorldConfigManager from "./WorldConfigManager";
import MailConfigManager from "./MailConfigManager";
import GeneralConfigManager from "./GeneralConfigManager";
import SoldierConfigManager from "./SoldierConfigManager";

// 游戏配置数据管理器
export default class ConfigDataManager extends Singleton {
    private parsed:boolean = false; // 是否已经解析了配置文件
    //实例
    public static get Instance(): ConfigDataManager {
        return ConfigDataManager.getInstance<ConfigDataManager>();
    }
    
    /**
     * 获取资源加载路径列表
     */
    getAssetLoadUrls(): any[] {
        let result: any[] = [];
        for (let i = 0; i < EDataConfig._endNull; i++) {
            let url: string = this.getConfigUrl(EDataConfig[i]);
            result.push({ url: url, type: Laya.Loader.JSON })
        }
        return result;
    }

    /**
     * 获取配置文件url(有路径)
     * param file: 文件名(无后缀)
     */
    getConfigUrl(file: string): string {
        return "res/config/" + file + ".json";
    }

    /**
     * 解析配置
     */
    parse():void{
        if(this.parsed){
            return;
        }
        this.parsed = true;
        for (let i = 0; i < EDataConfig._endNull; i++) {
            let file: string = EDataConfig[i];
            let url: string = this.getConfigUrl(file);
            this[file] = Laya.loader.getRes(url);
        }
        GoodsConfigManager.Instance.parse();
        WorldConfigManager.Instance.parse();
        MailConfigManager.Instance.parse();
        GeneralConfigManager.Instance.parse();
        SoldierConfigManager.Instance.parse();
    }
}

//json数据配置文件
export enum EDataConfig {
    //GoodsConfigManager
    goods,
    //WorldConfigManager
    city_type,
    pvp_army,
    //MailConfigManager
    mail_template,
    //GeneralConfigManager
    general,
    //SoldierConfigManager
    soldier,
    _endNull,
}
