//普通提示(这里是代码中的自定义配置)
export enum ETip{
    //通用

    //邮件
    MailNoAttchment = "没有附件可以领取",
    MailHaveRewardCannotDel = "邮件中有奖励未领取，请领取后删除",
    MailAllIsDel = "是否删除当前列表中全部邮件",
    MailSaveSuccess = "归档成功",
}