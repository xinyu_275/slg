import { ui } from "../../ui/layaMaxUI";
import GoodsConfigManager from "../../Config/Data/GoodsConfigManager";
import UI_RewardList from "../Script/UI_RewardList";
import EventManager from "../../Common/Core/EventManager";
import { ESceneName } from "../../Config/EnumConfig";

//奖励弹出框
export default class UI_RewardPopup extends ui.Panel.Lobby.RewardPopupUI {
    constructor() {
        super();
        this.btn_sure.on(Laya.Event.CLICK, this, this.onSure);
    }

    onEnable():void {
        EventManager.Instance.on(ESceneName.UI_RewardPopup, this, this.init);
    }

    onDisable() :void {
        EventManager.Instance.offAllCaller(this);
    }
    
    /**
     * 初始化
     * @param rewardList awesomepackage.Ip_goods_num[] | awesomepackage.Ip_reward[]
     */
    init(rewardList: awesomepackage.Ip_goods_num[] | awesomepackage.Ip_reward[]): void {
        //特殊处理
        (this.list_rewards as Laya.List).hScrollBarSkin = "";
        if (rewardList.length == 1){
            this.list_rewards.pos(320, 544);
        }else if (rewardList.length == 2){
            this.list_rewards.pos(250, 544);
        }else if (rewardList.length == 3){
            this.list_rewards.pos(177, 544);
        }else {
            this.list_rewards.pos(105, 544);
        }
        (this.list_rewards as UI_RewardList).init(rewardList);
    }

    onSure(): void {
        this.close();
    }
}