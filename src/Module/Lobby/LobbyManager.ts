import AbsGameModule from "../AbsGameModule";
import SceneManager from "../../Common/SceneManager";
import SwitchManager from "../../Common/SwitchManager";
import EventManager from "../../Common/Core/EventManager";
import { EEventType, ESceneName } from "../../Config/EnumConfig";

export default class LobbyManager extends AbsGameModule {
    //是否游戏数据初始化完成
    public initedGame:boolean = false;
    //实例
    public static get Instance(): LobbyManager {
        return LobbyManager.getInstance<LobbyManager>();
    }
    clear():void {
        this.initedGame = false;
    }
    //======================界面相关====================
    public openView(initedGame:boolean = false): void {
        this.initedGame = initedGame;
        SceneManager.Instance.openScene(ESceneName.UI_Lobby, null, false, Laya.Handler.create(this, this.openViewCallback));
    }
    openViewCallback():void {
       this.refreshView();
       //关闭切换动画
       SwitchManager.Instance.closeView();
    }

    //刷新整个大厅控件
    refreshView(initedGame:boolean = false):void {
        this.initedGame = initedGame || this.initedGame;
        if(this.initedGame) {
            //刷新大厅
            EventManager.Instance.event(EEventType.LobbyRefreshView);
        }
    }
}