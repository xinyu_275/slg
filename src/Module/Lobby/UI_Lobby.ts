import { ui } from "../../ui/layaMaxUI";
import GameStateManager, { TypeStateGame } from "../../GameState/GameStateManager";
import SceneManager from "../../Common/SceneManager";
import BagManager from "../Bag/BagManager";
import EventManager from "../../Common/Core/EventManager";
import { EEventType, ESceneName } from "../../Config/EnumConfig";
import LogM from "../../Common/LogM";
import WorldManager from "../World/WorldManager";
import UI_Bag from "../Bag/UI_Bag";
import NetSocketManager from "../../Common/NetSocketManager";
export default class UI_Lobby extends ui.Panel.Lobby.LobbyUI {
    constructor() {
        super();
        //按钮点击事件
        this.btn_chuzheng.on(Laya.Event.CLICK, this, this.onClickChuzheng);
        this.btn_huicheng.on(Laya.Event.CLICK, this, this.onClickHuicheng);
        this.btn_search.on(Laya.Event.CLICK, this, this.onClickSearch);
        this.btn_build.on(Laya.Event.CLICK, this, this.onClickBuild);
        this.btn_bag.on(Laya.Event.CLICK, this, this.onClickBag);
        this.btn_mail.on(Laya.Event.CLICK, this, this.onClickMail);
        this.btn_general.on(Laya.Event.CLICK, this, this.onClickGeneral);

        this.mapBox.on(Laya.Event.CLICK, this, this.onMapBoxClick);
        this.mapBox.on(Laya.Event.MOUSE_DOWN, this, this.onMapBoxMouseDown);
    }
    onEnable(): void {
        this.zOrder = 1;
        Laya.stage.addChild(this);
        EventManager.Instance.on(EEventType.LobbyRefreshView, this, this.onLobbyRefreshView);
    }
    onDisable(): void {
        EventManager.Instance.offAllCaller(this);
    }
    /**刷新大厅所有控件状态 */
    onLobbyRefreshView(): void {
        //1.刷新按钮的显隐
        this.refreshVisable();
    }
    //刷新按钮的显隐
    refreshVisable(): void {
        let InLobby = GameStateManager.Instance.curState == TypeStateGame.Main;
        this.btn_search.visible = !InLobby;
        this.btn_build.visible = InLobby;
        this.btn_chuzheng.visible = InLobby;
        this.btn_huicheng.visible = !InLobby;
    }

    //--------------------------------按钮点击事件----------------------------
    /**实现tiledmap地图点击事件 */
    onMapBoxClick(e: Laya.Event): void {
        EventManager.Instance.event(EEventType.MapMouseClick, e);
        e.stopPropagation();
    }
    /** 实现tiledmap地图按下事件*/
    onMapBoxMouseDown(e: Laya.Event): void {
        EventManager.Instance.event(EEventType.MapMouseDown, e);
        e.stopPropagation();
    }
    /**出征 */
    onClickChuzheng(): void {
        GameStateManager.Instance.switchState(TypeStateGame.World);
    }
    /**回程 */
    onClickHuicheng(): void {
        GameStateManager.Instance.switchState(TypeStateGame.Main);
    }
    /**搜索 */
    onClickSearch(): void {

    }
    /**建造 */
    onClickBuild(): void {

    }
    /**背包 */
    onClickBag(): void {
        SceneManager.Instance.openScene(ESceneName.UI_Bag);
    }
    
    /**邮件 */
    onClickMail():void {
        SceneManager.Instance.openScene(ESceneName.UI_Mail);
    }

    /**武将 */
    onClickGeneral():void {
        SceneManager.Instance.openScene(ESceneName.UI_General);
    }
}