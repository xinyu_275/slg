import { ui } from "../../ui/layaMaxUI";
import LockManager from "../../Common/LockManager";
import SceneManager from "../../Common/SceneManager";
import TipManager from "./TipManager";
export default class UI_Tip extends ui.Panel.Tip.TipUI {
    private mStartY: number = 0;

    constructor() {
        super();
        this.zOrder = 1;
    }

    onEnable(): void {
        this.mStartY = this.tips_1.y;
        Laya.timer.clearAll(this);
        this.doStartMsg();
    }

    onDisable(): void {
    }

    /**
     * 开始显示
     */
    private doStartMsg(): void {
        let text = TipManager.Instance.cacheNormalMsg.pop();
        if (!text) {
            this.close();
            return;
        }
        this.tips_1.text = text;
        this.tips_1.y = this.mStartY - 100;
        this.tips_1.alpha = 0.3;
        this.tips_1.visible = true;
        
        Laya.Tween.to(this.tips_1, { y: this.mStartY, alpha: 1.0 }, 200, Laya.Ease.circOut, Laya.Handler.create(this, this.doStartMsgComplete), 0);
    }
    public doStartMsgComplete(): void {
        if (TipManager.Instance.cacheNormalMsg.length == 0) {
            //2s后删除
            Laya.timer.once(2000, this, this.checkClose);
            return;
        }
        this.doStartMsg();
    }
    public checkClose(): void {
        if(TipManager.Instance.cacheNormalMsg.length == 0) {
            this.close();
            return;
        }
        this.doStartMsg();
    }
}