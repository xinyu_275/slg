import Singleton from "../../Common/Core/Singleton";
import { ETipType, ESceneName } from "../../Config/EnumConfig";
import UI_Tip from "./UI_Tip";
import SceneManager from "../../Common/SceneManager";

export default class TipManager extends Singleton {
    public cacheNormalMsg:string[] = [];
    //实例
    public static get Instance(): TipManager {
        return TipManager.getInstance<TipManager>();
    }

    /**
     * 根据Id显示 普通提示/跑马灯
     * @param id:number     
     */
    public showMsgById(id:number) : void {
        let tmpConfig;
        let type:ETipType = ETipType.Normal;
        let text:string = "";
        if (tmpConfig == null) {
            this.showMsgByType(ETipType.Normal, id+"没有配，请检查配置");
            return;
        }
        this.showMsgByType(type, text);
    }

    /**
     * 根据Id和参数显示 普通提示/跑马灯
     * @param id:number 
     * @param params：string[] 
     */
    public showMsgParam(id:number, params:string[]) :void {

    }

    /**
     * 根据类型显示 普通提示/跑马灯
     * @param type:ETipType   类型
     * @param text:string   文本
     */
    public showMsgByType(type:ETipType, text:string) : void {
        switch(type) {
            case ETipType.Normal:
                this.cacheNormalMsg.push(text);
                SceneManager.Instance.openScene(ESceneName.UI_Tip);
                break;
            case ETipType.Flash:
                break;
            default:
                break;
        }
    }
    /**
     * 普通提示
     * @param text:string   文本
     */
    public showNormalMsg(text:string):void {
        this.showMsgByType(ETipType.Normal, text);
    }
}