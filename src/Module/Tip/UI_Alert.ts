import { ui } from "../../ui/layaMaxUI";
import EventManager from "../../Common/Core/EventManager";
import { ESceneName } from "../../Config/EnumConfig";
export default class UI_Alert extends ui.Panel.Tip.AlertUI {
    private handlerYes:Laya.Handler = null;
    private handlerNo:Laya.Handler = null;
    private loginNoTip:boolean = false;           // 是否点击了 "下次登录不显示"
    constructor(){
        super();
        this.btn_yes.on(Laya.Event.CLICK,  this, this.onYes);
        this.btn_yes1.on(Laya.Event.CLICK, this, this.onYes);
        this.btn_no.on(Laya.Event.CLICK, this, this.onNo);
        this.btn_close.on(Laya.Event.CLICK, this, this.onNo);
        this.btn_tip.on(Laya.Event.CLICK, this, this.onTip);
       
    }
    onEnable():void {
        EventManager.Instance.on(ESceneName.UI_Alert, this, this.init);
    }
    onDisable():void {
        this.handlerYes = null;
        this.handlerNo = null;
        this.loginNoTip = false;
        EventManager.Instance.offAllCaller(this);
    }
    /**初始化 */
    init(title: string, text: string, handlerYes: Laya.Handler, type:number, showTip:boolean, handlerNo: Laya.Handler, yes: string, no: string):void {
        this.label_title.text = title;
        this.label_text.text = text;
        this.handlerYes = handlerYes;
        this.handlerNo = handlerNo;
        if(type == 0) {
            this.btn_no.visible = false;
            this.btn_yes.visible = false;
            this.btn_yes1.visible = true;
        }else {
            this.btn_no.visible = true;
            this.btn_yes.visible = true;
            this.btn_yes1.visible = false;
        }
        this.height = 428;
        if(!showTip) {
            this.height = 380;
        }
        this.btn_tip.visible = showTip;
        this.btn_yes.label = yes;
        this.btn_yes1.label = yes;
        this.btn_no.label = no;
        this.img_gou.visible = false;
    }
    //------------------------------事件处理-----------------------
    /**点击确认 */
    onYes():void {
        this.handlerYes && this.handlerYes.runWith(this.loginNoTip);
        this.close();
    }

    /**点击取消 */
    onNo():void {
        this.handlerNo && this.handlerNo.run();
        this.close();
    }

    /**点击tip */
    onTip():void {
        this.img_gou.visible = !this.img_gou.visible;
        this.loginNoTip = this.img_gou.visible;
    }
}