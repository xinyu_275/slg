export default class UI_Slider extends Laya.Box {
    private btnSliderAdd: Laya.Button = null;
    private btnSliderSub: Laya.Button = null;
    private labelSliderNum: Laya.Label = null;
    private slider: Laya.HSlider = null;
    onEnable(): void {
        this.btnSliderAdd = this.getChildByName("btn_sliderAdd") as Laya.Button;
        this.btnSliderSub = this.getChildByName("btn_sliderSub") as Laya.Button;
        this.labelSliderNum = this.getChildByName("label_sliderNum") as Laya.Label;
        this.slider = this.getChildByName("slider") as Laya.HSlider;
        this.btnSliderAdd.on(Laya.Event.CLICK, this, this.onBtnSliderAddClick);
        this.btnSliderSub.on(Laya.Event.CLICK, this, this.onBtnSliderSubClick);
    }

    /**初始化控件 */
    init(maxNum: number, minNum: number = 1, step: number = 1): void {
        this.slider.max = maxNum;
        this.slider.min = minNum;
        this.slider.tick = step;
        this.slider.value = maxNum;
        this.labelSliderNum.text = "" + maxNum;
        this.slider.changeHandler = new Laya.Handler(this, this.onChangeHandler);
    }

    /**滑动条改变 */
    onChangeHandler(): void {
        this.labelSliderNum.text = "" + this.slider.value;
    }

    /**+ */
    onBtnSliderAddClick(): void {
        let curNum = this.slider.value;
        if (curNum >= this.slider.max) {
            return;
        }
        let newNum = curNum + this.slider.tick;
        newNum = newNum >= this.slider.max ? this.slider.max : newNum;
        this.slider.value = newNum;
    }

    /**- */
    onBtnSliderSubClick():void {
        let curNum = this.slider.value;
        if (curNum <= this.slider.min) {
            return;
        }
        let newNum = curNum - this.slider.tick;
        newNum = newNum <= this.slider.min ? this.slider.min : newNum;
        this.slider.value = newNum;
    }
    
    /**获取进度调数量 */
    getValue(): number {
        return this.slider.value;
    }
}