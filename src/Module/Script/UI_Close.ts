export default class UI_Close extends Laya.Script {
    onClick(): void {
        //Laya.Tween.to(this.owner, { scale:1.5 }, 200, Laya.Ease.circOut, Laya.Handler.create(this, this.onClickCallback), 0);
        // if(this.owner.parent instanceof Laya.Dialog) {
        //     (this.owner.parent as Laya.Dialog).close();
        // }else if(this.owner.parent.parent instanceof Laya.Dialog) {
        //     (this.owner.parent.parent as Laya.Dialog).close();
        // }
        this.onClickCallback();
    }
    onClickCallback():void {
        if(this.owner.parent instanceof Laya.Dialog) {
            (this.owner.parent as Laya.Dialog).close();
        }else if(this.owner.parent.parent instanceof Laya.Dialog) {
            (this.owner.parent.parent as Laya.Dialog).close();
        }
    }
}