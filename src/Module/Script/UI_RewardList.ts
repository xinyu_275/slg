import VOGoods from "../Bag/VOGoods";
import Util from "../../Common/Core/Util";
import EventManager from "../../Common/Core/EventManager";
import { EEventType, EGoodsType, ESceneName } from "../../Config/EnumConfig";
import GoodsConfigManager from "../../Config/Data/GoodsConfigManager";
import SceneManager from "../../Common/SceneManager";
import UI_GoodsDesc from "../Bag/UI_GoodsDesc";

//奖励列表处理/弹出处理
export default class UI_RewardList extends Laya.List {
    private canClick:boolean = false;
    constructor(){
        super();
        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
        this.mouseHandler = new Laya.Handler(this, this.onMouseHandler);
    }
    onEnable():void {
    }
    onDisable():void {
        this.array = [];
    }
    
    /**
     * 初始化list数据 
     * _datas: [{goods_base_id:Id, num?:Num}] | awesomepackage.Ip_reward | awesomepackage.I
     * clickHandler:选中后回调处理
     * canClick:是否可以点击
     */
    init(_datas:any[], canClick:boolean = false) {
        this.vScrollBarSkin = "";
        //格式化数据
        let datas = [];
        for(let item of _datas) {
            let goodsBaseId:number = item.goods_base_id;
            if(goodsBaseId == 0) {
                goodsBaseId = 1025;     // 金币
                if (item.reward_type == awesomepackage.e_res_type.diamond) {
                    goodsBaseId = 1026;
                }
            }
            datas.push({goods_base_id:goodsBaseId, num:item.num})
        }
        this.array = datas;
        this.canClick = canClick;
    }

    /**渲染 */
    onRenderHandler(cell: Laya.Box, index: number):void {
        if (index >= this.array.length){
            return;
        }
        //物品图标
        let imgItemIcon = cell.getChildByName("img_itemIcon") as Laya.Image;
        let labelName = cell.getChildByName("label_itemName") as Laya.Label;
        let labelNum = cell.getChildByName("label_itemNum") as Laya.Label;
        let labelLv = cell.getChildByName("label_itemLv") as Laya.Label;
        let cellData = this.array[index];
        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(cellData.goods_base_id);
        imgItemIcon.skin = baseGoods.icon;
        labelName && (labelName.text = baseGoods.name);
        labelNum && (labelNum.text = "X"+cellData.num);
        if(baseGoods.type == EGoodsType.stren) {
            labelLv && (labelLv.text = "lv."+baseGoods.lv);
        }else {
            labelLv && (labelLv.text = "");
        }
    }

    onMouseHandler(e:Laya.Event, index:number):void {
        if(!this.canClick) return;
        let cellData = this.array[index];
        if(e.type == Laya.Event.CLICK) {
            SceneManager.Instance.openScene(ESceneName.UI_GoodsDesc, [cellData.goods_base_id]);
        }
    }
}