import SoldierConfigManager from "../../Config/Data/SoldierConfigManager";
import GeneralConfigManager from "../../Config/Data/GeneralConfigManager";

export interface IDeployData {
    id:number;      // 武将id /兵id
    type:number;    // 1.武将 2.士兵
    star?:number;   // 武将星级
    lv?:number;     // 武将等级
}
export default class UI_DeployList extends Laya.List {
    /**显示武将 和 兵的部署情况 */
    constructor() {
        super();
        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
    }
    onEnable(): void {

    }
    onDisable(): void {
        this.array = [];
    }

    /**
     * 初始化
     * @param cell chushi
     * @param index 
     */
    init(datas:IDeployData[]):void {
        this.array = datas;
    }

    /**渲染 */
    onRenderHandler(cell: Laya.Box, index: number): void {
        let cellData:IDeployData = this.array[index];
        let img_itemBg:Laya.Image = cell.getChildByName("img_itemBg") as Laya.Image;
        let img_itemIcon:Laya.Image = cell.getChildByName("img_itemIcon") as Laya.Image;
        let box_itemStars:Laya.Box = cell.getChildByName("box_itemStars") as Laya.Box;
        let label_itemLv:Laya.Label = cell.getChildByName("label_itemLv") as Laya.Label;
        label_itemLv.visible = cellData.type == 1;
        box_itemStars.visible = cellData.type == 1;
        if(cellData.type == 1){
            //武将
            let baseGeneral = GeneralConfigManager.Instance.getBaseGeneral(cellData.id);
            img_itemIcon.skin = baseGeneral.icon;
        }else {
            //士兵
            let baseSoldier = SoldierConfigManager.Instance.getBaseSoldier(cellData.id);
            img_itemIcon.skin = baseSoldier.icon;
        }
    }
}