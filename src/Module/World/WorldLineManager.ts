import Singleton from "../../Common/Core/Singleton";
import EventManager from "../../Common/Core/EventManager";
import WorldManager from "./WorldManager";
import Util from "../../Common/Core/Util";
import NetSocketManager from "../../Common/NetSocketManager";
import { EPoolName } from "../../Config/EnumConfig";
import WorldLine from "./WorldLine";
import VOWorldLine from "./VOWorldLine";
import TimeManager from "../../Common/Core/TimeManager";

export default class WorldLineManager extends Singleton {
    //#region 数据字段
    private armyIdList: number[] = [];
    private lines: { [key: number]: WorldLine } = {};

    constructor() {
        super();
    }
    //实例
    public static get Instance(): WorldLineManager {
        return WorldLineManager.getInstance<WorldLineManager>();
    }
    /**场景进入 */
    scene_enter(): void {
        this.armyIdList = [];
        this.lines = {};
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_pvp_lines, this, this.s_pvp_lines);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_pvp_add_line, this, this.s_pvp_add_line);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_pvp_del_line, this, this.s_pvp_del_line);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_pvp_lines, this, this.s_get_pvp_lines);
        Laya.timer.frameLoop(1, this, this.loopFrame);
        // NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_add_speed_army, this, this.s_add_speed_army);
        // NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_other_main_city_pos, this, this.s_get_other_main_city_pos);

    }
    /**场景退出 */
    scene_exit(): void {
        EventManager.Instance.offAllCaller(this);
        Laya.timer.clearAll(this);

        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_pvp_lines, this.s_pvp_lines);
        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_pvp_add_line, this.s_pvp_add_line);
        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_pvp_del_line, this.s_pvp_del_line);
        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_get_pvp_lines, this.s_get_pvp_lines);
        // NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_add_speed_army, this.s_add_speed_army);
        // NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_get_other_main_city_pos, this.s_get_other_main_city_pos);

        //清除城池数据 
        for (let key in this.lines) {
            this.lines[key].recover();
        }
        this.lines = {};
        //清除pool
        let objs: WorldLine[] = Laya.Pool.getPoolBySign(EPoolName.WorldLine);
        for (let item of objs) {
            item.destroy();
        }
        Laya.Pool.clearBySign(EPoolName.WorldLine);
    }

    /**帧循环 */
    public loopFrame(): void {
        for (let armyId in this.lines) {
            let line: WorldLine = this.lines[armyId];
            line.updateAnim();
        }
    }

    //====================================协议处理=============================
    /** 获取连线列表*/
    s_pvp_lines(data: awesomepackage.s_pvp_lines): void {
        if (!WorldManager.Instance.loadWorldMap) return;
        let newArmyIdList: number[] = data.army_id_list;
        //删除不在视野的连线
        for (let armyId of this.armyIdList) {
            if (newArmyIdList.indexOf(armyId) === -1) {
                this.delLine(armyId);
            }
        }
        this.armyIdList = newArmyIdList;
        //获取战斗连线
        let filterArmyIdList: number[] = [];
        for (let armyId of this.armyIdList) {
            if (!this.lines[armyId]) {
                filterArmyIdList.push(armyId);
            }
        }
        this.c_get_pvp_lines(filterArmyIdList);
    }
    /** 根据armyid列表获取连线*/
    c_get_pvp_lines(armyIdList: number[]): void {
        if (!WorldManager.Instance.loadWorldMap) return;
        if (!armyIdList || armyIdList.length == 0) {
            return;
        }
        let message = { "army_id_list": armyIdList };
        NetSocketManager.Instance.SendMsg("c_get_pvp_lines", message);
    }
    /** 添加战斗连线*/
    s_pvp_add_line(data: awesomepackage.s_pvp_add_line): void {
        if (!WorldManager.Instance.loadWorldMap) return;
        let voLine: VOWorldLine = new VOWorldLine();
        voLine.init(data.line);
        let armyId: number = voLine.armyId;
        if (this.armyIdList.indexOf(armyId) === -1) {
            this.armyIdList.push(armyId);
        }
        this.addLine(voLine);
    }
    /** 删除战斗连线*/
    s_pvp_del_line(data: awesomepackage.s_pvp_del_line): void {
        if (!WorldManager.Instance.loadWorldMap) return;
        this.delLine(data.army_id);
    }
    /** 获取战斗连线列表*/
    s_get_pvp_lines(data: awesomepackage.s_get_pvp_lines): void {
        if (!WorldManager.Instance.loadWorldMap) return;
        let voLine: VOWorldLine;
        for (let lineBytes of data.lines) {
            let armyId: number = VOWorldLine.getArmyId(lineBytes);
            if (this.armyIdList.indexOf(armyId) >= 0) {
                voLine = new VOWorldLine();
                voLine.init(lineBytes);
                this.addLine(voLine);
            }
        }
    }
    /**test */
    gmAddLine(): void {
        let vo = new VOWorldLine();
        vo.armyId = 1;
        vo.startX = 13;
        vo.startY = 877;
        vo.toX = 40;
        vo.toY = 1000;
        vo.startTime = TimeManager.Instance.serverTime;
        vo.arriveTime = TimeManager.Instance.serverTime + 120;
        vo.playerId = 1;
        vo.op = 1;
        vo.start_city_type = 4;
        vo.to_city_type = 4;
        vo.nickname = "aaaa";
        this.addLine(vo);
    }

    /**添加连线 */
    addLine(voLine: VOWorldLine) {
        if (!WorldManager.Instance.loadWorldMap) return;
        let oldLine: WorldLine = this.lines[voLine.armyId];
        if (!oldLine) {
            let line: WorldLine = Laya.Pool.getItemByClass(EPoolName.WorldLine, WorldLine);
            line.init(voLine);
            this.lines[voLine.armyId] = line;
        } else {
            oldLine.init(voLine);
        }
    }

    /** 删除连线*/
    public delLine(armyId: number | string): void {
        let line: WorldLine = this.lines[armyId];
        if (line) {
            delete this.lines[armyId];
            line.recover();
        }
    }
}