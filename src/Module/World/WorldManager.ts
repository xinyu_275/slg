import AbsGameModule from "../AbsGameModule";
import LogM from "../../Common/LogM";
import Common from "../../Config/Common";
import EventManager from "../../Common/Core/EventManager";
import { EEventType, EAniPath, EPoolName } from "../../Config/EnumConfig";
import LobbyManager from "../Lobby/LobbyManager";
import UI_Lobby from "../Lobby/UI_Lobby";
import TiledUtil from "../../Common/Core/TiledUtil";
import WorldItemManager from "./WorldItemManager";
import GameStateManager, { TypeStateGame } from "../../GameState/GameStateManager";
import PlayerManager from "../Player/PlayerManager";
import WorldLineManager from "./WorldLineManager";
import WorldItem from "./WorldItem";

export default class WorldManager extends AbsGameModule {
    public mapLoaded: boolean = false;                          //地图是否已经加载完成
    public worldTileMap: Laya.TiledMap = null;                  //tiledmap
    private itemLayer: Laya.MapLayer = null;                    //tiledmao 物件层
    private mapSprite: Laya.Sprite;                             //tiled容器
    private directionImge: Laya.Image = null;                   //方向提示
    private jumpHandler: Laya.Handler = null;

    //tieldmap宽高控制
    private _mapX: number = 0;
    private _mapY: number = 0;
    private nMinMapX: number = 0;
    private nMinMapY: number = 0;
    private nMaxMapX: number = 0;
    private nMaxMapY: number = 0;
    private lastDistance: number = 0;
    private mLastMouseX: number = 0;
    private mLastMouseY: number = 0;

    //容器
    public mTipSp: Laya.Sprite;
    public mCitysSp: Laya.Sprite;       // 城池和动画
    public mCityLabelsSp: Laya.Sprite;
    public mFightLineSp: Laya.Sprite;
    public mDirectionSp: Laya.Sprite;
    public mArmySp: Laya.Sprite;

    // Map move relationship parameters.
    private isScaleMap: boolean = false;
    private isMoveClick: boolean = false;    //是否是移动操作出发的click

    //实例
    public static get Instance(): WorldManager {
        return WorldManager.getInstance<WorldManager>();
    }
    /**场景进入 */
    scene_enter(): void {
        this.mapLoaded = false;
        WorldItemManager.Instance.scene_enter();
        WorldLineManager.Instance.scene_enter();
    }
    /**场景退出 */
    scene_exit(): void {
        this.mapLoaded = false;
        EventManager.Instance.offAllCaller(this);

        WorldItemManager.Instance.scene_exit();
        WorldLineManager.Instance.scene_exit();
    }
    //--------------------地图相关-------------------------
    /**加载世界地图 */
    loadWorldMap(): void {
        this.worldTileMap = new Laya.TiledMap();
        //因为地图需要缓存，因此这里先使用异步加载json
        var assetArr: Array<any> = [
            { url: Common.WorldTileMapJson, type: Laya.Loader.JSON },
            { url: EAniPath.WorldFire, type: Laya.Loader.ATLAS },
            { url: EAniPath.WorldProtect, type: Laya.Loader.ATLAS },
            { url: EAniPath.WorldFight, type: Laya.Loader.ATLAS },
        ];
        Laya.loader.load(assetArr, Laya.Handler.create(this, this.loadWorldMapCallback));
    }
    loadWorldMapCallback(): void {
        let viewRect: Laya.Rectangle = new Laya.Rectangle(0, 0, Laya.stage.width, Laya.stage.height);
        //gridSize为tiled格子的整数倍
        let gridSize: Laya.Point = new Laya.Point(640, 464);
        this.worldTileMap.createMap(Common.WorldTileMapJson, viewRect, Laya.Handler.create(this, this.onMapLoaded), null, gridSize);
    }
    onMapLoaded(): void {
        LogM.debug("onWorldMapLoaded");
        this.mapLoaded = true;
        this.worldTileMap.setViewPortPivotByScale(0, 0);
        this.worldTileMap.scale = Common.DefaultMapScale;
        this.itemLayer = this.worldTileMap.getLayerByName("wujian");
        this.mapSprite = this.worldTileMap.mapSprite();

        //最上角的点是地图宽高-单屏的宽高
        this.nMaxMapX = this.worldTileMap.width - Laya.stage.width / this.worldTileMap.scale;
        this.nMaxMapY = this.worldTileMap.height - Laya.stage.height / this.worldTileMap.scale;

        //创建容器
        this.createContainer();

        //创建菱形提示
        this.createTipSprite();

        //添加事件
        EventManager.Instance.on(EEventType.MapMouseClick, this, this.onMapMouseClick);
        EventManager.Instance.on(EEventType.MapMouseDown, this, this.onMapMouseDown);

        //跳转
        if (this.jumpHandler) {
            this.jumpHandler.run();
            this.jumpHandler = null;
        } else {
            this.jumpTo(PlayerManager.Instance.mainPlayer.x, PlayerManager.Instance.mainPlayer.y);
        }
        //大厅打开，如果已经打开，那需要刷新大厅按钮状态
        LobbyManager.Instance.openView(true);

        WorldLineManager.Instance.gmAddLine();
    }
    /**创建容器 */
    createContainer(): void {
        //1.创建各种容器
        this.mTipSp = new Laya.Sprite();
        this.mCitysSp = new Laya.Sprite();
        this.mCityLabelsSp = new Laya.Sprite();
        this.mFightLineSp = new Laya.Sprite();
        this.mDirectionSp = new Laya.Sprite();
        this.mArmySp = new Laya.Sprite();
        this.mapSprite.addChild(this.mTipSp);
        this.mapSprite.addChild(this.mCitysSp);
        this.mapSprite.addChild(this.mCityLabelsSp);
        this.mapSprite.addChild(this.mFightLineSp);
        this.mapSprite.addChild(this.mDirectionSp);
        this.mapSprite.addChild(this.mArmySp);
    }
    /**创建菱形提示 */
    createTipSprite(): void {

    }
    /**创建方向键 */
    createDirectionImage(): void {
        this.directionImge = new Laya.Image();
        this.directionImge.skin = "res/worldTiledMap/direction.png";
        let label = new Laya.Label();
        label.x = 42;
        label.y = 46;
        label.fontSize = 21;
        label.rotation = 90;
        label.pivot(30, 12);
        label.color = "#ffffff";
        label.width = 60;
        label.align = "center";
        label.valign = "middle";
        this.directionImge.addChild(label);
        this.directionImge.on(Laya.Event.CLICK, this, this.clickDirectionImge);
        this.mDirectionSp.addChild(this.directionImge);
    }

    /**容器随着地图改变 */
    updateContainerLocation(): void {
        this.mTipSp.pos(this.worldTileMap.x, this.worldTileMap.y);
        this.mCitysSp.pos(this.worldTileMap.x, this.worldTileMap.y);
        this.mCityLabelsSp.pos(this.worldTileMap.x, this.worldTileMap.y);
        this.mFightLineSp.pos(this.worldTileMap.x, this.worldTileMap.y);
        this.mDirectionSp.pos(this.worldTileMap.x, this.worldTileMap.y);
        this.mArmySp.pos(this.worldTileMap.x, this.worldTileMap.y);
    }

    //设置mapxy
    get mapX(): number {
        return this._mapX;
    }
    get mapY(): number {
        return this._mapY;
    }
    set mapX(mapX: number) {
        this._mapX = mapX > this.nMaxMapX ? this.nMaxMapX : mapX;
        this._mapX = mapX < this.nMinMapX ? this.nMinMapX : mapX;
    }
    set mapY(mapY: number) {
        this._mapY = mapY > this.nMaxMapY ? this.nMaxMapY : mapY;
        this._mapY = mapY < this.nMinMapY ? this.nMinMapY : mapY;
    }

    /**
     * 跳转到某点上，某点要置于屏幕中央
     * @param logicX 游戏坐标x
     * @param logicY 游戏坐标Y
     */
    jumpTo(logicX: number, logicY: number): void {
        if (GameStateManager.Instance.curState == TypeStateGame.World) {
            let moveViewPos = this.getMoveViewPositionByCenterPos(logicX, logicY);
            this.setMoveView(moveViewPos.x, moveViewPos.y);
        } else {
            this.jumpHandler = Laya.Handler.create(this, this.jumpTo, [logicX, logicY]);
            GameStateManager.Instance.switchState(TypeStateGame.World);
        }
    }

    /**
     * 设置视口
     */
    public setMoveView(viewX: number, viewY: number): void {
        this.mapX = viewX;
        this.mapY = viewY;
        this.worldTileMap.moveViewPort(this.mapX, this.mapY);
        this.updateContainerLocation();
        WorldItemManager.Instance.checkFetchPvpCitys();
    }

    /**
     * 获取当前屏幕所在的地图中心点区块坐标
     * Create: qlw
     * @returns Point   当前屏幕所在区域的中心点区块坐标
     */
    getCurCenterBlockPos(): Laya.Point {
        var posResult: Laya.Point = new Laya.Point(0, 0);
        let curLogicCenterPos: Laya.Point = this.getLogicPosByStagePos(Laya.stage.width / 2, Laya.stage.height / 2);
        posResult.x = Math.floor(curLogicCenterPos.x / Common.MapBlockGridWidth);
        posResult.y = Math.floor(curLogicCenterPos.y / Common.MapBlockGridHeight);
        return posResult;
    }

    /**
     * 把舞台坐标转化成逻辑坐标
     * @param stageX : 舞台坐标x
     * @param stageY : 舞台坐标y
     * @return 游戏逻辑坐标
     */
    public getLogicPosByStagePos(stageX: number, stageY: number): Laya.Point {
        var p: Laya.Point = new Laya.Point(0, 0);
        this.itemLayer.getTilePositionByScreenPos(stageX, stageY, p);
        return TiledUtil.getLogicPosByMapGridPos(p);
    }

    /**
     * 根据中心点游戏坐标获取moveview位置
     * @param CenterLogicX 游戏坐标x
     * @param CenterLogicY 游戏坐标Y
     */
    public getMoveViewPositionByCenterPos(CenterLogicX, CenterLogicY): Laya.Point {
        let p: Laya.Point = this.getItemAnchorPixelPos(CenterLogicX, CenterLogicY);
        p.x = Math.round(p.x - Laya.stage.width / this.worldTileMap.scale / 2);
        p.y = Math.round(p.y - Laya.stage.height / this.worldTileMap.scale / 2);
        return p;
    }

    /**
     * 根据物件逻辑坐标计算锚点像素位置（item中心点）
     * @param logicX 逻辑坐标(游戏坐标)x
     * @param logicY 逻辑坐标(游戏坐标)y
     * @param itemSize: 物品占用 itemSize * itemSize 单元格
     */
    public getItemAnchorPixelPos(logicX: number, logicY: number, itemSize: number = 1): Laya.Point {
        let p: Laya.Point = new Laya.Point(0, 0);
        //中心点需要加上 itemSize * this.halfTileWidth
        p.x = (logicX + itemSize) * Common.TileWidth / 2;
        p.y = (logicY + 1) * Common.TileHeight / 2;
        return p;
    }

    //------------------------------鼠标事件------------------------------
    /**点击方向指示 */
    clickDirectionImge(): void {
        //todo
    }

    /**鼠标click事件 */
    onMapMouseClick(): void {
        //判断是否是移动操作触发的click
        if (this.isMoveClick) {
            return;
        }
        let pos: Laya.Point = this.getLogicPosByStagePos(Laya.stage.mouseX, Laya.stage.mouseY);
        //点击item
        if(WorldItemManager.Instance.clickItem(pos.x, pos.y)){
            return;
        }
    }

    /**鼠标按下事件 */
    onMapMouseDown(e: Laya.Event): void {
        this.isMoveClick = false;
        this.isScaleMap = false;
        var touches: Array<any> = e.touches;
        if (touches && touches.length == 2) {
            this.isScaleMap = true;
            this.lastDistance = TiledUtil.getEventDistance2(touches);
        } else if (!touches || touches.length == 1) {
            this.mLastMouseX = Laya.stage.mouseX;
            this.mLastMouseY = Laya.stage.mouseY;
        } else {
            return;
        }
        Laya.stage.on(Laya.Event.MOUSE_UP, this, this.onMouseUp);
        Laya.stage.on(Laya.Event.MOUSE_OUT, this, this.onMouseUp);
        Laya.stage.on(Laya.Event.MOUSE_MOVE, this, this.onMouseMove);
    }
    /**鼠标事件MouseUp */
    onMouseUp(): void {
        Laya.stage.off(Laya.Event.MOUSE_MOVE, this, this.onMouseMove);
        Laya.stage.off(Laya.Event.MOUSE_UP, this, this.onMouseUp);
        Laya.stage.off(Laya.Event.MOUSE_OUT, this, this.onMouseUp);

        if (this.isScaleMap) {
            return;
        }
        let moveX = Laya.stage.mouseX - this.mLastMouseX;
        let moveY = Laya.stage.mouseY - this.mLastMouseY;
        if (moveX == 0 && moveY == 0) {
            return;
        }
        this.mapX = this.mapX - moveX / this.worldTileMap.scale;
        this.mapY = this.mapY - moveY / this.worldTileMap.scale;

    }
    /**鼠标移动事件 */
    onMouseMove(e: Laya.Event): void {
        if (this.isScaleMap) {
            let distance: number = TiledUtil.getEventDistance2(e.touches);
            const factor: number = 0.001;
            let scaleValue: number = (distance - this.lastDistance) * factor;
            let resultScale: number = this.worldTileMap.scale + scaleValue;
            if (resultScale < Common.MinMapScale) {
                resultScale = Common.MinMapScale;
            } else if (resultScale > Common.MaxMapScale) {
                resultScale = Common.MaxMapScale;
            }
            this.worldTileMap.scale = resultScale;

            //缩放同步maxmapX和maxmapY
            this.nMaxMapX = this.worldTileMap.width - Laya.stage.width / this.worldTileMap.scale;
            this.nMaxMapY = this.worldTileMap.height - Laya.stage.height / this.worldTileMap.scale;
            this.lastDistance = distance;
            this.isMoveClick = true;
            //todo缩放后处理
        } else {
            let moveX = (Laya.stage.mouseX - this.mLastMouseX) / this.worldTileMap.scale;
            let moveY = (Laya.stage.mouseY - this.mLastMouseY) / this.worldTileMap.scale;
            let tmpMapX = this.mapX - moveX;
            let tmpMapY = this.mapY - moveY;
            if (Math.abs(moveX) > 10 || Math.abs(moveY) > 10) {
                this.isMoveClick = true;
            }
            tmpMapX = tmpMapX < this.nMinMapX ? this.nMinMapX : tmpMapX;
            tmpMapX = tmpMapX > this.nMaxMapX ? this.nMaxMapX : tmpMapX;
            tmpMapY = tmpMapY < this.nMinMapY ? this.nMinMapY : tmpMapY;
            tmpMapY = tmpMapY > this.nMaxMapY ? this.nMaxMapY : tmpMapY;
            this.worldTileMap.moveViewPort(tmpMapX, tmpMapY);
        }
        this.updateContainerLocation();
        WorldItemManager.Instance.checkFetchPvpCitys();
        // this.showDirectionImage(moveX, moveY);
    }
}