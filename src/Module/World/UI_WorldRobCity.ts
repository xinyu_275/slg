import { ui } from "../../ui/layaMaxUI";
import EventManager from "../../Common/Core/EventManager";

export default class UI_WorldRobCity extends ui.Panel.World.WorldRobCityUI {
    constructor(){
        super();
        this.isPopupCenter = false;
        this.isShowEffect = false;
    }
    
    onEnable():void {

    }
    onDisable(): void {
        EventManager.Instance.offAllCaller(this);
    }
}
