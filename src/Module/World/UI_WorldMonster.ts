import { ui } from "../../ui/layaMaxUI";
import EventManager from "../../Common/Core/EventManager";
import { ESceneName } from "../../Config/EnumConfig";
import WorldItem from "./WorldItem";
import { EWorldCityType, MonsterMinLv, MonsterMaxLv } from "../../Config/ConfigWorld";
import WorldConfigManager from "../../Config/Data/WorldConfigManager";
import PlayerManager from "../Player/PlayerManager";
import UI_RewardList from "../Script/UI_RewardList";

//怪物
export default class UI_WorldMonster extends ui.Panel.World.WorldMonsterUI {
    private logicX:number = 0;
    private logicY:number = 0;
    private _lv:number = 0;
    private type:number = 0;
    constructor(){
        super();
        this.isPopupCenter = false;
        this.isShowEffect = false;
        this.btn_attack.on(Laya.Event.CLICK, this, this.onAttack);
        this.btn_share.on(Laya.Event.CLICK, this, this.onShare);
        this.btn_save.on(Laya.Event.CLICK, this, this.onSave);
    }
    onEnable(): void {
        EventManager.Instance.on(ESceneName.UI_WorldMonster, this, this.init);
    }
    onDisable(): void {
        EventManager.Instance.offAllCaller(this);
    }

    /**普通怪物等级会随着玩家lv_mon_refresh 而变化*/
    set lv(lv:number){
        if(this.isNormalMon(this.type)) {
            this._lv = lv + PlayerManager.Instance.mainPlayer.lv_mon_refresh;
            this._lv = this._lv < MonsterMinLv?MonsterMinLv:this._lv;
            this._lv = this._lv > MonsterMaxLv?MonsterMaxLv:this._lv;
        }else {
            this._lv = lv;
        }
    }
    get lv():number {
        return this._lv;
    }

    /**初始化 */
    init(item: WorldItem): void {
        if (!item || (!this.isNormalMon(item.vo.type) && item.vo.type != EWorldCityType.EliteMonster)) {
            this.close();
            return;
        }
        let vo = item.vo;
        this.type = vo.type;
        this.lv = vo.lv;
        this.logicX = vo.x;
        this.logicY = vo.y;

        let baseCityInfo = WorldConfigManager.Instance.getBaseCityTypeInfo(this.type);
        this.img_icon.skin = baseCityInfo.icon;
        this.label_name.text = baseCityInfo.name;
        this.label_lv.text = this.lv + "";
        this.label_power.text = WorldConfigManager.Instance.getPvpArmyPower(this.type, this.lv) + "";
        this.label_time.text = "00:00:20";

        //初始化奖励
        let listRewardGoods = [];
        for(let goodsBaseId of WorldConfigManager.Instance.getPvpArmyRewards(this.type, this.lv)) {
            listRewardGoods.push({goods_base_id:goodsBaseId});
        }
        (this.list_reward as UI_RewardList).init(listRewardGoods);
    }

    isNormalMon(type:number):boolean {
        return type == EWorldCityType.Monster1 || type == EWorldCityType.Monster2 || type == EWorldCityType.Monster3;
    }

    //-----------------------------事件-----------------------------
    /**攻击 */
    onAttack():void {

    }

    /**分享 */
    onShare():void {

    }

    /**收藏 */
    onSave():void {

    }
}