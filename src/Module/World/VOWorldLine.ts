import Util from "../../Common/Core/Util";

export default class VOWorldLine {
    public armyId: number = 0;
    public startX: number = 0;
    public startY: number = 0;
    public toX: number = 0;
    public toY: number = 0;
    public startTime: number = 0;
    public arriveTime: number = 0;
    public playerId?: number = 0;
    public op?: number = 0;        // 1.去攻击 2.返回中
    public start_city_type: number;//起始点城池类型
    public to_city_type: number;//目标点城池类型
    public nickname?: string = "";

    //字节数组转化成line
    public init(bytes1: Uint8Array) {
        //底层bug，slice后bytes.buff才会正常
        let bytes = bytes1.slice(0);
        let dv: DataView = new DataView(bytes.buffer);
        let start: number = 0;
        this.armyId = dv.getUint32(start);
        this.startX = dv.getUint16(start + 4);
        this.startY = dv.getUint16(start + 6);
        this.toX = dv.getUint16(start + 8);
        this.toY = dv.getUint16(start + 10);
        this.startTime = dv.getUint32(start + 12);
        this.arriveTime = dv.getUint32(start + 16);
        this.playerId = dv.getUint32(start + 20);
        this.op = dv.getUint8(start + 24);
        this.start_city_type = dv.getUint8(start + 25);
        this.to_city_type = dv.getUint8(start + 26);
        this.nickname = Util.byteToString(bytes.subarray(27));
    }

    /**
     * 根据uint8array获取armyId
     */
    public static getArmyId(bytes1: Uint8Array): number {
        let bytes = bytes1.slice(0);
        let dv: DataView = new DataView(bytes.buffer);
        let start: number = 0;
        return dv.getUint32(start);
    }
}