import Singleton from "../../Common/Core/Singleton";
import WorldItem from "./WorldItem";
import EventManager from "../../Common/Core/EventManager";
import WorldManager from "./WorldManager";
import Util from "../../Common/Core/Util";
import NetSocketManager from "../../Common/NetSocketManager";
import { EPoolName, ESceneName } from "../../Config/EnumConfig";
import WorldConfigManager from "../../Config/Data/WorldConfigManager";
import SceneManager from "../../Common/SceneManager";
import { EWorldCityType } from "../../Config/ConfigWorld";
import Common from "../../Config/Common";

export default class WorldItemManager extends Singleton {
    //城池信息map,key=x*10000+y
    public cityItems: { [key: number]: WorldItem } = {};
    //因为有城池大小的概念，这里做个格子映射->锚点格子上(现在定在左上方)
    public mPoss: { [key: number]: number } = {};

    private lastSendCenterBlock: Laya.Point = new Laya.Point(0, 0); //最后一次发送c_get_pvp_citys时的中心点坐标
    private lastRecvCenterBlock: Laya.Point = new Laya.Point(0, 0); //最后一次接受到的s_get_pvp_citys时的中心点坐标

    constructor() {
        super();
    }
    //实例
    public static get Instance(): WorldItemManager {
        return WorldItemManager.getInstance<WorldItemManager>();
    }
    /**场景进入 */
    scene_enter(): void {
        this.lastRecvCenterBlock.setTo(0, 0);
        this.lastSendCenterBlock.setTo(0, 0);
        this.cityItems = {};

        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_pvp_citys, this, this.s_get_pvp_citys);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_del_city, this, this.s_del_city);
        //NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_pvp_fight, this, this.s_pvp_fight);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_refresh_city_info, this, this.s_refresh_city_info);

    }
    /**场景退出 */
    scene_exit(): void {
        EventManager.Instance.offAllCaller(this);
        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_get_pvp_citys, this.s_get_pvp_citys);
        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_del_city, this.s_del_city);
        //NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_pvp_fight, this.s_pvp_fight);
        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_refresh_city_info, this.s_refresh_city_info);

        //清除城池数据 
        for (let key in this.cityItems) {
            this.cityItems[key].recover();
        }
        this.cityItems = {};
        this.mPoss = {};

        //清除pool
        let objs: WorldItem[] = Laya.Pool.getPoolBySign(EPoolName.WorldItem);
        for (let item of objs) {
            item.destroy();
        }
        Laya.Pool.clearBySign(EPoolName.WorldItem);
    }

    /**组装成worlditem */
    spawnWorldItem(cityInfo: awesomepackage.Ip_city_info, isCover: boolean = false): void {
        let virutalId: number = this.makeItemVirtual(cityInfo.x, cityInfo.y);
        //存在那就不要处理
        let oldCityItem = this.cityItems[virutalId];
        if (!oldCityItem) {
            this.spawnWorldItemNew(cityInfo)
        } else if (isCover || cityInfo.type != oldCityItem.vo.type) {
            //删除在添加
            this.delWorldItem(oldCityItem);
            this.spawnWorldItemNew(cityInfo);
        }
    }
    spawnWorldItemNew(cityInfo: awesomepackage.Ip_city_info): void {
        let virutalId: number = this.makeItemVirtual(cityInfo.x, cityInfo.y);
        let item = Laya.Pool.getItemByClass(EPoolName.WorldItem, WorldItem);
        item.init(cityInfo);
        this.cityItems[virutalId] = item;
        this.addMapPos(item);
    }

    //删除城池
    delWorldItem(item: WorldItem) {
        this.delMapPos(item);
        let virutalId: number = this.makeItemVirtual(item.vo.x, item.vo.y);
        delete this.cityItems[virutalId];
        item.recover();
    }

    /**强制删除过期的item（非九宫格范围内） */
    clearWorldItems(forceClearAll: boolean = false) {
        let block: Laya.Point = this.lastRecvCenterBlock;
        for (let index in this.cityItems) {
            let item: WorldItem = this.cityItems[index];
            if (Math.abs(item.nMapBlockX - block.x) <= 1 && Math.abs(item.nMapBlockY - block.y) <= 1) {
                continue;
            }
            this.delWorldItem(item);
        }
    }

    /** 返回某个位置的城池*/
    getWorldItemByPos(x: number, y: number): WorldItem {
        //城池占用格子>1也需要处理
        let virutalId = this.mPoss[this.makeItemVirtual(x, y)];
        return this.cityItems[virutalId];
    }

    /**为城池坐标x,y添加虚拟id */
    makeItemVirtual(x: number, y: number): number {
        return x * 10000 + y;
    }

    /**添加映射关系 */
    addMapPos(item: WorldItem): void {
        let size = WorldConfigManager.Instance.getCitySize(item.vo.type);
        let baseVPos = this.makeItemVirtual(item.vo.x, item.vo.y);
        let vposs = this.getCitySizeVirtualList(item.vo.x, item.vo.y, size);
        for (let vpos of vposs) {
            this.mPoss[vpos] = baseVPos;
        }
    }
    /**删除映射关系 */
    delMapPos(item: WorldItem): void {
        let size = WorldConfigManager.Instance.getCitySize(item.vo.type);
        let vposs = this.getCitySizeVirtualList(item.vo.x, item.vo.y, size);
        for (let vpos of vposs) {
            delete this.mPoss[vpos];
        }
    }
    /**获取城池大小占用的虚拟id */
    getCitySizeVirtualList(x: number, y: number, size: number): number[] {
        let resust: number[] = [];
        if (size == 1) {
            resust.push(this.makeItemVirtual(x, y));
        } else if (size == 2) {
            //[{X, Y}, {X+1, Y+1}, {X+2, Y}, {X+1, Y-1}];
            resust.push(this.makeItemVirtual(x, y));
            resust.push(this.makeItemVirtual(x + 1, y + 1));
            resust.push(this.makeItemVirtual(x + 2, y));
            resust.push(this.makeItemVirtual(x + 1, y - 1));
        } else if (size == 3) {
            //[{X, Y}, {X+1, Y+1}, {X+2, Y}, {X+1, Y-1}, {X+2, Y+2}, {X+3, Y+1}, {X+2, Y-2}, {X+3,Y-1}, {X+4, Y}].
            resust.push(this.makeItemVirtual(x, y));
            resust.push(this.makeItemVirtual(x + 1, y + 1));
            resust.push(this.makeItemVirtual(x + 2, y));
            resust.push(this.makeItemVirtual(x + 1, y - 1));
            resust.push(this.makeItemVirtual(x + 2, y + 2));
            resust.push(this.makeItemVirtual(x + 3, y + 1));
            resust.push(this.makeItemVirtual(x + 2, y - 2));
            resust.push(this.makeItemVirtual(x + 3, y - 1));
            resust.push(this.makeItemVirtual(x + 4, y));
        }
        return resust;
    }

    //----------------------协议处理---------------------------
    //判断是否发送协议获取城池列表
    //规则：
    // A:this.last_recv_center_block;
    // B:this.last_send_center_block
    // C:当前客户端中心block位置
    // 1.B == C 不需要请求协议
    // 2.A == B 那要判断C是否是在九宫格范围来决定force
    // 3.其他发送force
    checkFetchPvpCitys() {
        let curCenterBlock: Laya.Point = WorldManager.Instance.getCurCenterBlockPos();
        if (Util.checkPointEqual(this.lastSendCenterBlock, curCenterBlock)) {
            return;
        }
        let forceFullRefresh: boolean = true;
        if (this.lastSendCenterBlock.x != 0 || this.lastSendCenterBlock.y != 0) {
            if (Util.checkPointEqual(this.lastSendCenterBlock, this.lastRecvCenterBlock)) {
                //判断当前位置是否在九宫格范围内
                let diffBlockX: number = Math.abs(this.lastSendCenterBlock.x - curCenterBlock.x);
                let diffBlockY: number = Math.abs(this.lastSendCenterBlock.y - curCenterBlock.y);
                if (diffBlockX <= 1 && diffBlockY <= 1) {
                    forceFullRefresh = false;
                }
            }
        }
        this.c_get_pvp_citys(curCenterBlock.x, curCenterBlock.y, forceFullRefresh);
    }
    /**获取pvp城市列表 */
    c_get_pvp_citys(CenterBlockX: number, CenterBlockY: number, ForceFullRefresh: boolean = true): void {
        if (!WorldManager.Instance.mapLoaded) return;
        this.lastSendCenterBlock.x = CenterBlockX;
        this.lastSendCenterBlock.y = CenterBlockY;
        let message = { "center_block_x": CenterBlockX, "center_block_y": CenterBlockY, "force_full_refresh": ForceFullRefresh };
        NetSocketManager.Instance.SendMsg("c_get_pvp_citys", message);
    }
    /**城池数据返回*/
    s_get_pvp_citys(data: awesomepackage.s_get_pvp_citys): void {
        if (!WorldManager.Instance.mapLoaded) return;
        //过期数据
        if (data.center_block_x != this.lastSendCenterBlock.x || data.center_block_y != this.lastSendCenterBlock.y) {
            return;
        }
        //保存收到消息时刻的中心点block坐标
        this.lastRecvCenterBlock.x = data.center_block_x;
        this.lastRecvCenterBlock.y = data.center_block_y;
        //1.清除不在九宫格的城池
        this.clearWorldItems();
        //2.增加城池
        for (let index = 0; index < data.citys.length; index++) {
            this.spawnWorldItem(data.citys[index]);
        }
    }
    /**城池更新 */
    s_refresh_city_info(data: awesomepackage.s_refresh_city_info): void {
        if (!WorldManager.Instance.mapLoaded) return;
        let cityInfo: awesomepackage.Ip_city_info = data.city_info;
        this.spawnWorldItem(cityInfo, true);
    }
    /**删除城池 */
    s_del_city(data: awesomepackage.s_del_city): void {
        if (!WorldManager.Instance.mapLoaded) return;
        let virutalId: number = this.makeItemVirtual(data.x, data.y);
        let item: WorldItem = this.cityItems[virutalId];
        if (item) {
            this.delWorldItem(item);
        }
    }

    //--------------------------------------点击城池---------------------------------
    /**点击城池处理*/
    clickItem(x: number, y: number): boolean {
        let item = this.getWorldItemByPos(x, y);
        if (!item) {
            return false; 
        }
        switch(item.vo.type) {
            case EWorldCityType.Monster1:
            case EWorldCityType.Monster2:
            case EWorldCityType.Monster3:
            case EWorldCityType.EliteMonster:
                SceneManager.Instance.openScene(ESceneName.UI_WorldMonster, [item], false, Laya.Handler.create(this, this.clickItemCallback, [item]));
                break;
            case EWorldCityType.Main_City_Normal:
            case EWorldCityType.Main_City_Appear:
            case EWorldCityType.LongCheng:
                SceneManager.Instance.openScene(ESceneName.UI_WorldPlayerCity, [item], false, Laya.Handler.create(this, this.clickItemCallback, [item]));
                break;
        }
        return true;
    }
    //处理显示位置
    clickItemCallback(item:WorldItem, dialog:Laya.Dialog):void {
        if(!item || !dialog) {
            return;
        }
        let vo = item.vo;
        let scale = WorldManager.Instance.worldTileMap.scale;
        const checkX = 100;
        const checkY = 100;
        const fix = 20;
        const dialogWidth: number = dialog.width;
        const dialogHeight: number = dialog.height;
        const stageWidth = Laya.stage.width;
        const stageHeight = Laya.stage.height;
        const itemHalfWidth = Common.TileWidth * item.citySize / 2 * scale;
        const itemHalfHeight = Common.TileWidth * item.citySize / 2 * scale;
        
        //计算物件的锚点（中心点）
        let itemCenterAnchorPos = WorldManager.Instance.getItemAnchorPixelPos(vo.x, vo.y, item.citySize);
        //把itemCenterPos映射到屏幕坐标
        let itemCenterScreenX:number = (itemCenterAnchorPos.x - WorldManager.Instance.mapX) * scale;
        let itemCenterScreenY:number = (itemCenterAnchorPos.y - WorldManager.Instance.mapY) * scale;
        //物件在很左边，只要上下方有足够的空间，那就显示在右边
        if (itemCenterScreenX < checkX && itemCenterScreenY - dialogHeight / 2 >= checkY && stageHeight - itemCenterScreenY - dialogHeight / 2 >= checkY) {
            //显示在正右边
            dialog.x = itemCenterScreenX + itemHalfWidth + fix;
            dialog.y = itemCenterScreenY - dialogHeight / 2;
            return;
        }
        //物件很靠右，只要上下方有足够的空间，那就显示在左
        if (stageWidth - itemCenterScreenX < checkX && itemCenterScreenY - dialogHeight / 2 >= checkY && stageHeight - itemCenterScreenY - dialogHeight / 2 >= checkY) {
            //显示在左边
            dialog.x = itemCenterScreenX - itemHalfWidth - dialogWidth - fix;
            dialog.y = itemCenterScreenY - dialogHeight / 2;
            return;
        }
        if (itemCenterScreenY >= stageHeight / 2) {
            //显示在物件上面
            dialog.y = itemCenterScreenY - itemHalfHeight - dialogHeight;
        } else {
            //显示在物件下面
            dialog.y = itemCenterScreenY + itemHalfHeight;
        }
        if (itemCenterScreenX <= dialogWidth / 2) {
            //最左边
            dialog.x = fix;
        } else if (stageWidth - itemCenterScreenX < dialogWidth/2) {
            //最后边
            dialog.x = stageWidth - dialogWidth - fix;
        } else {
            dialog.x = itemCenterScreenX - dialogWidth/2;
        }
    }

}