import VOWorldLine from "./VOWorldLine";
import PlayerManager from "../Player/PlayerManager";
import WorldManager from "./WorldManager";
import WorldConfigManager from "../../Config/Data/WorldConfigManager";
import Util from "../../Common/Core/Util";
import { EPoolName, EAniPath } from "../../Config/EnumConfig";
import TimeManager from "../../Common/Core/TimeManager";
import AniManager from "../../Common/AniManager";

export default class WorldLine {
    public vo: VOWorldLine;
    private color: string = "green"; //颜色
    private totalTime: number = 0;  //总时间
    private startPos: Laya.Point;
    private toPos: Laya.Point;
    private toPosSize: number = 1;
    private lineDis: number = 0;     //连线距离
    private lineAngle: number = 0;   //连线角度
    private isArrive: boolean = false;  // 是否已经到达
    private animSpeed: number = 0;      // 动画速度
    //控件
    private sp_line: Laya.Sprite = null;         // 连线
    private ani_fight: Laya.Animation = null;    // 攻击动画
    private ani_army: Laya.Animation = null;     // 连线部队动画
    private sp_army:Laya.Sprite = null;          // army的父节点
    constructor() {
        this.sp_line = new Laya.Sprite();
        this.sp_army = new Laya.Sprite();
    }
    //初始化
    init(vo: VOWorldLine) {
        this.vo = vo;
        //攻击操作多2s的动画表现
        this.vo.op == 1 && (this.vo.arriveTime -= 1);
        this.isArrive = false;
        this.color = "#FFFF00";
        if (this.vo.playerId === PlayerManager.Instance.mainPlayer.id) {
            this.color = "#00FF00";
        }
        //已经计算好了中心点
        this.startPos = WorldManager.Instance.getItemAnchorPixelPos(this.vo.startX, this.vo.startY, WorldConfigManager.Instance.getCitySize(this.vo.start_city_type));
        this.toPos = WorldManager.Instance.getItemAnchorPixelPos(this.vo.toX, this.vo.toY, WorldConfigManager.Instance.getCitySize(this.vo.to_city_type));
        this.lineDis = Util.getDistance(this.startPos, this.toPos);
        this.lineAngle = Util.getAngle(this.startPos, this.toPos);
        this.totalTime = this.vo.arriveTime - this.vo.startTime;
        if (this.totalTime > 0) {
            this.animSpeed = this.lineDis / this.totalTime;
        }
        //划线
        this.sp_line.graphics.clear();
        this.sp_line.graphics.drawLine(this.startPos.x, this.startPos.y, this.toPos.x, this.toPos.y, this.color);
        WorldManager.Instance.mFightLineSp.addChild(this.sp_line);
    }

    /**回收 */
    recover(): void {
        AniManager.Instance.recover(this.ani_fight);
        AniManager.Instance.recover(this.ani_army);
        if (this.sp_line) {
            this.sp_line.removeSelf();
            this.sp_line.graphics.clear();
        }
        if(this.sp_army){
            //销毁所有子节点
            this.sp_army.removeSelf();
        }
        Laya.Pool.recover(EPoolName.WorldLine, this);
        this.ani_fight = null;
        this.ani_army = null;

    }

    /**销毁 */
    destroy(): void {
        this.sp_line && this.sp_line.destroy();
        this.sp_army && this.sp_army.destroy();
    }

    //-------------------------动画------------------------
    /**更新动画信息 */
    updateAnim(): void {
        if (this.lineDis <= 0 || this.totalTime <= 0 || this.isArrive) {
            return;
        }
        //到达目的地
        if (TimeManager.Instance.serverTime >= this.vo.arriveTime) {
            if (this.vo.op == 1) {
                this.playerFightAni();
            }
            //this.stopAnim();
            this.isArrive = true;
            return;
        }
        if(!this.ani_army) {
            this.createAniArmy();
        }
        //获取动画位置
        let curAnimPos: Laya.Point = this.getCurAnimPos();
        this.sp_army.pos(curAnimPos.x, curAnimPos.y);
    }

    /**获取当前动画的位置 */
    private getCurAnimPos(): Laya.Point {
        let curAnimPos: Laya.Point = new Laya.Point();
        if (TimeManager.Instance.serverTime >= this.vo.arriveTime) {
            curAnimPos = this.toPos;
        } else {
            let moveLength: number = this.animSpeed * (TimeManager.Instance.serverMillTime - this.vo.startTime * 1000) / 1000;
            let moveXLength: number = Math.cos(this.lineAngle * (Math.PI / 180)) * moveLength;
            let moveYLength: number = Math.sin(this.lineAngle * (Math.PI / 180)) * moveLength;
            curAnimPos.x = this.startPos.x + moveXLength;
            curAnimPos.y = this.startPos.y + moveYLength;
        }
        return curAnimPos;
    }

    /**创建aniArmy */
    createAniArmy():void {
        let angle = Util.getAngle(this.startPos, this.toPos);
        angle < 0 && (angle += 360);
        let index = angle / 45;
        if(index >= 7.5) index = 0;
        index = Math.round(index) + 1;
        let atlasPath = "res/atlas/image/worldMap/armyAni/0" + index+".atlas";
        Laya.loader.load(atlasPath, Laya.Handler.create(this, this.doCreateArmyAnim, [atlasPath]));
    }
    doCreateArmyAnim(atlasPath:string):void {
        if(this.ani_army) return;
        this.ani_army = AniManager.Instance.newAnimation(true);
        this.ani_army.loadAtlas(atlasPath, null, atlasPath);
        this.ani_army.pivot(150, 150);
        this.ani_army.play();
        this.sp_army.addChild(this.ani_army);
        WorldManager.Instance.mArmySp.addChild(this.sp_army);
    }

    /**攻击动画 */
    playerFightAni(): void {
        this.ani_fight = AniManager.Instance.newAnimation();
        this.ani_fight.loadAtlas(EAniPath.WorldFight, null, EAniPath.WorldFight);
        this.ani_fight.pivot(250 / 2, 220 / 2);
        this.ani_fight.pos(this.toPos.x, this.toPos.y);
        this.ani_fight.interval = 120;
        let toCitySize = WorldConfigManager.Instance.getCitySize(this.vo.to_city_type);
        this.ani_fight.scale(toCitySize, toCitySize);
        WorldManager.Instance.mArmySp.addChild(this.ani_fight);
        this.ani_fight.play();
    }
}