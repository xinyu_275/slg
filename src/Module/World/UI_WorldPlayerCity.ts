import { ui } from "../../ui/layaMaxUI";
import EventManager from "../../Common/Core/EventManager";
import WorldItem from "./WorldItem";

export default class UI_WorldPlayerCity extends ui.Panel.World.WorldPlayerCityUI {
    constructor(){
        super();
        this.isPopupCenter = false;
        this.isShowEffect = false;
        this.btn_enter.on(Laya.Event.CLICK, this, this.onEnter);
        this.btn_visit.on(Laya.Event.CLICK, this, this.onVisit);
        this.btn_shield.on(Laya.Event.CLICK, this, this.onShield);
        this.btn_save.on(Laya.Event.CLICK, this, this.onSave);
        this.btn_share.on(Laya.Event.CLICK, this, this.onShare);
        this.btn_appear.on(Laya.Event.CLICK, this, this.onAppear);
        this.btn_attack.on(Laya.Event.CLICK, this, this.onAttack);
        this.btn_detective.on(Laya.Event.CLICK, this, this.onDetective);
        this.btn_gather.on(Laya.Event.CLICK, this, this.onGather);
    }
    onEnable():void {

    }
    onDisable(): void {
        EventManager.Instance.offAllCaller(this);
    }
    
    /**初始化 */
    init(item: WorldItem): void {
    }

    //--------------------------------事件----------------------
    //进入
    onEnter():void{
    }
    //拜访
    onVisit():void {
    }
    //护盾
    onShield():void{
    }
    //收藏
    onSave():void {
    }
    //分享
    onShare():void {
    }
    //外观
    onAppear():void {
    }
    //攻击
    onAttack():void {
    }
    //侦查
    onDetective():void {
    }
    //集结
    onGather():void {
    }
}