import Common from "../../Config/Common";
import WorldConfigManager from "../../Config/Data/WorldConfigManager";
import WorldManager from "./WorldManager";
import { EPoolName, EAniPath } from "../../Config/EnumConfig";
import Util from "../../Common/Core/Util";
import PlayerManager from "../Player/PlayerManager";
import AniManager from "../../Common/AniManager";
import TimeManager from "../../Common/Core/TimeManager";
import { EWorldCityType, MonsterMinLv, MonsterMaxLv } from "../../Config/ConfigWorld";

export default class WorldItem {
    public vo: awesomepackage.Ip_city_info = null;
    public nMapBlockX: number;
    public nMapBlockY: number;
    public citySize: number = 1;     // 城池大小
    private itemAnchorPos: Laya.Point = new Laya.Point(0, 0);

    private img_city: Laya.Image = null;      // 城池图片
    private img_nameBg: Laya.Image = null;    // 城池名字背景色
    private img_lvBg: Laya.Image = null;      // 城池等级背景色
    private label_name: Laya.Label = null;    // 城池名字
    private label_lv: Laya.Label = null;      // 城池等级
    private ani_fire: Laya.Animation = null;   // 失火动画
    private ani_protect: Laya.Animation = null;  // 保护罩

    constructor() {
        //创建需要的控件
        this.img_city = new Laya.Image();
        this.img_city.anchorX = 0.5;
        this.img_city.anchorY = 0.5;

        this.img_nameBg = new Laya.Image("image/worldMap/label_bg.png");
        this.img_nameBg.anchorX = 0.5;
        this.img_nameBg.anchorY = 0.5;
        this.img_nameBg.pivot(0.5, 0.5);
        this.img_nameBg.alpha = 0.5;

        this.img_lvBg = new Laya.Image("image/worldMap/label_bg.png");
        this.img_lvBg.anchorX = 0.5;
        this.img_lvBg.anchorY = 0.5;
        this.img_lvBg.zOrder = 1;
        this.img_lvBg.alpha = 0.5;

        this.label_name = new Laya.Label();
        this.label_name.width = 160;
        this.label_name.anchorX = 0.5;
        this.label_name.anchorY = 0.5;
        this.label_name.font = "Arial";
        this.label_name.fontSize = 20;
        this.label_name.color = "#53b2da";
        this.label_name.align = "center";
        this.label_name.valign = "middle";
        this.label_name.zOrder = 2;

        this.label_lv = new Laya.Label();
        this.label_lv.width = 160;
        this.label_lv.anchorX = 0.5;
        this.label_lv.anchorY = 0.5;
        this.label_lv.font = "Arial";
        this.label_lv.fontSize = 24;
        this.label_lv.bold = true;
        this.label_lv.color = "#ffb03f";
        this.label_lv.align = "center";
        this.label_lv.valign = "middle";
        this.label_lv.zOrder = 2;
    }

    /**初始化 */
    init(vo: awesomepackage.Ip_city_info): void {
        this.vo = vo;
        this.nMapBlockX = Math.floor(this.vo.x / Common.MapBlockGridWidth);
        this.nMapBlockY = Math.floor(this.vo.y / Common.MapBlockGridHeight);
        this.citySize = WorldConfigManager.Instance.getCitySize(vo.type);
        this.itemAnchorPos = WorldManager.Instance.getItemAnchorPixelPos(vo.x, vo.y, this.citySize);

        //控件处理 重置属性
        this.img_lvBg.visible = true;
        this.label_lv.visible = true;
        this.label_name.color = "#53b2da";
        this.label_lv.color = "#ffb03f";
        switch (vo.type) {
            case EWorldCityType.Monster1:
            case EWorldCityType.Monster2:
            case EWorldCityType.Monster3:
                this.init_mon(true);
                break;
            case EWorldCityType.EliteMonster:
                this.init_mon(false);
                break;
            case EWorldCityType.Main_City_Normal:
            case EWorldCityType.Main_City_Appear:
            case EWorldCityType.LongCheng:
                this.init_main_city(vo);
                break;
            case EWorldCityType.GuildLand:
                this.init_guild_land(vo);
                break;
        }
        //img_city
        this.img_city.skin = WorldConfigManager.Instance.getCityIcon(vo.type);
        this.img_city.pos(this.itemAnchorPos.x, this.itemAnchorPos.y);
        WorldManager.Instance.mCitysSp.addChild(this.img_city);

        let charWidth = 12;
        //label_name
        this.label_name.x = this.itemAnchorPos.x;
        this.label_name.y = this.itemAnchorPos.y + 20;
        this.img_nameBg.width = Util.getLength(this.label_name.text) * charWidth;
        this.img_nameBg.x = this.label_name.x;
        this.img_nameBg.y = this.label_name.y;
        WorldManager.Instance.mCityLabelsSp.addChild(this.img_nameBg);
        WorldManager.Instance.mCityLabelsSp.addChild(this.label_name);

        //label_lv
        if (this.label_lv.visible) {
            this.label_lv.x = this.itemAnchorPos.x;
            this.label_lv.y = this.itemAnchorPos.y + 40;
            this.img_lvBg.width = Util.getLength(this.label_lv.text) * charWidth;
            this.img_lvBg.x = this.label_lv.x;
            this.img_lvBg.y = this.label_lv.y;
            WorldManager.Instance.mCityLabelsSp.addChild(this.img_lvBg);
            WorldManager.Instance.mCityLabelsSp.addChild(this.label_lv);
        }
    }
    /**设置怪物 */
    init_mon(IsNormalMon: boolean): void {
        let baseCityInfo = WorldConfigManager.Instance.getBaseCityTypeInfo(this.vo.type);
        this.label_name.text = baseCityInfo.name;
        if (IsNormalMon) {
            let lv: number = this.vo.lv + PlayerManager.Instance.mainPlayer.lv_mon_refresh;
            lv = lv < MonsterMinLv ? MonsterMinLv : lv;
            lv = lv > MonsterMaxLv ? 70 : MonsterMaxLv;
            this.label_lv.text = "LV." + lv;
        } else {
            this.label_lv.text = "LV." + this.vo.lv;
        }
    }

    /**设置玩家城池 */
    init_main_city(vo: awesomepackage.Ip_city_info): void {
        //自己的主城
        let player = PlayerManager.Instance.mainPlayer;
        if (vo.player_id == player.id) {
            this.label_name.text = PlayerManager.Instance.mainPlayer.nickname + "(" + player.x + "," + player.y + ")";
            this.img_lvBg.visible = false;
            this.label_lv.visible = false;
            this.label_name.color = "#00ff00";
        } else {
            this.label_lv.text = "LV." + vo.lv;
            let nickname = Util.cutOutString(Util.byteToString(vo.nickname), 8);
            if (vo.guild_name) {
                this.label_name.text = "[" + Util.byteToString(vo.guild_name) + "]" + nickname;
            }
        }
        //失火动画
        if (vo.state === awesomepackage.e_city_state.fire || vo.state === awesomepackage.e_city_state.smoking) {
            this.createAniFire();
        }
        if (vo.protect_end_time > TimeManager.Instance.serverTime) {
            this.createAniProtect();
        }
    }
    /**失火动画 */
    createAniFire(): void {
        this.ani_fire = AniManager.Instance.newAnimation(true);
        this.ani_fire.loadAtlas(EAniPath.WorldFire, null, EAniPath.WorldFire);
        this.ani_fire.zOrder = 2;
        this.ani_fire.pos(this.itemAnchorPos.x, this.itemAnchorPos.y);
        this.ani_fire.pivot(110 / 2, 110 / 2);
        this.ani_fire.scale(this.citySize, this.citySize);
        this.ani_fire.play();
        WorldManager.Instance.mCitysSp.addChild(this.ani_fire);
    }
    /**保护罩动画 */
    createAniProtect(): void {
        this.ani_protect = AniManager.Instance.newAnimation(true);
        this.ani_protect.loadAtlas(EAniPath.WorldProtect, null, EAniPath.WorldProtect);
        this.ani_protect.zOrder = 3;
        this.ani_protect.pos(this.itemAnchorPos.x, this.itemAnchorPos.y);
        this.ani_protect.pivot(150 / 2, 150 / 2);
        this.ani_protect.scale(this.citySize, this.citySize);
        this.ani_protect.play();
        WorldManager.Instance.mCitysSp.addChild(this.ani_protect);
    }
    /**血盟领地 */
    init_guild_land(vo: awesomepackage.Ip_city_info): void {
        this.img_lvBg.visible = false;
        this.label_lv.visible = false;
        this.label_name.text = "[" + Util.byteToString(vo.guild_name) + "]" + Util.byteToString(vo.guild_full_name);
    }

    /**物资 */
    init_rob(vo: awesomepackage.Ip_city_info): void {
        this.img_lvBg.visible = false;
        this.label_lv.visible = false;
        this.label_name.text = "物资";
        this.label_name.color = "#ffe79e";
    }

    /**回收 */
    recover(): void {
        this.vo = null;
        this.img_city && this.img_city.removeSelf();
        this.img_nameBg && this.img_nameBg.removeSelf();
        this.img_lvBg && this.img_lvBg.removeSelf();
        this.label_name && this.label_name.removeSelf();
        this.label_lv && this.label_lv.removeSelf();
        Laya.Pool.recover(EPoolName.WorldItem, this);

        //回收动画
        AniManager.Instance.recover(this.ani_fire);
        AniManager.Instance.recover(this.ani_protect);
        this.ani_fire = null;
        this.ani_protect = null;
    }

    /**销毁对象 */
    destroy(): void {
        this.img_city && this.img_city.destroy();
        this.img_nameBg && this.img_nameBg.destroy();
        this.img_lvBg && this.img_lvBg.destroy();
        this.label_name && this.label_name.destroy();
        this.label_lv && this.label_lv.destroy();
    }
}