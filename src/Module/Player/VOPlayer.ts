import Util from "../../Common/Core/Util";

export default class VOPlayer {
    public id: number;
    public nickname: string;        /**玩家名 */
    public avatar_url?: string = "";   /**玩家头像 */
    public gold?: number = 0;       /**玩家金币 */
    public diamond: number = 0;     /**玩家元宝 */
    public x?: number = 0;          /**玩家主城坐标x */
    public y?: number = 0;          /**玩家主城坐标y */
    public lv?: number = 1;         /**玩家等级 */
    public exp?:number = 0;         /**玩家经验 */

    public lv_mon_refresh?:number = 1;  /**怪物刷新时玩家的等级 */

    /**初始化 */
    public init(data:awesomepackage.s_get_player_info):void {
        this.id = data.player_id;
        this.nickname = Util.byteToString(data.nickname);
        this.avatar_url = "";
        this.gold = Number(data.gold);
        this.diamond = data.diamond;
        this.x = data.x;
        this.y = data.y;
        this.lv = data.lv;
        this.exp = Number(data.exp);
        this.lv_mon_refresh = data.lv_mon_refresh;
    }
}