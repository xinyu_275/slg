import Singleton from "../../Common/Core/Singleton";
import AbsGameModule from "../AbsGameModule";
import NetSocketManager from "../../Common/NetSocketManager";
import VOPlayer from "./VOPlayer";
import { EEventType } from "../../Config/EnumConfig";
import LogM from "../../Common/LogM";
import EventManager from "../../Common/Core/EventManager";

export default class PlayerManager extends AbsGameModule {
    public mainPlayer: VOPlayer = null;

    constructor() {
        super();
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_player_info, this, this.s_get_player_info);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_refresh_player_attr, this, this.s_refresh_player_attr);
    }

    public static get Instance(): PlayerManager {
        return PlayerManager.getInstance<PlayerManager>();
    }

    /**发送获取玩家信息协议 */
    c_get_player_info(): void {
        NetSocketManager.Instance.SendMsg("c_get_player_info", {});
    }
    s_get_player_info(data: awesomepackage.s_get_player_info): void {
        let mPlayer = new VOPlayer();
        mPlayer.init(data);
        this.mainPlayer = mPlayer;
    }

    /**刷新玩家信息 */
    s_refresh_player_attr(data: awesomepackage.s_refresh_player_attr): void {
        if (!this.mainPlayer) {
            return;
        }
        let mRefresh: { [key: string]: number } = {};
        let value: number = 0;
        for (let item of data.attrs) {
            value = Number(item.attr_value);
            let et = this.refreshPlayerAttrItem(item.attr_key, value);
            if(et) {
                mRefresh[et] = 1;
            }
        }
        //发送更新信息事件
        for(let et in mRefresh) {
            EventManager.Instance.event(et);
        }
    }
    //刷新属性 返回属性注册事件
    refreshPlayerAttrItem(key: awesomepackage.e_playe_attr_key, value: number): string {
        //玩家属性更新需要发送事件 去更新其他界面
        let attrEventType: EEventType = null;
        switch (key) {
            case awesomepackage.e_playe_attr_key.gold:
                this.mainPlayer.gold = value;
                attrEventType = EEventType.PlayerResUpdate;
                break;
            case awesomepackage.e_playe_attr_key.diamond:
                this.mainPlayer.diamond = value;
                attrEventType = EEventType.PlayerResUpdate;
                break;
            case awesomepackage.e_playe_attr_key.x:
                this.mainPlayer.x = value;
                attrEventType = EEventType.PlayerPosUpdate;
                break;
            case awesomepackage.e_playe_attr_key.y:
                this.mainPlayer.y = value;
                attrEventType = EEventType.PlayerPosUpdate;
                break;
            default:
                LogM.error("refreshPlayerAttrItem not deal", key, value)
        }
        return attrEventType;
    }

    /**gm */
    c_gm(content:string): void {
        NetSocketManager.Instance.SendMsg("c_gm", {content:content});
    }
    
}