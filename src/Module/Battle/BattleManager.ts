import Singleton from "../../Common/Core/Singleton";
import NetSocketManager from "../../Common/NetSocketManager";
import AbsGameModule from "../AbsGameModule";
import { EPrefabPath, ESceneName } from "../../Config/EnumConfig";
import SceneManager from "../../Common/SceneManager";

export default class BattleManager extends AbsGameModule {
    //实例
    public static get Instance(): BattleManager {
        return BattleManager.getInstance<BattleManager>();
    }

    constructor() {
        super();
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_start_battle, this, this.s_start_battle);
    }
    //开启播放战斗录像
    s_start_battle(data: awesomepackage.s_start_battle): void {
        //1.加载prefab资源
        let assets: Array<any> = [
            { url: EPrefabPath.BoxSoldier, type: Laya.Loader.PREFAB },
            { url: EPrefabPath.BattleHp, type: Laya.Loader.PREFAB },
        ];
        Laya.loader.load(assets, Laya.Handler.create(this, this.onLoadedBattleResComplete, [data]));
    }
    onLoadedBattleResComplete(data: awesomepackage.s_start_battle): void {
        SceneManager.Instance.openScene(ESceneName.UI_Battle, [data]);
    }
}