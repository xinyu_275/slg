import { EBattleSide, BattleSidePosMap, BattleAniDuration } from "../../Config/ConfigBattle";
import { EPrefabPath } from "../../Config/EnumConfig";
import SoldierConfigManager, { BaseSoldier } from "../../Config/Data/SoldierConfigManager";

export default class UI_BoxSoldier extends Laya.Box {
    private soldier: awesomepackage.Ip_battle_soldier = null;
    private side: EBattleSide;
    private mBattleHpPrefab:Laya.Prefab = null;

    private img_soldier: Laya.Image = null;
    private img_soldierNumBg: Laya.Image = null;
    private pr_hp: Laya.ProgressBar = null;
    private label_soldierNum: Laya.Label = null;
    private label_hp: Laya.Label = null;

    private curHP: number = 0;           // 当前血量
    private curNum: number = 0;          // 当前士兵数量
    onEnable(): void {
        this.img_soldier = this.getChildByName("img_soldier") as Laya.Image;
        this.img_soldierNumBg = this.getChildByName("img_soldierNumBg") as Laya.Image;
        this.pr_hp = this.getChildByName("pr_hp") as Laya.ProgressBar;
        this.label_soldierNum = this.getChildByName("label_soldierNum") as Laya.Label;
        this.label_hp = this.getChildByName("label_hp") as Laya.Label;
    }
    onDisable(): void {
        this.soldier = null;
    }

    /**回收 */
    recover(): void {
        this.removeSelf();
        Laya.Pool.recover(EPrefabPath.BoxSoldier, this);
    }

    /**初始化兵 */
    init(soldier: awesomepackage.Ip_battle_soldier, side: EBattleSide, mBattleHpPrefab:Laya.Prefab): void {
        this.soldier = soldier;
        this.side = side;
        this.mBattleHpPrefab = mBattleHpPrefab;

        let baseSoldier: BaseSoldier = SoldierConfigManager.Instance.getBaseSoldier(soldier.soldier_base_id);
        this.curHP = Number(soldier.hp_lim);
        this.curNum = soldier.num;
        if (side == EBattleSide.Left) {
            this.img_soldier.skin = baseSoldier.icon;
            this.img_soldierNumBg.skin = "image/battle/ui_soldier_num_blue_bg.png";
            this.pr_hp.skin = "image/comp/pr_green.png";
        } else {
            this.img_soldier.skin = baseSoldier.def_icon;
            this.img_soldierNumBg.skin = "image/battle/ui_soldier_num_red_bg.png";
            this.pr_hp.skin = "image/comp/pr_red.png";
        }
        this.refresh();

        //初始化坐标
        let pos = BattleSidePosMap["pos_" + side + "_" + soldier.pos];
        this.x = pos.x;
        this.y = pos.y;
    }

    /**刷新UI */
    refresh(): void {
        this.pr_hp.value = this.curHP / Number(this.soldier.hp_lim);
        this.label_soldierNum.text = this.curNum + "/" + this.soldier.num;
        this.label_hp.text = this.curHP + "";
    }

    /**自己发起攻击 */
    shot(mPlayMulti:number): void {
        let curX: number = this.x;
        let curY:number = this.y;
        let muti:number = this.side == EBattleSide.Left?-1:1;
        let aniTime = BattleAniDuration / mPlayMulti;
        Laya.Tween.to(this, { x: curX - 50 * muti, y:curY+50*muti }, aniTime, Laya.Ease.quadInOut);
        Laya.Tween.to(this, { x: curX, y:curY}, aniTime, Laya.Ease.quadInOut, null, aniTime);
    }

    /**自己收到伤害,处理掉血 */
    hurt(hurtInfo: awesomepackage.Ip_battle_shot_hurt, shotObj: number, mPlayMulti:number): void {
        if(hurtInfo.num == 0) {
            this.recover();
            return;
        }
        this.curNum = hurtInfo.num;
        this.curHP = Number(hurtInfo.hp);
        let curX: number = this.x;
        let muti:number = this.side == EBattleSide.Left?-1:1;
        let aniTime = BattleAniDuration / mPlayMulti;
        Laya.Tween.to(this, { x: curX + 50 * muti }, aniTime, Laya.Ease.quadInOut);
        Laya.Tween.to(this, { x: curX }, aniTime, Laya.Ease.quadInOut, null, aniTime);

        //掉血效果
        let prefab:Laya.Label = Laya.Pool.getItemByCreateFun(EPrefabPath.BattleHp, this.mBattleHpPrefab.create, this.mBattleHpPrefab) as Laya.Label;
        prefab.text = "-" + hurtInfo.hurt;
        prefab.x = this.label_hp.x;
        prefab.y = this.label_hp.y;
        this.addChild(prefab);
        Laya.Tween.to(prefab, { y: this.label_hp.y - 50 }, 500, Laya.Ease.quadInOut, Laya.Handler.create(this, () => {
            prefab.removeSelf();
            Laya.Pool.recover(EPrefabPath.BattleHp, prefab);
        }));
        this.refresh();
    }

}