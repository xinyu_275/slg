import { ui } from "../../ui/layaMaxUI";
import BattleManager from "./BattleManager";
import EventManager from "../../Common/Core/EventManager";
import { ESceneName, EPrefabPath } from "../../Config/EnumConfig";
import PlayerManager from "../Player/PlayerManager";
import { EBattleResult, EBattleSide, BattleAniDelayTime } from "../../Config/ConfigBattle";
import UI_BoxSoldier from "./UI_BoxSoldier";
import LogM from "../../Common/LogM";
import SceneManager from "../../Common/SceneManager";
import Util from "../../Common/Core/Util";
import UI_DeployList from "../Script/UI_DeployList";
import UI_RewardList from "../Script/UI_RewardList";

export default class UI_Battle extends ui.Panel.Battle.BattleUI {
    private mPlayMulti: number = 2;              // 倍速
    private mBoxSoldierPrefab: Laya.Prefab = null;   // BoxSoldierPrefab
    private mBattleHpPrefab: Laya.Prefab = null;     // BattleHpPrefab

    private mData: awesomepackage.Ip_battle_data = null;
    private mRewardList: awesomepackage.Ip_reward[] = [];
    private mResult: number = 1;                      // 结果 1.胜利 2.失败
    private mLastAttArmyId: number = 0;               // 上回合攻击方armyid
    private mLastDefArmyId: number = 0;               // 上回合防守方armyid 
    private mArmys: { [key: number]: awesomepackage.Ip_battle_army } = {};
    private mArmySides: { [key: number]: EBattleSide } = {};
    private mCurrentRound = -1;                       // 当前回合
    private mShotIndex = 0;                           // shot index
    private stopped: boolean = false;                 // 是否停止
    private over: boolean = false;                     // 是否播放结束
    private loginNoTip: boolean = false;               // 退出提示界面 是否本次登录有效  
    constructor() {
        super();
        this.btn_back.on(Laya.Event.CLICK, this, this.onClose);
        this.btn_skip.on(Laya.Event.CLICK, this, this.onSkip);
        this.btn_speedUp.on(Laya.Event.CLICK, this, this.onSpeedUp);
        this.btn_resultBack.on(Laya.Event.CLICK, this, () => {
            this.close();
        })
    }
    onEnable(): void {
        EventManager.Instance.on(ESceneName.UI_Battle, this, this.init);
    }

    onDisable(): void {
        delete this.mData;
        this.stopped = true;
        this.over = true;
        Laya.timer.clearAll(this);
        EventManager.Instance.offAllCaller(this);
    }
    reset(): void {
        this.mData = null;
        this.mRewardList = [];
        this.mLastAttArmyId = 0;
        this.mLastDefArmyId = 0;
        this.stopped = false;
        this.over = false;
        /**销毁兵节点 */
        this.box_objs.destroyChildren();
    }
    //====================按钮事件==================================
    /**关闭按钮确认 */
    onClose() {
        this.stopped = true;
        Laya.timer.clearAll(this);
        //提示
        if (this.loginNoTip) {
            this.close();
            return;
        }
        SceneManager.Instance.openAlert("是否退出战斗", "提示", Laya.Handler.create(this, this.onCloseCallback), 1,
            true, Laya.Handler.create(this, () => {
                if (!this.over) {
                    this.stopped = false;
                    this.doShot();
                }
            }));
    }
    onCloseCallback(loginNoTip: boolean): void {
        this.loginNoTip = loginNoTip;
        this.close();
    }

    /**跳过按钮 */
    onSkip(): void {
        this.stopped = true;
        //显示结果
        this.doBattleEnd();
    }

    /**加速 */
    onSpeedUp(): void {
        this.mPlayMulti++;
        if (this.mPlayMulti > 3) {
            this.mPlayMulti = 1;
        }
        this.btn_speedUp.label = "X" + this.mPlayMulti;
    }
    //=============================初始化======================
    /**初始化 */
    init(_data: awesomepackage.s_start_battle): void {
        this.reset();
        this.mData = awesomepackage["p_battle_data"].decode(_data.battle_data);
        this.mRewardList = _data.reward_list;
        if (!this.mBoxSoldierPrefab) {
            this.mBoxSoldierPrefab = Laya.loader.getRes(EPrefabPath.BoxSoldier) as Laya.Prefab;
        }
        if (!this.mBattleHpPrefab) {
            this.mBattleHpPrefab = Laya.loader.getRes(EPrefabPath.BattleHp) as Laya.Prefab;
        }
        //初始化左右方玩家信息
        this.initBattleData();
        //ui初始化
        this.initUI();
        //回合处理
        this.doRounds();
    }

    /**初始化BattleData数据 */
    initBattleData(): void {
        let left = this.mData.attackers;
        let right = this.mData.defenders;
        this.mResult = this.mData.result;
        for (let a of this.mData.defenders) {
            if (a.player_id == PlayerManager.Instance.mainPlayer.id) {
                left = this.mData.defenders;
                right = this.mData.attackers;
                this.mResult = this.mData.result == 1 ? EBattleResult.Fail : EBattleResult.Win;
                break;
            }
        }
        this.initBattleArmy(left, EBattleSide.Left);
        this.initBattleArmy(right, EBattleSide.Right);
    }
    /** 初始化BattleArmy */
    initBattleArmy(armys: awesomepackage.Ip_battle_army[], side: EBattleSide): void {
        for (let army of armys) {
            this.mArmySides[army.army_id] = side;
            this.mArmys[army.army_id] = army;
        }
    }

    /**初始化UI */
    initUI(): void {
        //1.初始化倍数，默认是1倍，如果玩家调整了，那保留倍数
        this.btn_speedUp.label = "X" + this.mPlayMulti;
        //结果界面隐藏
        this.box_result.visible = false;
    }

    //======================================逻辑处理=====================================
    //获取部队的side
    getArmySide(armyId: number): EBattleSide {
        return this.mArmySides[armyId];
    }
    /**获取部队信息 */
    getBattleArmy(armyId: number): awesomepackage.Ip_battle_army {
        return this.mArmys[armyId];
    }

    /**获取soldier的item名字 */
    getObjItemName(objId: number): string {
        return "box_item_" + objId;
    }

    /**战斗结束 */
    doBattleEnd(): void {
        // this.close();
        this.over = true;
        this.box_result.visible = true;
        if(this.mResult == EBattleResult.Win){
            this.img_resultWin.visible = true;
            this.img_resultFail.visible = false;
            this.img_resultTitle.skin = "image/battle/ui_battle_win_jiangli_font.png";
            this.box_resultFail.visible = false;
            this.list_winRewards.visible = true;
            let list_winRewards:UI_RewardList = this.list_winRewards as UI_RewardList;
            list_winRewards.init(this.mRewardList);
        }else {
            this.img_resultWin.visible = false;
            this.img_resultFail.visible = true;
            this.img_resultTitle.skin = "image/battle/ui_battle_zjzl_font.png";
            this.box_resultFail.visible = true;
            this.list_winRewards.visible = false;
        }
    }
    /**回合处理 */
    doRounds(): void {
        if (this.mData.battle_round_list.length == 0) {
            this.doBattleEnd();
            return;
        }
        this.mCurrentRound = -1;
        this.doRound();
    }
    /**单回合处理:（回合：每个对象都出手一次） */
    doRound(): void {
        if (this.stopped) return;
        this.mCurrentRound++;
        if (this.mCurrentRound >= this.mData.battle_round_list.length) {
            this.doBattleEnd();
            return;
        }
        let round = this.mData.battle_round_list[this.mCurrentRound];
        //第一回合 或者有部队被灭，另外一只部队顶上处理
        if (this.mCurrentRound == 0 || this.mLastAttArmyId != round.a_army_id) {
            this.doRoundArmyInitUI(round.a_army_id);
        }
        if (this.mCurrentRound == 0 || this.mLastDefArmyId != round.d_army_id) {
            this.doRoundArmyInitUI(round.d_army_id);
        }
        this.mLastAttArmyId = round.a_army_id;
        this.mLastDefArmyId = round.d_army_id;

        this.mShotIndex = -1;
        this.doShot();
    }

    /**出手处理 */
    doShot(): void {
        if (this.stopped) return;
        this.mShotIndex++;
        let round = this.mData.battle_round_list[this.mCurrentRound];
        if (this.mShotIndex >= round.shot_list.length) {
            this.doRound();
            return;
        }
        let shot = round.shot_list[this.mShotIndex];
        let prefabItemName = this.getObjItemName(shot.shot_obj_id);
        let box_soldier: UI_BoxSoldier = this.box_objs.getChildByName(prefabItemName) as UI_BoxSoldier;
        if (!box_soldier) {
            LogM.error("doShot error, objId not found");
            return;
        }
        box_soldier.shot(this.mPlayMulti);
        //收到伤害处理
        for (let hurtObj of shot.hurt_list) {
            prefabItemName = this.getObjItemName(hurtObj.be_obj_id);
            let box_hurt_soldier: UI_BoxSoldier = this.box_objs.getChildByName(prefabItemName) as UI_BoxSoldier;
            box_hurt_soldier.hurt(hurtObj, shot.shot_obj_id, this.mPlayMulti);
        }
        Laya.timer.once(BattleAniDelayTime / this.mPlayMulti, this, () => {
            this.doShot();
        });
    }

    /**回合初始化UI */
    doRoundArmyInitUI(armyId: number): void {
        let army: awesomepackage.Ip_battle_army = this.getBattleArmy(armyId);
        let side: EBattleSide = this.getArmySide(armyId);
        //处理化兵
        for (let soldier of army.soldier_list) {
            let prefab = Laya.Pool.getItemByCreateFun(EPrefabPath.BoxSoldier, this.mBoxSoldierPrefab.create, this.mBoxSoldierPrefab) as UI_BoxSoldier;
            let prefabItemName = this.getObjItemName(soldier.obj_id);
            prefab.name = prefabItemName;
            this.box_objs.addChild(prefab);
            prefab.init(soldier, side, this.mBattleHpPrefab);
        }
        //初始化底部玩家信息
        let labelName: Laya.Label = this.label_leftName;
        let listDeploy: UI_DeployList = this.list_leftDeploys as UI_DeployList;
        if (side == EBattleSide.Right) {
            labelName = this.label_rightName;
            listDeploy = this.list_rightDeploys;
        }
        labelName.text = Util.byteToString(army.nickname);

        //武将
        let generalList = [];
        for (let generalInfo of army.general_list) {
            generalList.push({
                id: generalInfo.general_base_id,
                type: 1,
                lv: generalInfo.lv,
                star: generalInfo.star,
            })
        }
        listDeploy.init(generalList);
    }

}