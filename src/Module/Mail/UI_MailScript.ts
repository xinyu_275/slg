import MailManager from "./MailManager";
import TipManager from "../Tip/TipManager";
import EventManager from "../../Common/Core/EventManager";
import { EEventType } from "../../Config/EnumConfig";
import { ETip } from "../../Config/EnumTip";
import SceneManager from "../../Common/SceneManager";

//邮件公用script操作
export default class UI_MailScript extends Laya.Script {
    private mail: awesomepackage.Ip_mail = null;
    private index: number = 0;
    private box_pages: Laya.Box = null;
    private box_btns: Laya.Box = null;
    private btn_del: Laya.Button = null;
    private btn_getAttachment: Laya.Button = null;
    private btn_save: Laya.Button = null;
    private btn_pageLeft: Laya.Image = null;
    private btn_pageRight: Laya.Image = null;

    onEnable(): void {
        this.box_pages = this.owner.getChildByName("box_pages") as Laya.Box;
        this.box_btns = this.owner.getChildByName("box_btns") as Laya.Box;
        if (this.box_btns) {
            this.btn_del = this.box_btns.getChildByName("btn_del") as Laya.Button;
            this.btn_getAttachment = this.box_btns.getChildByName("btn_getAttachment") as Laya.Button;
            this.btn_save = this.box_btns.getChildByName("btn_save") as Laya.Button;
        }
        if (this.box_pages) {
            this.btn_pageLeft = this.box_pages.getChildByName("btn_pageLeft") as Laya.Image;
            this.btn_pageRight = this.box_pages.getChildByName("btn_pageRight") as Laya.Image;
        }
        this.btn_del && this.btn_del.on(Laya.Event.CLICK, this, this.onDel);
        this.btn_getAttachment && this.btn_getAttachment.on(Laya.Event.CLICK, this, this.onGetAttachment);
        this.btn_save && this.btn_save.on(Laya.Event.CLICK, this, this.onSave);
        this.btn_pageLeft && this.btn_pageLeft.on(Laya.Event.CLICK, this, this.onPageLeft);
        this.btn_pageRight && this.btn_pageRight.on(Laya.Event.CLICK, this, this.onPageRight);

        EventManager.Instance.on(EEventType.MailRewardSuccess, this, this.onMailRewardSuccess);
    }

    onDisable(): void {
        EventManager.Instance.offAllCaller(this);
    }

    /**初始化 */
    init(mail: awesomepackage.Ip_mail, mailNum:number, index: number): void {
        if (!mail.is_read) {
            MailManager.Instance.c_read_mails([Number(mail.mail_id)]);
        }
        this.mail = mail;
        this.index = index;
        if (this.btn_getAttachment) {
            this.btn_getAttachment.visible = mail.reward_list && mail.reward_list.length > 0;
            this.btn_getAttachment.disabled = mail.is_reward;
            this.btn_getAttachment.label = mail.is_reward ? "已领取" : "领取";
        }
        if(this.box_pages) {
            this.btn_pageLeft.visible = index > 0;
            this.btn_pageRight.visible = index < mailNum - 1;
        }
    }

    /**删除邮件 */
    onDel(): void {
        if (!this.mail.is_reward && this.mail.reward_list.length > 0) {
            SceneManager.Instance.openAlert(ETip.MailHaveRewardCannotDel);
            return;
        }
        MailManager.Instance.c_delete_mails([Number(this.mail.mail_id)]);
    }

    /**获取附件 */
    onGetAttachment(): void {
        if (this.mail.is_reward || !this.mail.reward_list || this.mail.reward_list.length == 0) {
            return;
        }
        MailManager.Instance.c_get_mail_reward(Number(this.mail.mail_id));
    }

    /**归档 */
    onSave(): void {
        MailManager.Instance.c_save_mail(Number(this.mail.mail_id));
        TipManager.Instance.showNormalMsg(ETip.MailSaveSuccess);
    }

    /**往前翻页 */
    onPageLeft(): void {
        EventManager.Instance.event(EEventType.MailPageUpdate, [this.index - 1]);
    }

    /**往后翻页 */
    onPageRight(): void {
        EventManager.Instance.event(EEventType.MailPageUpdate, [this.index + 1]);
    }

    /**领奖成功 */
    onMailRewardSuccess(mailId: number): void {
        if (this.mail.mail_id != mailId) {
            return;
        }
        this.btn_getAttachment.label = "已领取";
        this.btn_getAttachment.disabled = this.mail.is_reward;
    }
}