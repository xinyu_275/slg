import AbsGameModule from "../AbsGameModule";
import NetSocketManager from "../../Common/NetSocketManager";
import EventManager from "../../Common/Core/EventManager";
import { EEventType, ESceneName } from "../../Config/EnumConfig";
import TipManager from "../Tip/TipManager";
import Util from "../../Common/Core/Util";
import MailConfigManager from "../../Config/Data/MailConfigManager";
import SceneManager from "../../Common/SceneManager";

export default class MailManager extends AbsGameModule {
    private mMails: { [key: number]: awesomepackage.Ip_mail } = {};
    private mTabIds: { [key: number]: number[] } = {};

    //实例
    public static get Instance(): MailManager {
        return MailManager.getInstance<MailManager>();
    }
    constructor() {
        super();
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_mail_list, this, this.s_mail_list);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_add_mail, this, this.s_add_mail);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_delete_mails, this, this.s_delete_mails);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_mail_reward, this, this.s_get_mail_reward);
    }

    /**清理数据 */
    clear(): void {
        this.mMails = {};
        this.mTabIds = {};
    }

    /**根据tab获取邮件列表 */
    getMailListByTab(tab: number): awesomepackage.Ip_mail[] {
        let tabIds = this.mTabIds[tab];
        if (!tabIds) {
            return [];
        }
        let tabMails: awesomepackage.Ip_mail[] = [];
        for (let mailId of tabIds) {
            tabMails.push(this.mMails[mailId]);
        }
        return tabMails;
    }

    /**根据tab获取邮件id列表 */
    getMailIdListByTab(tab: number): number[] {
        let tabIds = this.mTabIds[tab];
        if (!tabIds) {
            return [];
        }
        return tabIds;
    }

    /**获取邮件title */
    getMailTitle(mail: awesomepackage.Ip_mail): string {
        let title: string = Util.byteToString(mail.title);
        if (title.length > 0) {
            return title;
        }
        return MailConfigManager.Instance.getMailTitle(mail.mail_template_id);
    }

    /**获取邮件内容 */
    getMailContent(mail: awesomepackage.Ip_mail): string {
        let content: string = Util.byteToString(mail.content);
        if (content.length > 0) {
            return content;
        }
        return MailConfigManager.Instance.getMailContent(mail.mail_template_id);
    }

    //------------------------------处理协议---------------------------
    /**获取邮件列表 */
    c_mail_list(): void {
        NetSocketManager.Instance.SendMsg("c_mail_list", {});
    }
    s_mail_list(data: awesomepackage.s_mail_list): void {
        for (let mail of data.mail_list) {
            this.mMails[Number(mail.mail_id)] = mail;
            if (!this.mTabIds[mail.tab]) this.mTabIds[mail.tab] = [];
            this.mTabIds[mail.tab].push(Number(mail.mail_id));
        }
        //tab -> ids排序
        for (let k in this.mTabIds) {
            this.mTabIds[k].sort((a: number, b: number) => b - a);
        }
    }

    /**添加邮件 */
    s_add_mail(data: awesomepackage.s_add_mail): void {
        let mail = data.mail_info;
        this.mMails[Number(mail.mail_id)] = mail;
        if (!this.mTabIds[mail.tab]) this.mTabIds[mail.tab] = [];
        //加到第一个
        this.mTabIds[mail.tab].unshift(Number(mail.mail_id));
        EventManager.Instance.event(EEventType.MailListUpdate);
    }

    /**设置已读邮件id */
    c_read_mails(mailIds: number[]): void {
        for (let mailId of mailIds) {
            this.mMails[mailId].is_read = true;
        }
        NetSocketManager.Instance.SendMsg("c_read_mails", { mail_id_list: mailIds });
        EventManager.Instance.event(EEventType.MailRead);
    }

    /** 删除邮件*/
    c_delete_mails(mailIds: number[]): void {
        NetSocketManager.Instance.SendMsg("c_delete_mails", { mail_id_list: mailIds });
    }
    s_delete_mails(data: awesomepackage.s_delete_mails): void {
        let mail: awesomepackage.Ip_mail;
        let mailId: number;
        for (let mailIdStr of data.mail_id_list) {
            mailId = Number(mailIdStr);
            let mail = this.mMails[mailId];
            if (!mail) continue;

            delete this.mMails[mailId];
            let tabIds = this.mTabIds[mail.tab];
            let indexOf = tabIds.indexOf(mailId);
            if (indexOf >= 0) {
                this.mTabIds[mail.tab].splice(indexOf, 1)
            }
        }
        EventManager.Instance.event(EEventType.MailListUpdate);
    }
    /**邮件归档 */
    c_save_mail(mailId: number): void {
        let toTab = 3;
        NetSocketManager.Instance.SendMsg("c_save_mail", { mail_id: mailId });
        //tab变成3
        let mail = this.mMails[mailId];
        if(!mail || mail.tab == toTab) return;
        let tabIds = this.mTabIds[mail.tab];
        let indexOf = tabIds.indexOf(mailId);
        if(indexOf >= 0) {
            this.mTabIds[mail.tab].splice(indexOf, 1)
        }
        mail.tab = toTab;
        if (!this.mTabIds[mail.tab]) this.mTabIds[mail.tab] = [];
        this.mTabIds[mail.tab].push(Number(mail.mail_id));
        //排序
        this.mTabIds[toTab].sort((a: number, b: number) => b - a);
        EventManager.Instance.event(EEventType.MailListUpdate);
    }

    /**领取附件 */
    c_get_mail_reward(mailId: number): void {
        NetSocketManager.Instance.SendMsg("c_get_mail_reward", { mail_id: mailId });
    }
    s_get_mail_reward(data: awesomepackage.s_get_mail_reward): void {
        if (data.code != awesomepackage.e_code.ok) {
            TipManager.Instance.showMsgById(data.code);
            return;
        }
        if (data.mail_id > 0) {
            let mailId:number = Number(data.mail_id);
            if (this.mMails[mailId]) {
                this.mMails[mailId].is_reward = true;
                this.mMails[mailId].is_read = true;
                EventManager.Instance.event(EEventType.MailRewardSuccess, mailId)
            }
        } else {
            //一键领取所有的邮件(只有系统邮件有附件)
            //未读的邮件变成已读
            let mailList = this.getMailListByTab(0);
            for (let mail of mailList) {
                if (mail.reward_list.length > 0 && !mail.is_reward) {
                    mail.is_reward = true;
                    mail.is_read = true;
                }
            }
        }
        //奖励弹出
        SceneManager.Instance.openRewardPopup(data.reward_list);
    }
}