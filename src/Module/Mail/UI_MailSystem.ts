import { ui } from "../../ui/layaMaxUI";
import EventManager from "../../Common/Core/EventManager";
import Util from "../../Common/Core/Util";
import MailManager from "./MailManager";
import { EEventType, ESceneName } from "../../Config/EnumConfig";
import TipManager from "../Tip/TipManager";
import { ETip } from "../../Config/EnumTip";
import UI_MailScript from "./UI_MailScript";
import UI_RewardList from "../Script/UI_RewardList";
//系统邮件
export default class UI_MailSystem extends ui.Panel.Mail.MailSystemUI {
    constructor() {
        super();
    }
    onEnable(): void {
        EventManager.Instance.on(ESceneName.UI_MailSystem, this, this.init);
        EventManager.Instance.on(EEventType.MailMessageUpdate, this, this.init);
    }
    onDisable(): void {
        EventManager.Instance.offAllCaller(this);
    }
    //------------------------事件处理-----------------------
    /**邮件message更新 */
    init(mail: awesomepackage.Ip_mail, mailNum: number, index: number): void {
        if (mailNum == 0) {
            this.close();
            return;
        }
        //初始化组件
        (this.list_rewardList as UI_RewardList).init(mail.reward_list, true);
        (this.getComponent(UI_MailScript) as UI_MailScript).init(mail, mailNum, index);

        //
        this.label_mailTitle.text = MailManager.Instance.getMailTitle(mail);
        this.label_mailContent.text = MailManager.Instance.getMailContent(mail);
        this.label_pageIndex.text = (index + 1) + "/" + mailNum;
        this.label_mailTime.text = Util.secondToDateTime(mail.send_time, false);
    }
   
}