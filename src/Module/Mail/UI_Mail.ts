import { ui } from "../../ui/layaMaxUI";
import EventManager from "../../Common/Core/EventManager";
import MailManager from "./MailManager";
import UI_MailList from "./UI_MailList";
import LogM from "../../Common/LogM";
import { EEventType, ESceneName } from "../../Config/EnumConfig";
import TipManager from "../Tip/TipManager";
import { ETip } from "../../Config/EnumTip";
import SceneManager from "../../Common/SceneManager";

export default class UI_Mail extends ui.Panel.Mail.MailUI {
    private mgr: MailManager = MailManager.Instance;
    private tab: number = 0;
    constructor() {
        super();
        this.tabs.selectHandler = new Laya.Handler(this, this.onSelectTab);
        this.btn_batchDel.on(Laya.Event.CLICK, this, this.onBatchDel);
        this.btn_allRead.on(Laya.Event.CLICK, this, this.onAllRead);
        this.btn_batchGetAttchment.on(Laya.Event.CLICK, this, this.onBatchGetAttchment);
    }
    onEnable(): void {
        EventManager.Instance.on(EEventType.MailListUpdate, this, this.onMailListUpdate);
        this.tabs.selectedIndex = 0;
    }
    onDisable(): void {
        this.tab = 0;
        this.tabs.selectedIndex = -1;
        EventManager.Instance.offAllCaller(this);
    }

    /**选择标签 */
    onSelectTab(tabIndex: number): void {
        LogM.debug("onSelectTab", tabIndex);
        this.tab = tabIndex;
        //只有系统邮件有一键领取
        this.btn_batchGetAttchment.visible = tabIndex == 0;
        //只有收藏不显示全部已读
        this.btn_allRead.visible = tabIndex != 3;
        this.refresh();
    }

    /**刷新列表 */
    refresh(): void {
        LogM.debug("Mail Refresh");
        let mailList = this.mgr.getMailListByTab(this.tab);
        this.box_noMail.visible = mailList.length === 0;
        let boxList = this.list_mails as UI_MailList;
        boxList.init(mailList);
    }

    /**邮件列表更新事件 */
    onMailListUpdate(): void {
        LogM.debug("onMailListUpdate");
        let mailList = this.mgr.getMailListByTab(this.tab);
        let mailIdList = this.mgr.getMailIdListByTab(this.tab);
        this.refresh();
    }
    //----------------------------事件处理------------------------------
    /**批量删除 */
    onBatchDel(): void {
        let mailList = this.mgr.getMailListByTab(this.tab);
        let canDel: boolean = true;
        for (let mail of mailList) {
            if (!mail.is_reward && mail.reward_list.length > 0) {
                canDel = false;
                break;
            }
        }
        if (!canDel) {
            //提示框
            SceneManager.Instance.openAlert(ETip.MailHaveRewardCannotDel);
            return;
        }
        SceneManager.Instance.openAlert(ETip.MailAllIsDel, "确认删除", Laya.Handler.create(this, this.onBatchDelCallback), 1);
    }
    onBatchDelCallback(): void {
        LogM.debug("onBatchDelCallback");
        let mailIdList = this.mgr.getMailIdListByTab(this.tab);
        this.mgr.c_delete_mails(mailIdList);
    }

    /**全部已读 */
    onAllRead(): void {
        let mailIdList = this.mgr.getMailIdListByTab(this.tab);
        this.mgr.c_read_mails(mailIdList);
        //this.refresh();
    }

    /**一键领取 */
    onBatchGetAttchment(): void {
        if (this.tab !== 0) {
            return;
        }
        let mailList = this.mgr.getMailListByTab(this.tab);
        let haveAttachment: boolean = false;
        for (let mail of mailList) {
            if (!mail.is_reward && mail.reward_list.length > 0) {
                haveAttachment = true;
                break;
            }
        }
        if (!haveAttachment) {
            TipManager.Instance.showNormalMsg(ETip.MailNoAttchment);
            return;
        }
        this.mgr.c_get_mail_reward(0);
    }
}