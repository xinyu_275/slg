import Util from "../../Common/Core/Util";
import MailManager from "./MailManager";
import SceneManager from "../../Common/SceneManager";
import { ESceneName, EEventType, EMailType } from "../../Config/EnumConfig";
import UI_MailSystem from "./UI_MailSystem";
import MailConfigManager from "../../Config/Data/MailConfigManager";
import EventManager from "../../Common/Core/EventManager";

export default class UI_MailList extends Laya.List {
    private openIndex: number = -1;
    onEnable(): void {
        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
        this.mouseHandler = new Laya.Handler(this, this.onMouseHandler);

        EventManager.Instance.on(EEventType.MailRead, this, this.onMailRead);
        EventManager.Instance.on(EEventType.MailPageUpdate, this, this.onMailPageUpdate);
    }
    onDisable(): void {
        this.array = [];
        this.openIndex = -1;

        EventManager.Instance.offAllCaller(this);
    }

    /**
     * 初始化list数据 
     * _datas: Ip_mail[]
     */
    init(_datas: awesomepackage.Ip_mail[]) {
        this.vScrollBarSkin = "";
        this.array = _datas;
        //列表数据有更新时，确保打开的邮件也会更新（例如：打开的邮件执行删除操作,删除后要显示下一封邮件）
        this.checkOpenMailMessageUpdate();
    }

    /**渲染 */
    onRenderHandler(cell: Laya.Box, index: number): void {
        if (index >= this.array.length) {
            return;
        }
        //物品图标
        this.renderMailItem(cell, index);
    }

    /** click*/
    onMouseHandler(e: Laya.Event, index: number) {
        if (e.type == Laya.Event.CLICK) {
            let mailItem: awesomepackage.Ip_mail = this.array[index];
            //打开邮件详细信息
            this.openMailDetail(mailItem, index);
        }
    }

    //打开邮件详细信息
    openMailDetail(mail: awesomepackage.Ip_mail, index: number): void {
        this.openIndex = index;
        let sceneName: ESceneName;
        switch (MailConfigManager.Instance.getMailType(mail.mail_template_id)) {
            case EMailType.SystemMail:
                sceneName = ESceneName.UI_MailSystem;
                break;
            case EMailType.BattleMail:
                break;
            case EMailType.GuildMail:
                break;
        }
        SceneManager.Instance.openScene(sceneName, [mail, this.array.length, index]);
    }

    /**检测打开邮件更新 */
    checkOpenMailMessageUpdate(): void {
        if (this.openIndex == -1) return;
        if (this.openIndex > this.array.length - 1) {
            this.openIndex = this.array.length - 1;
        }
        let mail = this.array[this.openIndex];
        EventManager.Instance.event(EEventType.MailMessageUpdate, [mail, this.array.length, this.openIndex]);
    }

    /**有邮件已读 */
    onMailRead():void {
        this.refresh();
    }

    /**邮件详情翻页操作 */
    onMailPageUpdate(openIndex:number):void {
        this.openIndex = openIndex;
        this.checkOpenMailMessageUpdate();
    }
    
    /**渲染选项 */
    renderMailItem(cell: Laya.Box, index: number): void {
        let img_itemBg = cell.getChildByName("img_itemBg") as Laya.Image;
        let img_itemAttachment = cell.getChildByName("img_itemAttachment") as Laya.Image;
        let img_itemRead = cell.getChildByName("img_itemRead") as Laya.Image;
        let img_itemRedpoint = cell.getChildByName("img_itemRedpoint") as Laya.Image;
        let label_itemTitle = cell.getChildByName("label_itemTitle") as Laya.Label;
        let label_itemContent = cell.getChildByName("label_itemContent") as Laya.Label;
        let label_itemTime = cell.getChildByName("label_itemTime") as Laya.Label;
        let mailItem: awesomepackage.Ip_mail = this.array[index];

        label_itemTitle.text = MailManager.Instance.getMailTitle(mailItem);
        label_itemContent.text = MailManager.Instance.getMailContent(mailItem);
        label_itemTime.changeText(Util.secondToDateTime(mailItem.send_time, false));
        if (mailItem.is_read) {
            img_itemBg.skin = "image/mail/ui_mail_item_bg_01.png";
            img_itemRead.visible = true;
            img_itemRedpoint.visible = false;
        } else {
            img_itemBg.skin = "image/mail/ui_mail_item_bg.png";
            img_itemRead.visible = false;
            img_itemRedpoint.visible = true;
        }
        if (!mailItem.reward_list || mailItem.reward_list.length == 0) {
            img_itemAttachment.visible = false;
        } else {
            img_itemAttachment.visible = true;
        }
    }
}