import { ui } from "../../ui/layaMaxUI";
import GeneralManager from "./GeneralManager";
import EventManager from "../../Common/Core/EventManager";
import UI_GeneralList from "./UI_GeneralList";
import GeneralConfigManager from "../../Config/Data/GeneralConfigManager";
import Util from "../../Common/Core/Util";

export default class UI_General extends ui.Panel.General.GeneralUI {
    private mgr: GeneralManager = GeneralManager.Instance;
    constructor() {
        super();
    }

    onEnable(): void {
        this.init();
    }

    onDisable(): void {
        EventManager.Instance.offAllCaller(this);
    }

    /**初始化 */
    init():void {
        //已经招募的武将id列表
        let recruitIdList = this.mgr.getGeneralIdList();
        let recruitInitY = 153;
        //确定宽高
        let recruitHeight = 220 * Math.ceil(recruitIdList.length / 4); 
        let list_recruit = (this.list_recruit as UI_GeneralList);
        list_recruit.height = recruitHeight;
        list_recruit.init(recruitIdList);

        let allIds = GeneralConfigManager.Instance.getAllGeneralBaseIds();
        let noRecruitIdList = Util.difference(allIds, recruitIdList);
        let noRecruitIdHeight = 220 * Math.ceil(noRecruitIdList.length / 4); 
        let list_noRecruit = (this.list_noRecruit as UI_GeneralList);
        list_noRecruit.height = noRecruitIdHeight;
        list_noRecruit.y = recruitInitY + recruitHeight + 80;
        list_noRecruit.init(noRecruitIdList);

        this.img_NoRecruit.y = recruitInitY + recruitHeight;
    }
}