import GeneralConfigManager from "../../Config/Data/GeneralConfigManager";
import VOGeneral from "./VOGeneral";
import GeneralManager from "./GeneralManager";

export default class UI_GeneralList extends Laya.List {
    constructor() {
        super();
        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
    }
    onEnable(): void {
    }
    onDisable(): void {
        this.array = [];
    }

    /**初始化数据 */
    init(datas: number[]) {
        this.vScrollBarSkin = "";
        this.array = datas;
    }

    /**渲染 */
    onRenderHandler(cell: Laya.Box, index: number): void {
        let img_itemIcon: Laya.Image = cell.getChildByName("img_itemIcon") as Laya.Image;
        let img_itemOp: Laya.Image = cell.getChildByName("img_itemOp") as Laya.Image;
        let label_itemNum: Laya.Label = cell.getChildByName("label_itemNum") as Laya.Label;
        let label_itemName: Laya.Label = cell.getChildByName("label_itemName") as Laya.Label;
        let box_stars:Laya.Box = cell.getChildByName("box_stars") as Laya.Box;

        let generalBaseId: number = this.array[index];
        let baseGeneral = GeneralConfigManager.Instance.getBaseGeneral(generalBaseId);
        let vo: VOGeneral = GeneralManager.Instance.getGeneral(generalBaseId);
        img_itemIcon.skin = baseGeneral.icon;
        img_itemOp.visible = false;
        label_itemName.text = baseGeneral.name;
        let star = 0;
        if (vo) {
            label_itemNum.text = "LV." + vo.lv;
            star = vo.star;
        } else {
            label_itemNum.text = "0/10";
        }
        for (let i = 1; i <= 5; i++) {
            if (star >= i) {
                (box_stars.getChildByName("img_star" + i) as Laya.Image).skin = "image/comp/ui_star_ light.png";
            } else {
                (box_stars.getChildByName("img_star" + i) as Laya.Image).skin = "image/comp/ui_star_gray.png";
            }
        }
    }
}