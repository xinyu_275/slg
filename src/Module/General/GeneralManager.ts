import AbsGameModule from "../AbsGameModule";
import NetSocketManager from "../../Common/NetSocketManager";
import VOGeneral from "./VOGeneral";

export default class GeneralManager extends AbsGameModule {
    private mGenerals: { [key: number]: VOGeneral } = {};
    //实例
    public static get Instance(): GeneralManager {
        return GeneralManager.getInstance<GeneralManager>();
    }
    constructor() {
        super();
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_general_list, this, this.s_get_general_list);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_add_general, this, this.s_add_general);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_refresh_general_attr, this, this.s_refresh_general_attr);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_general_add_exp, this, this.s_general_add_exp);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_general_up_star, this, this.s_general_up_star);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_general_compose, this, this.s_general_compose);
    }

    /**获取获得的武将列表 */
    getGeneralIdList(): number[] {
        let arr: number[] = [1, 2, 3, 4, 5];
        for (let k in this.mGenerals) {
            arr.push(Number(k));
        }
        return arr;
    }

    /**获取武将信息 */
    getGeneral(generalBaseId: number): VOGeneral {
        return this.mGenerals[generalBaseId];
    }

    //---------------------处理协议--------------------------
    /**获取武将列表信息 */
    c_get_general_list(): void {
        NetSocketManager.Instance.SendMsg("c_get_general_list", {});
    }
    s_get_general_list(data: awesomepackage.s_get_general_list): void {
        for (let general of data.general_list) {
            let vo = new VOGeneral();
            vo.init(general);
            this.mGenerals[vo.general_base_id] = vo;
        }
    }

    /**添加武将 */
    s_add_general(data: awesomepackage.s_add_general): void {
        let vo = new VOGeneral();
        vo.init(data.general_info);
        this.mGenerals[vo.general_base_id] = vo;
    }

    /**刷新武将信息 */
    s_refresh_general_attr(data: awesomepackage.s_refresh_general_attr): void {
        let vo = this.mGenerals[data.general_base_id];
        if (!vo) return;
        for (let generalAttr of data.attrs) {
            switch (generalAttr.attr_key) {
                case awesomepackage.e_general_attr_key.lv:
                    vo.lv = generalAttr.attr_value;
                    break;
                case awesomepackage.e_general_attr_key.exp:
                    vo.exp = generalAttr.attr_value;
                    break;
                case awesomepackage.e_general_attr_key.star:
                    vo.star = generalAttr.attr_value;
                    break;
                case awesomepackage.e_general_attr_key.state:
                    vo.state = generalAttr.attr_value;
                    break;
            }
        }
        //todo 更新
    }

    /**武将使用物品增加经验 */
    c_general_add_exp(generalBaseId: number, goodsList: awesomepackage.Ip_goods_num[]): void {
        NetSocketManager.Instance.SendMsg("c_general_add_exp", {
            general_base_id: generalBaseId,
            goods_list: goodsList,
        })
    }
    s_general_add_exp(data: awesomepackage.s_general_add_exp): void {

    }

    /**武将升星 */
    c_general_up_star(generalBaseId: number): void {
        NetSocketManager.Instance.SendMsg("c_general_up_star", { general_base_id: generalBaseId });
    }
    s_general_up_star(data: awesomepackage.s_general_up_star): void {

    }

    /**碎片合成武将 */
    c_general_compose(generalBaseId: number): void {
        NetSocketManager.Instance.SendMsg("c_general_compose", { general_base_id: generalBaseId });
    }
    s_general_compose(data: awesomepackage.s_general_compose): void {

    }
}