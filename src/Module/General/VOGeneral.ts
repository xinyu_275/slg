export default class VOGeneral {
    public general_base_id:number;                   // GeneralBaseId
    public lv?:number = 1;
    public exp?:number = 0;
    public star?:number = 1;
    public state?:number = 0;                       // 武将状态（>0说明出征在外）

    init(general:awesomepackage.Ip_general){
        this.general_base_id = general.general_base_id;
        this.lv = general.lv;
        this.exp = general.exp;
        this.star = general.star;
        this.state = general.state;
    }
}
