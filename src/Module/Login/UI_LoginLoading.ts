import { ui } from "../../ui/layaMaxUI";
import LogM from "../../Common/LogM";
import SceneManager from "../../Common/SceneManager";
import { EEventType } from "../../Config/EnumConfig";
import EventManager from "../../Common/Core/EventManager";
export default class UI_LoginLoading extends ui.Panel.Loading.LoginLoadingUI {
    constructor() {
        super();
    }
    onEnable(): void {
        EventManager.Instance.on(EEventType.LoginLoadingProgress, this, this.onLoginLoadingProgress);
        EventManager.Instance.on(EEventType.CloseLoginLoadingView, this, this.onCloseLoginLoadingView);
    }

    onDisable(): void {
        EventManager.Instance.offAllCaller(this);
    }

    //设置加载进度
    onLoginLoadingProgress(progress: number): void {
        LogM.debug("onLoginLoadingProgress", progress);
        const width: number = 666 * progress;
        this.img_progress.width = width;
    }

    onCloseLoginLoadingView():void {
        this.close();
    }
}