import ConfigDataManager from "../../Config/Data/ConfigDataManager";
import LogM from "../../Common/LogM";
import NetSocketManager from "../../Common/NetSocketManager";
import { EEventType, ESceneName } from "../../Config/EnumConfig";
import Singleton from "../../Common/Core/Singleton";
import EventManager from "../../Common/Core/EventManager";
import Util from "../../Common/Core/Util";
import TipManager from "../Tip/TipManager";
import AbsGameModule from "../AbsGameModule";
import TimeManager from "../../Common/Core/TimeManager";
import SceneManager from "../../Common/SceneManager";

// //登录状态
// enum LoginState {
//     no_finish,          // 没登录完成
//     login_fail,         // 登录失败
// }
export default class LoginManager extends AbsGameModule {
    private resLoaded: boolean = false;          // 资源是否加载完成
    private netConnectSuccess: boolean = false;  // 网络是否连接

    public serverIp: string = "192.168.0.101";
    //public serverIp:string = "192.168.1.184";
    //public serverIp:string = "192.168.1.51";
    public serverPort: number = 7001;

    //实例
    public static get Instance(): LoginManager {
        return LoginManager.getInstance<LoginManager>();
    }

    constructor() {
        super();
        //平台初始化
        //注册事件
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_player_login, this, this.s_player_login);
        EventManager.Instance.on(EEventType.OpenSocketSuccess, this, this.onOpenSocketSuccess);
    }

    //获取ip和端口
    public get serverIpPort(): string {
        if (this.serverIp) {
            return this.serverIp + ":" + this.serverPort;
        }
        return "";
    }

    /**场景退出 */
    scene_exit(): void {
        this.resLoaded = false;
        this.netConnectSuccess = false;
        //关闭加载界面
        this.closeView();
    }

    //=========================界面处理============================
    // /**打开加载界面 */
    openView(sceneName = ESceneName.UI_LoginLoading) {
        //显示加载界面
        SceneManager.Instance.openScene(sceneName, null, true, Laya.Handler.create(this, this.onOpened));
    }
    /**关闭加载界面 */
    closeView():void {
        EventManager.Instance.event(EEventType.CloseLoginLoadingView);
    }
    //界面打开后处理
    onOpened() {
        //1.加载资源
        this.beginLoadRes();
        //2.并发连接服务器
        this.startLogin();
        //检查是否加载以及连接上了服务器
        Laya.timer.loop(100, this, this.checkFinish);
    }

    /**
     * 判断登录模块是否完成(加载和登录都完成)
     */
    checkFinish() {
        if (this.resLoaded && this.netConnectSuccess) {
            EventManager.Instance.event(EEventType.LoginSuccess);
            Laya.timer.clear(this, this.checkFinish);
        }
    }
    //----------------------处理加载-------------------
    beginLoadRes(): void {
        //配置文件
        let configArr: Array<any> = ConfigDataManager.Instance.getAssetLoadUrls();
        var assetArr: Array<any> = [
            //{ url: "res/atlas/comp.png" },
            //{ url: "res/atlas/army.atlas", type: Laya.Loader.ATLAS },
            //{ url: "res/atlas/army.png" },
        ];
        let allAll: Array<any> = Util.union(assetArr, configArr);
        Laya.loader.load(allAll, Laya.Handler.create(this, this.onLoadedResComplete), Laya.Handler.create(this, this.onLoadedResProgress, null, false));
    }
    //加载完成
    onLoadedResComplete(): void {
        LogM.debug("onLoadedResComplete");
        this.resLoaded = true;
    }

    //加载进度
    onLoadedResProgress(progress: number): void {
        EventManager.Instance.event(EEventType.LoginLoadingProgress, progress);
    }

    //----------------------处理登录-------------------
    /**
    * 加载资源期间并发处理登录
    */
    startLogin(): void {
        let serverInfo: string = this.serverIpPort;
        LogM.info("startLogin:" + serverInfo);
        let proto = "ws://";
        if (serverInfo.indexOf('.com') >= 0 || serverInfo.indexOf(".cn") >= 0) {
            proto = 'wss://';
        }
        NetSocketManager.Instance.Init(proto + serverInfo);
    }
    //socket打开回调
    onOpenSocketSuccess(): void {
        LogM.info("onOpenSocketSuccess");
        this.c_player_login();
    }

    /**发送玩家登录协议 */
    c_player_login(): void {
        NetSocketManager.Instance.SendMsg("c_player_login", {
            token: Util.string2Byte("aaaa"),
            sn: 1,
        });
    }
    s_player_login(data: awesomepackage.s_player_login): void {
        if (data.code == awesomepackage.e_code.ok) {
            this.netConnectSuccess = true;
            TimeManager.Instance.initServerTime(Number(data.time));
            return;
        }
        //错误提示
        LogM.info("s_player_login error" + data.code);
        TipManager.Instance.showMsgById(data.code);
    }
}