import { ui } from "../../ui/layaMaxUI";
import EventManager from "../../Common/Core/EventManager";
import { ESceneName } from "../../Config/EnumConfig";
import GoodsConfigManager from "../../Config/Data/GoodsConfigManager";
export default class UI_GoodsDesc extends ui.Panel.Bag.GoodsDescUI {
    constructor(){
        super();
    }
    onEnable(): void {
        EventManager.Instance.on(ESceneName.UI_GoodsDesc, this, this.init);
    }
    onDisable():void {
        EventManager.Instance.offAllCaller(this);
    }

    /**初始化 */
    init(goodsBaseId:number):void {
        if(!goodsBaseId){
            this.close();
        }
        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(goodsBaseId);
        this.label_title.text = baseGoods.name;
        this.label_desc.text = baseGoods.des;
        this.img_icon.skin = baseGoods.icon;
    }
}