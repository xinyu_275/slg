import { ui } from "../../ui/layaMaxUI";
import EventManager from "../../Common/Core/EventManager";
import GoodsConfigManager, { BaseGoods } from "../../Config/Data/GoodsConfigManager";
import BagManager from "./BagManager";
import UI_Slider from "../Script/UI_Slider";
export default class UI_BagDel extends ui.Panel.Bag.BagDelUI {
    private mgr:BagManager = BagManager.Instance;
    constructor(){
        super();
        this.btn_del.on(Laya.Event.CLICK, this, this.onDel);
    }
    onEnable(): void {
        let goodsBaseId = this.mgr.selectGoodsBaseId;
        let num = this.mgr.getGoodsNum(goodsBaseId);
        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(goodsBaseId);
        this.label_itemDesc.text = baseGoods.des;
        let boxSlider = this.box_slider as UI_Slider;
        boxSlider.init(num);
    }
    onDisable():void {
        EventManager.Instance.offAllCaller(this);
    }

    /**删除物品 */
    onDel():void {
        let goodsBaseId = this.mgr.selectGoodsBaseId;
        let boxSlider = this.box_slider as UI_Slider;
        let num = boxSlider.getValue();
        this.mgr.c_del_goods(goodsBaseId, num);
        this.close();
    }
}