import { ui } from "../../ui/layaMaxUI";
import LogM from "../../Common/LogM";
import LockManager from "../../Common/LockManager";
import BagManager from "./BagManager";
import SceneManager from "../../Common/SceneManager";
import EventManager from "../../Common/Core/EventManager";
import { EEventType, ESceneName } from "../../Config/EnumConfig";
import GoodsConfigManager from "../../Config/Data/GoodsConfigManager";
import UI_Slider from "../Script/UI_Slider";
import UI_BagItemList from "./UI_BagItemList";
export default class UI_Bag extends ui.Panel.Bag.BagUI {
    private mgr:BagManager = BagManager.Instance;
    constructor() {
        super();
        this.tabs.selectHandler = new Laya.Handler(this, this.selectTab);
        this.btn_use.on(Laya.Event.CLICK, this, this.onUseGoods);
        this.btn_delete.on(Laya.Event.CLICK, this, this.onDeleteGoods);
    }
    onEnable(): void {
        EventManager.Instance.on(EEventType.BagItemListClick, this, this.onBagItemListClick);
        EventManager.Instance.on(EEventType.BagItemListUpdate, this, this.onBagItemListUpdate);
        this.selectTab(0);
    }
    onDisable():void {
        EventManager.Instance.offAllCaller(this);
    }

    /**添加/删除物品导致界面更新 */
    onBagItemListUpdate(): void {
        let tab = this.tabs.selectedIndex;
        let goodsList = this.mgr.getSortGoodsListByTab(tab);
        let prefabScript = this.list_bagList as UI_BagItemList;
        prefabScript.refreshList(goodsList);
        this.mgr.selectGoodsBaseId = prefabScript.getSelectGoodsBaseId();
        this.showGoodsDetail();
    }
    /**背包标签选中 */
    selectTab(tab: number): void {
        LogM.debug("UI_Bag selectTab:", tab);
        this.tabs.selectedIndex = tab;
        let goodsList =  this.mgr.getSortGoodsListByTab(tab);
        let prefabScript = this.list_bagList as UI_BagItemList;
        prefabScript.init(goodsList, true);
        this.bottoms.visible = goodsList.length > 0;
    }
    /**点击物品回调处理 */
    onBagItemListClick(goodsBaseId: number): void {
        LogM.debug("UI_Bag clickItem:", goodsBaseId);
        this.mgr.selectGoodsBaseId = goodsBaseId;
        this.mgr.setIsNew(goodsBaseId, false);
        this.showGoodsDetail();
    }
    /**显示物品详细信息 */
    showGoodsDetail(): void {
        LogM.debug("showGoodsDetail:",  this.mgr.selectGoodsBaseId);
        let goodsBaseId =  this.mgr.selectGoodsBaseId;
        let goodsNum =  this.mgr.getGoodsNum(goodsBaseId);
        if (goodsNum == 0) {
            return;
        }
        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(goodsBaseId);
        //显示详细信息
        let goodsName = baseGoods.name;
        let goodsDesc = baseGoods.des;

        this.label_itemName1.text = goodsName;
        this.label_desc.text = goodsDesc;
        this.btn_use.visible = false;
        this.btn_compose.visible = false;
        this.box_slider.visible = false;
        if (baseGoods.use == 1) {
            this.box_slider.visible = true;
            this.btn_use.visible = true;
            let box_slider = this.box_slider as UI_Slider;
            box_slider.init(goodsNum);
        } else if (baseGoods.compose == 1) {
            this.btn_compose.visible = true;
        }
    }

    /**使用物品 */
    onUseGoods(): void {
        let boxSlider = this.box_slider as UI_Slider;
        let useNum = boxSlider.getValue();
        this.mgr.c_use_goods( this.mgr.selectGoodsBaseId, useNum);
    }
    
    /**删除物品 */ 
    onDeleteGoods():void {
        if(this.mgr.selectGoodsBaseId > 0){
            SceneManager.Instance.openScene(ESceneName.UI_BagDel);
        }
    }
}