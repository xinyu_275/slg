import VOGoods from "../Bag/VOGoods";
import Util from "../../Common/Core/Util";
import EventManager from "../../Common/Core/EventManager";
import { EEventType, EGoodsType } from "../../Config/EnumConfig";
import GoodsConfigManager from "../../Config/Data/GoodsConfigManager";

//背包物品列表
export default class UI_BagItemList extends Laya.List {
    onEnable():void {
        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
    }
    onDisable():void {
        this.array = [];
    }
    
    /**
     * 初始化list数据 
     * _datas: [{id:Id, num:Num, is_new?:boolean}]
     * clickHandler:选中后回调处理
     * canClick:是否可以点击
     */
    init(_datas:Object[], canClick:boolean = false) {
        this.vScrollBarSkin = "";
        this.array = _datas;
        if(canClick) {
            this.selectHandler = new Laya.Handler(this, this.onSelectHandler);
            if(this.array.length > 0) {
                this.selectedIndex = 0;
            }
        }
    }

    /**刷新 例如 删除了物品或者添加了物品*/
    refreshList(_datas:Object[]):void {
        this.array = _datas;
    }

    /**渲染 */
    onRenderHandler(cell: Laya.Box, index: number):void {
        if (index >= this.array.length){
            return;
        }
        //物品图标
        let imgItemIcon = cell.getChildByName("img_itemIcon") as Laya.Image;
        let imgRedPoint = cell.getChildByName("img_redpoint") as Laya.Image;
        let labelName = cell.getChildByName("label_itemName") as Laya.Label;
        let labelNum = cell.getChildByName("label_itemNum") as Laya.Label;
        let labelLv = cell.getChildByName("label_itemLv") as Laya.Label;
        let cellData = this.array[index];
        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(cellData.id);
        imgItemIcon.skin = baseGoods.icon;
        imgRedPoint.visible = cellData.is_new == true;
        labelName.text = baseGoods.name;
        labelNum.text = "X"+cellData.num;
        if(baseGoods.type == EGoodsType.stren) {
            labelLv.text = "lv."+baseGoods.lv;
        }else {
            labelLv.text = "";
        }
    }

    /** click*/
    onSelectHandler(index:number) {
        let cell:Laya.Box = this.selection;
        let cellData = this.array[index];
        cellData.is_new = false;
        let imgRedpoint = cell.getChildByName("img_redpoint") as Laya.Image;
        imgRedpoint.visible = false;
        EventManager.Instance.event(EEventType.BagItemListClick, cellData.id);
    }

    /**选区选中的goodsBaseId */
    getSelectGoodsBaseId():number {
        let select = this.selectedItem;
        if(select) {
            return select.id;
        }
        return -1;
    }
}