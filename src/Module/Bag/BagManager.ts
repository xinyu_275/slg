import AbsGameModule from "../AbsGameModule";
import NetSocketManager from "../../Common/NetSocketManager";
import VOGoods from "./VOGoods";
import SceneManager from "../../Common/SceneManager";
import GoodsConfigManager from "../../Config/Data/GoodsConfigManager";
import EventManager from "../../Common/Core/EventManager";
import { EEventType, ETipType } from "../../Config/EnumConfig";
import LogM from "../../Common/LogM";
import TipManager from "../Tip/TipManager";

export default class BagManager extends AbsGameModule {
    private mGoods:{[key:number]:VOGoods} = {};
    public selectGoodsBaseId: number = 0;          // 玩家选中的物品id
    constructor() {
        super();
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_bag_goods_list, this, this.s_get_bag_goods_list);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_update_goods_list, this, this.s_update_goods_list);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_use_goods, this, this.s_use_goods);
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_del_goods, this, this.s_del_goods);
    }
    //实例
    public static get Instance(): BagManager {
        return BagManager.getInstance<BagManager>();
    }

    /**玩家下线清理数据 */
    clear() : void {
        this.mGoods = {};
    }

    /**按tab排序 根据id从小到大排序后的物品列表 */
    getSortGoodsListByTab(tab:number = 0):VOGoods[]{
        let vos:VOGoods[] = [];
        for(let k in this.mGoods){
            let baseGoods = GoodsConfigManager.Instance.getBaseGoods(k);
            if(tab == baseGoods.tab - 1){
                vos.push(this.mGoods[k]);
            }
            
        }
        vos.sort((a:VOGoods, b:VOGoods) => a.id - b.id);
        return vos;
    }

    /**修改物品的isNew状态 */
    setIsNew(goodsBaseId:number, isNew:boolean = false): void {
        let vo = this.mGoods[goodsBaseId];
        vo && (vo.is_new = isNew);
    }

    /**根据baseid获取物品数量 */
    getGoodsNum(goodsBaseId:number):number {
        let vo = this.mGoods[goodsBaseId];
        if(vo) {
            return vo.num;
        }
        return 0;
    }

    /**判断物品是够足够 */
    isGoodsEnough(goodsBaseId:number, num:number = 1):boolean {
        return this.getGoodsNum(goodsBaseId) >= num;
    }
    //------------------------------协议处理----------------------------------------
    /**获取物品列表 */
    c_get_bag_goods_list():void {
        NetSocketManager.Instance.SendMsg("c_get_bag_goods_list", {});
    }
    s_get_bag_goods_list(data:awesomepackage.s_get_bag_goods_list):void {
        for(let item of data.goods_list) {
            let vo = new VOGoods();
            vo.init(item.goods_base_id, Number(item.num));
            this.mGoods[item.goods_base_id] = vo;
        }
    }
    
    /**更新列表 */
    s_update_goods_list(data:awesomepackage.s_update_goods_list): void{
        for(let item of data.goods_list) {
            let vo = this.mGoods[item.goods_base_id];
            let newNum = Number(item.num);
            if(vo && item.num > 0){
                if(vo.num < newNum){
                    vo.is_new = true;
                }
                vo.num = newNum;
            }else if(vo) {
                delete this.mGoods[item.goods_base_id];
            }else {
                vo = new VOGoods();
                vo.init(item.goods_base_id, newNum, true);
                this.mGoods[item.goods_base_id] = vo;
            }
        }
        EventManager.Instance.event(EEventType.BagItemListUpdate);
    }

    /**使用物品 */
    c_use_goods(goodsBaseId:number, useNum:number):void {
        let goodsNum = this.getGoodsNum(goodsBaseId);
        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(goodsBaseId);
        if(useNum <= 0 || goodsNum < useNum || baseGoods.use != 1){
            LogM.error("c_use_goods error:", useNum, goodsBaseId, goodsNum);
            return;
        }
        NetSocketManager.Instance.SendMsg("c_use_goods", {
            goods_base_id:goodsBaseId,
            num:useNum,
        });
    }
    s_use_goods(data:awesomepackage.s_use_goods):void {
        if(data.code != awesomepackage.e_code.ok) {
            TipManager.Instance.showMsgById(data.code);
            return;
        }
    }

    /**删除物品 */
    c_del_goods(goodsBaseId:number, num:number):void {
        if(goodsBaseId > 0 && num > 0)  {
            NetSocketManager.Instance.SendMsg("c_del_goods", {
                goods_base_id:goodsBaseId,
                num:num,
            });
        }
    }
    s_del_goods(data:awesomepackage.s_del_goods):void {
        TipManager.Instance.showMsgByType(ETipType.Normal, "删除成功");
    }
}