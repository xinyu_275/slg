export default class VOGoods {
    public id:number;                   // goodsBaseId
    public num:number;
    public is_new?:boolean = false;

    /**初始化 */
    public init(goodsBaseId:number, num:number, isNew = false):void {
        this.id = goodsBaseId;
        this.num = num;
        this.is_new = isNew;
    }
}