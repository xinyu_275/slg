import AbsGameModule from "../AbsGameModule";
import NetSocketManager from "../../Common/NetSocketManager";

export default class RankManager extends AbsGameModule {
    //实例
    public static get Instance(): RankManager {
        return RankManager.getInstance<RankManager>();
    }

    constructor() {
        super();
        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_player_rank_list, this, this.s_get_player_rank_list);
    }

    //-------------------------处理协议----------------------------
    /**获取排行榜数据 */
    c_get_player_rank_list(rankType:awesomepackage.e_rank_type, nowPage:number = 1):void {
        NetSocketManager.Instance.SendMsg("c_get_player_rank_list", {
            rank_type:rankType, 
            nowPage:nowPage,
        })
    }
    s_get_player_rank_list(data:awesomepackage.s_get_player_rank_list):void {

    }

    /**血盟排行榜 */
    c_get_guild_rank_list(nowPage:number = 1):void {
        NetSocketManager.Instance.SendMsg("c_get_guild_rank_list", {
            nowPage:nowPage,
        })
    }
    s_get_guild_rank_list(data:awesomepackage.s_get_guild_rank_list):void {
        
    }
}