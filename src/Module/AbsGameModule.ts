import Singleton from "../Common/Core/Singleton";

export interface IGameModule {
    scene_enter(): void;    // 场景进入
    scene_exit(): void;     // 场景退出
    clear() : void;         // 玩家下线清理
}

export default class AbsGameModule extends Singleton implements IGameModule {
    /**
     * 场景进入？
     */
    scene_enter(): void {

    }
    
    /**
     * 退出场景清除？
     */
    scene_exit(): void {

    }

    /**
     * 玩家下线清理
     */
    clear():void {
        
    }
}