import IState from "./State/IState";
import StateGameInit from "./State/StateGameInit";
import StateGameLogin from "./State/StateGameLogin";
import Common from "../Config/Common";
import LogM from "../Common/LogM";
import StateGameMain from "./State/StateGameMain";
import SwitchManager from "../Common/SwitchManager";
import StateGameWorld from "./State/StateGameWorld";

// 游戏状态控制器
export enum TypeStateGame {
    Init = "init",
    Login = "login",
    Main = "main",          // 主界面
    World = "world",
}

export default class GameStateManager {
    private static _instance: GameStateManager = null;
    private states: { [key: number]: IState } = {};
    private _stateCurrent: IState = null;            // 当前state
    private _curState: TypeStateGame = null;         // 当前状态
    private _prevState:TypeStateGame = null;         // 上一状态
    private _switchingState: TypeStateGame = null;   // 正在切换的状态

    public static get Instance(): GameStateManager {
        if (!this._instance) {
            this._instance = new GameStateManager();
        }
        return this._instance;
    }


    /**
     * 初始化
     */
    public start(): void {
        console.log("game start!!!")
        //1.初始化状态
        this.initStates();
        this.switchState(TypeStateGame.Init);
    }

    /**
     * 转换状态
     */
    public switchState(type: TypeStateGame): void {
        if (type == this._switchingState) {
            return;
        }
        if (type == this._curState) {
            return;
        }
        //添加场景切换动画
        if (type !== TypeStateGame.Init && type !== TypeStateGame.Login) {
            this.addSwitchAnim(type);
            return;
        }
        this.doSwitchState(type);
    }
    //添加场景动画
    private addSwitchAnim(type: TypeStateGame): void {
        this._switchingState = type;
        SwitchManager.Instance.openView(Laya.Handler.create(this, this.doSwitchState, [type]));
        return;
    }
    private doSwitchState(type: TypeStateGame): void {
        this._switchingState = null;
        if (type == this._curState) {
            return;
        }
        if (this._stateCurrent) {
            this._stateCurrent.exit();
            LogM.info("state:" + this._curState + " exit");
        }
        this._prevState = this._curState;
        this._curState = type;
        this._stateCurrent = this.states[type];
        LogM.info("state:" + this._curState + " enter");
        this._stateCurrent.enter();
    }

    /**
     * 初始化状态列表
     */
    private initStates(): void {
        this.states[TypeStateGame.Init] = new StateGameInit();
        this.states[TypeStateGame.Login] = new StateGameLogin();
        this.states[TypeStateGame.Main] = new StateGameMain();
        this.states[TypeStateGame.World] = new StateGameWorld();
    }

    /**
     *  获取当前状态
     */
    public get curState(): TypeStateGame {
        return this._curState;
    }

    /**
     * 获取上一个状态
     */
    public get prevState() : TypeStateGame {
        return this._prevState;
    }
}