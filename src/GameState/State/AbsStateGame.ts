import IState from "./IState";
import { IGameModule } from "../../Module/AbsGameModule";

export default class AbsStateGame implements IState {
    private initedOnlyOnce:boolean = false;
    //模块列表
    public mods: IGameModule[] = [];
    
    //enter 进入场景
    enter(): void {
       if(!this.initedOnlyOnce) {
           this.initedOnlyOnce = true;
           this.initOnlyOnce();
       }
    }
    
    // 退出场景
    exit(): void {

    }

    // 只初始化一次
    initOnlyOnce(): void {

    }

    //重置模块信息
    enterModules(): void {
        for (let mod of this.mods) {
            mod.scene_enter();
        }
    }

    //清除模块信息
    exitModules(): void {
        for (let mod of this.mods) {
            mod.scene_exit();
        }
    }
}