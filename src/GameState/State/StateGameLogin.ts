import AbsStateGame from "./AbsStateGame";
import GameStateManager, { TypeStateGame } from "../GameStateManager";
import { EEventType } from "../../Config/EnumConfig";
import EventManager from "../../Common/Core/EventManager";
import LoginManager from "../../Module/Login/LoginManager";
import ConfigDataManager from "../../Config/Data/ConfigDataManager";

export default class StateGameLogin extends AbsStateGame {
    //进入
    enter(): void {
        super.enter();

        //模块重置
        this.enterModules();

        LoginManager.Instance.openView();
    }

    /**
    * 只初始化一次
    */
    initOnlyOnce(): void {
        // 登录模块
        this.mods.push(LoginManager.Instance);

        //监听完成状态
        EventManager.Instance.on(EEventType.LoginSuccess, this, this.onLoginSuccess);
    }
    private onLoginSuccess() {
        //生成配置
        ConfigDataManager.Instance.parse();
        // 转到主界面
        GameStateManager.Instance.switchState(TypeStateGame.Main);
    }

    // 退出
    public exit(): void {
        super.exit();
        this.exitModules();
    }
}