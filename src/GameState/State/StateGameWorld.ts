import AbsStateGame from "./AbsStateGame";
import GameStateManager, { TypeStateGame } from "../GameStateManager";
import WorldManager from "../../Module/World/WorldManager";


export default class StateGameWorld extends AbsStateGame {
    //进入
    enter(): void {
        super.enter();

        //场景进入
        this.enterModules();

        //加载世界地图
        WorldManager.Instance.loadWorldMap();
    }

    /**
    * 只初始化一次
    */
    initOnlyOnce(): void {
        this.mods.push(WorldManager.Instance);        // 世界
    }

    // 退出
    public exit(): void {
        super.exit();
        this.exitModules();
    }
}