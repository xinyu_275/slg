import AbsStateGame from "./AbsStateGame";
import GameStateManager, { TypeStateGame } from "../GameStateManager";
import { EEventType } from "../../Config/EnumConfig";
import EventManager from "../../Common/Core/EventManager";
import LoginManager from "../../Module/Login/LoginManager";
import PlayerManager from "../../Module/Player/PlayerManager";
import LobbyManager from "../../Module/Lobby/LobbyManager";
import UI_Lobby from "../../Module/Lobby/UI_Lobby";
import BagManager from "../../Module/Bag/BagManager";
import SceneManager from "../../Common/SceneManager";
import GeneralManager from "../../Module/General/GeneralManager";
import MailManager from "../../Module/Mail/MailManager";
import RankManager from "../../Module/Rank/RankManager";
import BattleManager from "../../Module/Battle/BattleManager";

export default class StateGameMain extends AbsStateGame {
    //进入
    enter(): void {
        super.enter();

        //场景进入
        this.enterModules();

        let prevLoginState = GameStateManager.Instance.prevState == TypeStateGame.Login;
        //打开大厅界面
        LobbyManager.Instance.openView(!prevLoginState);
        //如果玩家是登录/重连进入主城，那就需要发送初始化玩家数据协议
        if (prevLoginState) {
            this.sendInitGameProto();
        }
    }

    /**
    * 只初始化一次
    */
    initOnlyOnce(): void {
        this.mods.push(PlayerManager.Instance);     // 玩家管理器
        this.mods.push(LobbyManager.Instance);      // 大厅管理
        this.mods.push(BagManager.Instance);        // 背包
        this.mods.push(GeneralManager.Instance);    // 武将
        this.mods.push(MailManager.Instance);       // 邮件
        this.mods.push(RankManager.Instance);       // 排行榜
        this.mods.push(BattleManager.Instance);     // 战斗
    }

    // 退出
    public exit(): void {
        super.exit();
        this.exitModules();
    }

    //玩家登录初始化游戏需要发送协议
    sendInitGameProto(): void {
        //1.获取玩家数据协议
        PlayerManager.Instance.c_get_player_info();
        //2.获取背包数据
        BagManager.Instance.c_get_bag_goods_list();
        //3.获取武将列表
        GeneralManager.Instance.c_get_general_list();
        //4.获取邮件列表
        MailManager.Instance.c_mail_list();
        
        //todo 接受到所有协议数据后刷新整个大厅界面
        LobbyManager.Instance.refreshView(true);
    }
}