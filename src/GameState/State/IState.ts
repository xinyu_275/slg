export default interface IState {
    enter(): void;
    exit(): void;
    initOnlyOnce(): void;
}
