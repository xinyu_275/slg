import AbsStateGame from "./AbsStateGame";
import Common from "../../Config/Common";
import GameStateManager, { TypeStateGame } from "../GameStateManager";
import { RefreshManager } from "../../Common/Core/RefreshManager";
import TimeManager from "../../Common/Core/TimeManager";

export default class StateGameInit extends AbsStateGame {
    // 进入
    public enter(): void {
        super.enter();
        //是否显示输入帐号界面
        if (Common.OpenTestAccountPanel) {

        } else {
            GameStateManager.Instance.switchState(TypeStateGame.Login);
        }
    }
    // 重写只初始化一次
    public initOnlyOnce(): void {
        // 镇循环管理器
        RefreshManager.Instance;
        // 时间管理器
        TimeManager.Instance;
        // 时间管理器加到帧循环中
        RefreshManager.Instance.add(TimeManager.Instance);
    }

    // 退出
    public exit(): void {
        super.exit();
    }
}