(function () {
	'use strict';

	var REG = Laya.ClassUtils.regClass;
	var ui;
	(function (ui) {
	    var Panel;
	    (function (Panel) {
	        var Bag;
	        (function (Bag) {
	            class BagUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Bag/Bag");
	                }
	            }
	            Bag.BagUI = BagUI;
	            REG("ui.Panel.Bag.BagUI", BagUI);
	            class BagDelUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Bag/BagDel");
	                }
	            }
	            Bag.BagDelUI = BagDelUI;
	            REG("ui.Panel.Bag.BagDelUI", BagDelUI);
	            class GoodsDescUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Bag/GoodsDesc");
	                }
	            }
	            Bag.GoodsDescUI = GoodsDescUI;
	            REG("ui.Panel.Bag.GoodsDescUI", GoodsDescUI);
	        })(Bag = Panel.Bag || (Panel.Bag = {}));
	    })(Panel = ui.Panel || (ui.Panel = {}));
	})(ui || (ui = {}));
	(function (ui) {
	    var Panel;
	    (function (Panel) {
	        var Battle;
	        (function (Battle) {
	            class BattleUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Battle/Battle");
	                }
	            }
	            Battle.BattleUI = BattleUI;
	            REG("ui.Panel.Battle.BattleUI", BattleUI);
	        })(Battle = Panel.Battle || (Panel.Battle = {}));
	    })(Panel = ui.Panel || (ui.Panel = {}));
	})(ui || (ui = {}));
	(function (ui) {
	    var Panel;
	    (function (Panel) {
	        var General;
	        (function (General) {
	            class GeneralUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/General/General");
	                }
	            }
	            General.GeneralUI = GeneralUI;
	            REG("ui.Panel.General.GeneralUI", GeneralUI);
	        })(General = Panel.General || (Panel.General = {}));
	    })(Panel = ui.Panel || (ui.Panel = {}));
	})(ui || (ui = {}));
	(function (ui) {
	    var Panel;
	    (function (Panel) {
	        var Loading;
	        (function (Loading) {
	            class LoginLoadingUI extends Laya.Scene {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Loading/LoginLoading");
	                }
	            }
	            Loading.LoginLoadingUI = LoginLoadingUI;
	            REG("ui.Panel.Loading.LoginLoadingUI", LoginLoadingUI);
	        })(Loading = Panel.Loading || (Panel.Loading = {}));
	    })(Panel = ui.Panel || (ui.Panel = {}));
	})(ui || (ui = {}));
	(function (ui) {
	    var Panel;
	    (function (Panel) {
	        var Lobby;
	        (function (Lobby) {
	            class LobbyUI extends Laya.Scene {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Lobby/Lobby");
	                }
	            }
	            Lobby.LobbyUI = LobbyUI;
	            REG("ui.Panel.Lobby.LobbyUI", LobbyUI);
	            class RewardPopupUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Lobby/RewardPopup");
	                }
	            }
	            Lobby.RewardPopupUI = RewardPopupUI;
	            REG("ui.Panel.Lobby.RewardPopupUI", RewardPopupUI);
	        })(Lobby = Panel.Lobby || (Panel.Lobby = {}));
	    })(Panel = ui.Panel || (ui.Panel = {}));
	})(ui || (ui = {}));
	(function (ui) {
	    var Panel;
	    (function (Panel) {
	        var Mail;
	        (function (Mail) {
	            class MailUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Mail/Mail");
	                }
	            }
	            Mail.MailUI = MailUI;
	            REG("ui.Panel.Mail.MailUI", MailUI);
	            class MailSystemUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Mail/MailSystem");
	                }
	            }
	            Mail.MailSystemUI = MailSystemUI;
	            REG("ui.Panel.Mail.MailSystemUI", MailSystemUI);
	        })(Mail = Panel.Mail || (Panel.Mail = {}));
	    })(Panel = ui.Panel || (ui.Panel = {}));
	})(ui || (ui = {}));
	(function (ui) {
	    var Panel;
	    (function (Panel) {
	        var Tip;
	        (function (Tip) {
	            class AlertUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Tip/Alert");
	                }
	            }
	            Tip.AlertUI = AlertUI;
	            REG("ui.Panel.Tip.AlertUI", AlertUI);
	            class TipUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/Tip/Tip");
	                }
	            }
	            Tip.TipUI = TipUI;
	            REG("ui.Panel.Tip.TipUI", TipUI);
	        })(Tip = Panel.Tip || (Panel.Tip = {}));
	    })(Panel = ui.Panel || (ui.Panel = {}));
	})(ui || (ui = {}));
	(function (ui) {
	    var Panel;
	    (function (Panel) {
	        var World;
	        (function (World) {
	            class WorldMonsterUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/World/WorldMonster");
	                }
	            }
	            World.WorldMonsterUI = WorldMonsterUI;
	            REG("ui.Panel.World.WorldMonsterUI", WorldMonsterUI);
	            class WorldPlayerCityUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/World/WorldPlayerCity");
	                }
	            }
	            World.WorldPlayerCityUI = WorldPlayerCityUI;
	            REG("ui.Panel.World.WorldPlayerCityUI", WorldPlayerCityUI);
	            class WorldRobCityUI extends Laya.Dialog {
	                constructor() { super(); }
	                createChildren() {
	                    super.createChildren();
	                    this.loadScene("Panel/World/WorldRobCity");
	                }
	            }
	            World.WorldRobCityUI = WorldRobCityUI;
	            REG("ui.Panel.World.WorldRobCityUI", WorldRobCityUI);
	        })(World = Panel.World || (Panel.World = {}));
	    })(Panel = ui.Panel || (ui.Panel = {}));
	})(ui || (ui = {}));

	class Common {
	    static get OpenTestAccountPanel() {
	        return this.DebugMode && this._openTestAccountPanel;
	    }
	}
	Common.DebugMode = true;
	Common._openTestAccountPanel = false;
	Common.TileWidth = 160;
	Common.TileHeight = 116;
	Common.WorldTileMapJson = "res/worldTiledMap/world.json";
	Common.DefaultMapScale = 1;
	Common.MaxMapScale = 2;
	Common.MinMapScale = 0.5;
	Common.MapBlockGridWidth = 10;
	Common.MapBlockGridHeight = 10;

	var ELogType;
	(function (ELogType) {
	    ELogType[ELogType["DEBUG"] = 0] = "DEBUG";
	    ELogType[ELogType["INFO"] = 1] = "INFO";
	    ELogType[ELogType["WARN"] = 2] = "WARN";
	    ELogType[ELogType["ERROR"] = 3] = "ERROR";
	})(ELogType || (ELogType = {}));
	class LogM {
	    static set defaultLogType(logType) {
	        this._defaultLogType = logType;
	    }
	    static debug(messgge, ...params) {
	        this.doLog(ELogType.DEBUG, messgge, ...params);
	    }
	    static info(messgge, ...params) {
	        this.doLog(ELogType.INFO, messgge, ...params);
	    }
	    static warn(messgge, ...params) {
	        this.doLog(ELogType.WARN, messgge, ...params);
	    }
	    static error(messgge, ...params) {
	        this.doLog(ELogType.ERROR, messgge, ...params);
	    }
	    static doLog(logType, message, ...params) {
	        if (!Common.DebugMode && logType <= this._defaultLogType) {
	            return;
	        }
	        switch (logType) {
	            case ELogType.DEBUG:
	                console.info(message, ...params);
	                break;
	            case ELogType.INFO:
	                console.log(message, ...params);
	                break;
	            case ELogType.WARN:
	                console.warn(message, ...params);
	                break;
	            case ELogType.ERROR:
	                console.error(message, ...params);
	                break;
	        }
	    }
	}
	LogM._defaultLogType = ELogType.INFO;

	class Singleton {
	    static getInstance() {
	        if (!this._instance) {
	            this._instance = new this();
	        }
	        return this._instance;
	    }
	}
	Singleton._instance = null;

	class AbsGameModule extends Singleton {
	    scene_enter() {
	    }
	    scene_exit() {
	    }
	    clear() {
	    }
	}

	class TimeManager {
	    static get Instance() {
	        if (!this._instance) {
	            this._instance = new TimeManager();
	        }
	        return this._instance;
	    }
	    initServerTime(time) {
	        this._serverMillTime = time;
	        this._serverTime = (this._serverMillTime / 1000) | 0;
	        this._diffMillTime = this._serverMillTime - this.localMillTime;
	        this._isSyncTime = true;
	    }
	    refresh(dt) {
	        if (!this._isSyncTime)
	            return;
	        this._serverMillTime = this.localMillTime + this._diffMillTime;
	        this._serverTime = (this._serverMillTime / 1000) | 0;
	    }
	    get serverTime() {
	        if (this._serverTime === void 0) {
	            console.warn("服务器时间还未同步，请先同步时间！");
	            return null;
	        }
	        return this._serverTime;
	    }
	    get serverMillTime() {
	        if (this._serverMillTime === void 0) {
	            console.warn("服务器时间还未同步，请先同步时间！");
	            return null;
	        }
	        return this._serverMillTime;
	    }
	    get localTime() {
	        const time = (Date.now() / 1000) | 0;
	        return time;
	    }
	    get localMillTime() {
	        return Date.now();
	    }
	    getServerNextHourTime(hour = 5) {
	        const d = new Date(this.serverMillTime);
	        let year = d.getFullYear();
	        let month = d.getMonth() + 1;
	        let day = d.getDate();
	        let nowHour = d.getHours();
	        let date = new Date("" + year + "-" + month + "-" + day + " 00:00:00");
	        if (nowHour < hour) {
	            return ((date.getTime() / 1000) | 0) + 5 * 3600;
	        }
	        else {
	            return ((date.getTime() / 1000) | 0) + 86400 + 5 * 3600;
	        }
	    }
	}
	TimeManager._instance = null;

	class EventManager extends Laya.EventDispatcher {
	    static get Instance() {
	        if (!this._instance) {
	            this._instance = new EventManager();
	        }
	        return this._instance;
	    }
	}

	var ETipType;
	(function (ETipType) {
	    ETipType[ETipType["Normal"] = 0] = "Normal";
	    ETipType[ETipType["Flash"] = 1] = "Flash";
	})(ETipType || (ETipType = {}));
	var EGoodsType;
	(function (EGoodsType) {
	    EGoodsType[EGoodsType["stren"] = 7] = "stren";
	})(EGoodsType || (EGoodsType = {}));
	var EMailType;
	(function (EMailType) {
	    EMailType[EMailType["SystemMail"] = 0] = "SystemMail";
	    EMailType[EMailType["BattleMail"] = 1] = "BattleMail";
	    EMailType[EMailType["GuildMail"] = 2] = "GuildMail";
	})(EMailType || (EMailType = {}));
	var EEventType;
	(function (EEventType) {
	    EEventType["LoginSuccess"] = "LoginSuccess";
	    EEventType["OpenSocketSuccess"] = "OpenSocketSuccess";
	    EEventType["LoginLoadingProgress"] = "LoginLoadingProgress";
	    EEventType["CloseLoginLoadingView"] = "CloseLoginLoadingView";
	    EEventType["PlayerResUpdate"] = "PlayerResUpdate";
	    EEventType["PlayerPosUpdate"] = "PlayerPosUpdate";
	    EEventType["LobbyRefreshView"] = "LobbyRefreshView";
	    EEventType["BagItemListClick"] = "BagItemListClick";
	    EEventType["BagItemListUpdate"] = "BagItemListUpdate";
	    EEventType["MapMouseClick"] = "MapMouseClick";
	    EEventType["MapMouseDown"] = "MapMouseDown";
	    EEventType["MailListUpdate"] = "MailListUpdate";
	    EEventType["MailMessageUpdate"] = "MailMessageUpdate";
	    EEventType["MailRewardSuccess"] = "MailRewardSuccess";
	    EEventType["MailRead"] = "MailRead";
	    EEventType["MailPageUpdate"] = "MailPageUpdate";
	})(EEventType || (EEventType = {}));
	var EPoolName;
	(function (EPoolName) {
	    EPoolName["WorldItem"] = "WorldItem";
	    EPoolName["WorldLine"] = "WorldLine";
	    EPoolName["Animation"] = "Animation";
	})(EPoolName || (EPoolName = {}));
	var EAniPath;
	(function (EAniPath) {
	    EAniPath["WorldFire"] = "res/atlas/image/worldMap/fireAni.atlas";
	    EAniPath["WorldProtect"] = "res/atlas/image/worldMap/protectAni.atlas";
	    EAniPath["WorldFight"] = "res/atlas/image/worldMap/fightAni.atlas";
	})(EAniPath || (EAniPath = {}));
	class SceneConfig {
	    constructor() {
	        this.sceneUrl = "";
	        this.titleUrl = "";
	        this.titleName = "";
	    }
	}
	var ESceneName;
	(function (ESceneName) {
	    ESceneName["UI_LoginLoading"] = "UI_LoginLoading";
	    ESceneName["UI_Lobby"] = "UI_Lobby";
	    ESceneName["UI_Tip"] = "UI_Tip";
	    ESceneName["UI_Alert"] = "UI_Alert";
	    ESceneName["UI_Bag"] = "UI_Bag";
	    ESceneName["UI_BagDel"] = "UI_BagDel";
	    ESceneName["UI_GoodsDesc"] = "UI_GoodsDesc";
	    ESceneName["UI_Mail"] = "UI_Mail";
	    ESceneName["UI_MailSystem"] = "UI_MailSystem";
	    ESceneName["UI_RewardPopup"] = "UI_RewardPopup";
	    ESceneName["UI_General"] = "UI_General";
	    ESceneName["UI_Battle"] = "UI_Battle";
	    ESceneName["UI_WorldMonster"] = "UI_WorldMonster";
	    ESceneName["UI_WorldPlayerCity"] = "UI_WorldPlayerCity";
	    ESceneName["UI_WorldRobCity"] = "UI_WorldRobCity";
	})(ESceneName || (ESceneName = {}));
	const SceneConfigMap = {
	    "UI_LoginLoading": { sceneUrl: "Panel/Loading/LoginLoading.scene" },
	    "UI_Lobby": { sceneUrl: "Panel/Lobby/Lobby.scene" },
	    "UI_Tip": { sceneUrl: "Panel/Tip/Tip.json" },
	    "UI_Alert": { sceneUrl: "Panel/Tip/Alert.json" },
	    "UI_Bag": { sceneUrl: "Panel/Bag/Bag.json", titleUrl: "image/bag/bag.png" },
	    "UI_BagDel": { sceneUrl: "Panel/Bag/BagDel.json", titleName: "删除" },
	    "UI_GoodsDesc": { sceneUrl: "Panel/Bag/GoodsDesc.json" },
	    "UI_Mail": { sceneUrl: "Panel/Mail/Mail.json", titleUrl: "image/mail/mail.png" },
	    "UI_MailSystem": { sceneUrl: "Panel/Mail/MailSystem.json", titleUrl: "image/mail/mail.png" },
	    "UI_RewardPopup": { sceneUrl: "Panel/Lobby/RewardPopup.json" },
	    "UI_General": { sceneUrl: "Panel/General/General.json", titleUrl: "image/general/general.png" },
	    "UI_Battle": { sceneUrl: "Panel/Battle/Battle.json" },
	    "UI_WorldMonster": { sceneUrl: "Panel/World/WorldMonster.json" },
	    "UI_WorldPlayerCity": { sceneUrl: "Panel/World/WorldPlayerCity.json" },
	    "UI_WorldRobCity": { sceneUrl: "Panel/World/WorldRobCity.json" },
	};
	var EPrefabPath;
	(function (EPrefabPath) {
	    EPrefabPath["BoxSoldier"] = "Panel/Battle/BoxSoldier.prefab";
	    EPrefabPath["BattleHp"] = "Panel/Battle/BattleHp.prefab";
	})(EPrefabPath || (EPrefabPath = {}));

	class LockManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.locks = {};
	    }
	    static get Instance() {
	        return LockManager.getInstance();
	    }
	    isLock(key, expireTime = 5000) {
	        let v = this.locks[key];
	        let nowTime = TimeManager.Instance.localMillTime;
	        if (!v || nowTime - v > expireTime) {
	            this.locks[key] = nowTime;
	            return false;
	        }
	        return true;
	    }
	}

	class SceneManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.scenes = {};
	    }
	    static get Instance() {
	        return SceneManager.getInstance();
	    }
	    openScene(sceneName, initEventParams, closeOther = false, complete) {
	        let inst = this.scenes[sceneName];
	        if (inst) {
	            this.showScene(inst, closeOther);
	            EventManager.Instance.event(sceneName, initEventParams);
	            complete && complete.runWith(inst);
	            return;
	        }
	        if (LockManager.Instance.isLock(sceneName)) {
	            return;
	        }
	        let sceneConfig = SceneConfigMap[sceneName];
	        if (!sceneConfig) {
	            LogM.error("openDialog sceneName" + sceneName + "not config");
	            return;
	        }
	        Laya.Scene.open(sceneConfig.sceneUrl, closeOther, null, Laya.Handler.create(this, this.openSceneCallback, [sceneName, initEventParams, complete]));
	    }
	    openSceneCallback(sceneName, initEventParams, complete, scene) {
	        if (scene.autoDestroyAtClosed) {
	            LogM.error("openSceneCallback sceneName error, this scene cannot use autoDestroyAtClosed = true, sceneName:", sceneName);
	        }
	        let sceneConfig = SceneConfigMap[sceneName];
	        this.scenes[sceneName] = scene;
	        EventManager.Instance.event(sceneName, initEventParams);
	        complete && complete.runWith(scene);
	        let header = scene.getChildByName("headers");
	        if (sceneConfig.titleUrl && header && header.getChildByName("img_title")) {
	            header.getChildByName("img_title").skin = sceneConfig.titleUrl;
	        }
	        else if (sceneConfig.titleName && header && header.getChildByName("label_title")) {
	            header.getChildByName("label_title").text = sceneConfig.titleName;
	        }
	    }
	    openRewardPopup(rewardList) {
	        this.openScene(ESceneName.UI_RewardPopup, [rewardList]);
	    }
	    openAlert(text, title = "提示", handlerYes = null, type = 0, showTip = false, handlerNo = null, yes = "确认", no = "取消") {
	        let Params = [title, text, handlerYes, type, showTip, handlerNo, yes, no];
	        SceneManager.Instance.openScene(ESceneName.UI_Alert, Params);
	    }
	    showScene(inst, closeOther = false) {
	        if (inst.parent) {
	            return;
	        }
	        if (inst instanceof Laya.Dialog) {
	            if (inst.isModal) {
	                console.log("=============", inst.isShowEffect);
	                inst.popup(closeOther, inst.isShowEffect);
	            }
	            else {
	                inst.show(closeOther, inst.isShowEffect);
	            }
	        }
	        else {
	            Laya.stage.addChild(inst);
	        }
	    }
	}

	class TipManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.cacheNormalMsg = [];
	    }
	    static get Instance() {
	        return TipManager.getInstance();
	    }
	    showMsgById(id) {
	        let tmpConfig;
	        let type = ETipType.Normal;
	        let text = "";
	        if (tmpConfig == null) {
	            this.showMsgByType(ETipType.Normal, id + "没有配，请检查配置");
	            return;
	        }
	        this.showMsgByType(type, text);
	    }
	    showMsgParam(id, params) {
	    }
	    showMsgByType(type, text) {
	        switch (type) {
	            case ETipType.Normal:
	                this.cacheNormalMsg.push(text);
	                SceneManager.Instance.openScene(ESceneName.UI_Tip);
	                break;
	            case ETipType.Flash:
	                break;
	            default:
	                break;
	        }
	    }
	    showNormalMsg(text) {
	        this.showMsgByType(ETipType.Normal, text);
	    }
	}

	class MsgCaller {
	    constructor(caller, fun) {
	        this.callBack = fun;
	        this.caller = caller;
	    }
	}
	class NetSocketManager extends Singleton {
	    constructor() {
	        super();
	        this.MsgCallBackDic = {};
	        this.MsgCallerDic = {};
	        this.NetHeartNum = 0;
	        this.Ping = 0;
	        this.InitNet = false;
	        this.reconnect = false;
	        this.check = false;
	        this.RecvByte = new Laya.Byte();
	        this.RecvByte.endian = "LITTLE_ENDIAN";
	        this.SendByte = new Laya.Byte();
	        this.SendByte.endian = "LITTLE_ENDIAN";
	    }
	    static get Instance() {
	        return NetSocketManager.getInstance();
	    }
	    Init(server, proto) {
	        LogM.info("NetSocketManager:" + server);
	        this.serverip = server;
	        this.RegisterNet(awesomepackage.serviceNo.s_heartbeat, this, this.s_heartbeat);
	        this.connect();
	        Laya.timer.loop(2000, this, this.NetHeart);
	    }
	    connect() {
	        this.setTips("正在获取服务器信息...");
	        this.webSocket = new WebSocket(this.serverip);
	        this.webSocket.binaryType = "arraybuffer";
	        this.webSocket.onopen = () => { this.onSocketOpen(); };
	        this.webSocket.onclose = () => { this.onSocketClose(); };
	        this.webSocket.onmessage = (obj) => { this.onMessageReveived(obj); };
	        this.webSocket.onerror = (err) => { this.onConnectError(err); };
	    }
	    close() {
	        if (this.webSocket == null) {
	            return;
	        }
	        this.webSocket.close();
	        this.webSocket = null;
	        this.NetConnect = false;
	        this.reconnect = false;
	    }
	    sleep(delay) {
	        var start = (new Date()).getTime();
	        while ((new Date()).getTime() - start < delay) {
	            continue;
	        }
	    }
	    onSocketOpen() {
	        LogM.info("onSocketOpen");
	        this.InitNet = true;
	        this.NetConnect = true;
	        this.reconnect = false;
	        EventManager.Instance.event(EEventType.OpenSocketSuccess);
	    }
	    onSocketClose() {
	        LogM.info("onSocketClose");
	        this.InitNet = true;
	        this.NetConnect = false;
	        this.reconnect = false;
	        this.setTips("与服务器断开连接...");
	    }
	    onMessageReveived(message) {
	        if (message != null) {
	            this.Recv(message.data);
	        }
	    }
	    onConnectError(e) {
	        this.InitNet = true;
	        this.NetConnect = false;
	        this.reconnect = false;
	        this.setTips("与服务器断开连接...");
	    }
	    SendMsg(name, data) {
	        var MsgData = awesomepackage[name];
	        if (MsgData == null) {
	            console.warn("NetSocketManager SendMsg:" + name + " not found");
	            return;
	        }
	        this.doSend(awesomepackage.serviceNo[name], MsgData, data);
	    }
	    doSend(id, msgData, data) {
	        var buf = msgData.encode(data).finish();
	        if (this.SendByte != null) {
	            this.SendByte.clear();
	            this.SendByte.writeInt16(id);
	            this.SendByte.writeArrayBuffer(buf, 0, buf.len);
	            if (this.NetConnect && this.webSocket != null) {
	                this.webSocket.send(this.SendByte.buffer);
	            }
	        }
	    }
	    Recv(data) {
	        if (data == null) {
	            return;
	        }
	        this.RecvByte.clear();
	        this.RecvByte.writeArrayBuffer(data);
	        if (this.RecvByte.length > 0) {
	            this.RecvByte.pos = 0;
	            this.RecvID = this.RecvByte.getInt16();
	            let classType = awesomepackage.serviceNo[this.RecvID];
	            if (!classType) {
	                return;
	            }
	            var headData;
	            headData = awesomepackage[classType].decode(this.RecvByte.getUint8Array(2, this.RecvByte.length));
	            if (headData == null)
	                return;
	            var call = this.MsgCallBackDic[this.RecvID];
	            if (call) {
	                for (var i = 0; i < call.length; i++) {
	                    call[i].callBack.call(call[i].caller, headData);
	                }
	            }
	        }
	    }
	    RegisterNet(id, caller, handler) {
	        var call = this.MsgCallBackDic[id];
	        if (!call) {
	            call = [new MsgCaller(caller, handler)];
	            this.MsgCallBackDic[id] = call;
	            return;
	        }
	        for (let index = 0; index < call.length; index++) {
	            if (call[index].caller === caller && call[index].callBack === handler) {
	                return;
	            }
	        }
	        call[call.length] = new MsgCaller(caller, handler);
	    }
	    UnRegisterNet(id, handler) {
	        var call = this.MsgCallBackDic[id];
	        if (call != null) {
	            for (var i = call.length - 1; i >= 0; i--) {
	                if (call[i].callBack == handler) {
	                    call.splice(i, 1);
	                }
	            }
	        }
	    }
	    ReConnect() {
	        if (this.reconnect) {
	            console.warn("正在重连中,不要重复重连...");
	            return;
	        }
	        this.setTips("正在重新连接服务器...");
	        if (this.webSocket != null) {
	            this.webSocket.close();
	            this.NetConnect = false;
	        }
	        if (this.serverip != null && this.serverip != "") {
	            this.connect();
	            this.reconnect = true;
	        }
	    }
	    NetHeart() {
	        if (!this.InitNet)
	            return;
	        this.NetHeartNum = this.NetHeartNum + 1;
	        this.PingTest = Laya.timer.currTimer;
	        this.c_heartbeat();
	        if (!this.NetConnect || this.NetHeartNum > 2) {
	            console.log("NetHeart Reconect, NetHeartNum:" + this.NetHeartNum);
	            this.NetHeartNum = 0;
	            this.ReConnect();
	        }
	    }
	    c_heartbeat() {
	        this.SendMsg("c_heartbeat", {});
	    }
	    s_heartbeat(data) {
	        this.NetHeartNum = this.NetHeartNum - 1;
	        TimeManager.Instance.initServerTime(data.time);
	    }
	    setTips(message) {
	        TipManager.Instance.showMsgByType(ETipType.Normal, message);
	    }
	}

	class VOGoods {
	    constructor() {
	        this.is_new = false;
	    }
	    init(goodsBaseId, num, isNew = false) {
	        this.id = goodsBaseId;
	        this.num = num;
	        this.is_new = isNew;
	    }
	}

	class BaseSoldier {
	    constructor() {
	        this.type = 1;
	        this.lv = 1;
	        this.attr_template_id = 0;
	        this.nextId = 0;
	        this.def_icon = "";
	        this.size = 0;
	        this.mPivotX = 0;
	        this.mPivotY = 0;
	        this.lv_up_des = "";
	        this.skill_compose = 0;
	        this.power = 0;
	    }
	}
	class SoldierConfigManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.mSoldiers = {};
	    }
	    static get Instance() {
	        return SoldierConfigManager.getInstance();
	    }
	    parse() {
	        let mgr = ConfigDataManager.Instance;
	        this.parseSoldier(mgr["soldier"]);
	        delete mgr["soldier"];
	    }
	    parseSoldier(jsonData) {
	        for (let baseId in jsonData) {
	            let intBaseId = Number(baseId);
	            this.mSoldiers[intBaseId] = jsonData[baseId];
	        }
	    }
	    getBaseSoldier(soldierBaseId) {
	        return this.mSoldiers[soldierBaseId];
	    }
	    getSoldierPower(soldierBaseId, num = 1) {
	        return this.mSoldiers[soldierBaseId].power * num;
	    }
	}

	class BaseCityType {
	    constructor() {
	        this.size = 1;
	        this.cost_phy = 0;
	        this.icon = "";
	        this.goods_id_list = [];
	    }
	}
	class BasePvpArmy {
	}
	class WorldConfigManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.mBaseCitys = {};
	        this.mPvpArmys = {};
	    }
	    static get Instance() {
	        return WorldConfigManager.getInstance();
	    }
	    parse() {
	        let mgr = ConfigDataManager.Instance;
	        this.parseCity(mgr["city_type"]);
	        delete mgr["city_type"];
	        this.parsePvpArmy(mgr['pvp_army']);
	        delete mgr['pvp_army'];
	    }
	    parseCity(jsonData) {
	        for (let city_type in jsonData) {
	            let intCityType = Number(city_type);
	            this.mBaseCitys[intCityType] = jsonData[city_type];
	        }
	    }
	    getBaseCityTypeInfo(cityType) {
	        return this.mBaseCitys[cityType];
	    }
	    getCitySize(cityType) {
	        return this.mBaseCitys[cityType].size;
	    }
	    getCityName(cityType) {
	        return this.mBaseCitys[cityType].name;
	    }
	    getCityIcon(cityType) {
	        return this.mBaseCitys[cityType].icon;
	    }
	    parsePvpArmy(jsonData) {
	        for (let k in jsonData) {
	            let intK = Number(k);
	            let item = jsonData[intK];
	            let vid = this.makeVirtualPVPArmyId(item.city_type, item.lv);
	            this.mPvpArmys[vid] = item;
	        }
	    }
	    makeVirtualPVPArmyId(type, lv) {
	        return type * 10000 + lv;
	    }
	    getBasePvpArmy(type, lv) {
	        let vid = this.makeVirtualPVPArmyId(type, lv);
	        return this.mPvpArmys[vid];
	    }
	    getPvpArmyPower(type, lv) {
	        let basePvpArmy = this.getBasePvpArmy(type, lv);
	        return SoldierConfigManager.Instance.getSoldierPower(basePvpArmy.soldier_base_id, basePvpArmy.num);
	    }
	    getPvpArmyRewards(type, lv) {
	        return this.getBasePvpArmy(type, lv).reward;
	    }
	}

	class BaseMail {
	    constructor() {
	        this.desc = "";
	        this.type = 0;
	        this.event = "";
	        this.title = "";
	        this.content = "";
	    }
	}
	class MailConfigManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.mBaseMails = {};
	    }
	    static get Instance() {
	        return MailConfigManager.getInstance();
	    }
	    parse() {
	        let mgr = ConfigDataManager.Instance;
	        this.parseMailTemplate(mgr["mail_template"]);
	        delete mgr["mail_template"];
	    }
	    parseMailTemplate(jsonData) {
	        for (let templateId in jsonData) {
	            let intTemplateId = Number(templateId);
	            this.mBaseMails[intTemplateId] = jsonData[intTemplateId];
	        }
	    }
	    getMailTitle(templateId) {
	        return this.mBaseMails[templateId].title;
	    }
	    getMailContent(templateId) {
	        return this.mBaseMails[templateId].content;
	    }
	    getMailType(templateId) {
	        return this.mBaseMails[templateId].type;
	    }
	}

	class BaseGeneral {
	    constructor() {
	        this.quality = 1;
	        this.type = 0;
	        this.painting = "";
	        this.skill_id = 0;
	        this.init_force = 0;
	        this.init_def = 0;
	        this.init_soldiers = 0;
	        this.composition = 0;
	        this.attr_template_id = 0;
	    }
	}
	class GeneralConfigManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.mBaseGenerals = {};
	        this.mGeneralBaseIds = [];
	    }
	    static get Instance() {
	        return GeneralConfigManager.getInstance();
	    }
	    parse() {
	        let mgr = ConfigDataManager.Instance;
	        this.parseGeneral(mgr["general"]);
	        delete mgr["general"];
	    }
	    parseGeneral(jsonData) {
	        for (let id in jsonData) {
	            let generalBaseId = Number(id);
	            this.mBaseGenerals[generalBaseId] = jsonData[id];
	            this.mGeneralBaseIds.push(generalBaseId);
	        }
	    }
	    getBaseGeneral(generalBaseId) {
	        return this.mBaseGenerals[generalBaseId];
	    }
	    getAllGeneralBaseIds() {
	        return this.mGeneralBaseIds;
	    }
	}

	class ConfigDataManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.parsed = false;
	    }
	    static get Instance() {
	        return ConfigDataManager.getInstance();
	    }
	    getAssetLoadUrls() {
	        let result = [];
	        for (let i = 0; i < EDataConfig._endNull; i++) {
	            let url = this.getConfigUrl(EDataConfig[i]);
	            result.push({ url: url, type: Laya.Loader.JSON });
	        }
	        return result;
	    }
	    getConfigUrl(file) {
	        return "res/config/" + file + ".json";
	    }
	    parse() {
	        if (this.parsed) {
	            return;
	        }
	        this.parsed = true;
	        for (let i = 0; i < EDataConfig._endNull; i++) {
	            let file = EDataConfig[i];
	            let url = this.getConfigUrl(file);
	            this[file] = Laya.loader.getRes(url);
	        }
	        GoodsConfigManager.Instance.parse();
	        WorldConfigManager.Instance.parse();
	        MailConfigManager.Instance.parse();
	        GeneralConfigManager.Instance.parse();
	        SoldierConfigManager.Instance.parse();
	    }
	}
	var EDataConfig;
	(function (EDataConfig) {
	    EDataConfig[EDataConfig["goods"] = 0] = "goods";
	    EDataConfig[EDataConfig["city_type"] = 1] = "city_type";
	    EDataConfig[EDataConfig["pvp_army"] = 2] = "pvp_army";
	    EDataConfig[EDataConfig["mail_template"] = 3] = "mail_template";
	    EDataConfig[EDataConfig["general"] = 4] = "general";
	    EDataConfig[EDataConfig["soldier"] = 5] = "soldier";
	    EDataConfig[EDataConfig["_endNull"] = 6] = "_endNull";
	})(EDataConfig || (EDataConfig = {}));

	class BaseGoods {
	    constructor() {
	        this.icon = "";
	        this.quality = 1;
	        this.tab = 0;
	        this.type = 1;
	        this.subtype = 1;
	        this.des = "";
	        this.compose = 0;
	        this.use = 0;
	        this.lv = 1;
	        this.attr_template_id = 0;
	        this.effect = [];
	        this.exchange = 0;
	    }
	}
	class GoodsConfigManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.mBaseGoods = {};
	    }
	    static get Instance() {
	        return GoodsConfigManager.getInstance();
	    }
	    parse() {
	        let mgr = ConfigDataManager.Instance;
	        this.parseGoods(mgr["goods"]);
	        delete mgr["goods"];
	    }
	    parseGoods(jsonData) {
	        for (let goodsBaseId in jsonData) {
	            let intGoodsBaseId = Number(goodsBaseId);
	            this.mBaseGoods[intGoodsBaseId] = jsonData[goodsBaseId];
	        }
	    }
	    getBaseGoods(goodsBaseId) {
	        return this.mBaseGoods[goodsBaseId];
	    }
	    getGoodsName(goodsBaseId) {
	        return this.mBaseGoods[goodsBaseId].name;
	    }
	    getGoodsIcon(goodsBaseId) {
	        return this.mBaseGoods[goodsBaseId].icon;
	    }
	}

	class BagManager extends AbsGameModule {
	    constructor() {
	        super();
	        this.mGoods = {};
	        this.selectGoodsBaseId = 0;
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_bag_goods_list, this, this.s_get_bag_goods_list);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_update_goods_list, this, this.s_update_goods_list);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_use_goods, this, this.s_use_goods);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_del_goods, this, this.s_del_goods);
	    }
	    static get Instance() {
	        return BagManager.getInstance();
	    }
	    clear() {
	        this.mGoods = {};
	    }
	    getSortGoodsListByTab(tab = 0) {
	        let vos = [];
	        for (let k in this.mGoods) {
	            let baseGoods = GoodsConfigManager.Instance.getBaseGoods(k);
	            if (tab == baseGoods.tab - 1) {
	                vos.push(this.mGoods[k]);
	            }
	        }
	        vos.sort((a, b) => a.id - b.id);
	        return vos;
	    }
	    setIsNew(goodsBaseId, isNew = false) {
	        let vo = this.mGoods[goodsBaseId];
	        vo && (vo.is_new = isNew);
	    }
	    getGoodsNum(goodsBaseId) {
	        let vo = this.mGoods[goodsBaseId];
	        if (vo) {
	            return vo.num;
	        }
	        return 0;
	    }
	    isGoodsEnough(goodsBaseId, num = 1) {
	        return this.getGoodsNum(goodsBaseId) >= num;
	    }
	    c_get_bag_goods_list() {
	        NetSocketManager.Instance.SendMsg("c_get_bag_goods_list", {});
	    }
	    s_get_bag_goods_list(data) {
	        for (let item of data.goods_list) {
	            let vo = new VOGoods();
	            vo.init(item.goods_base_id, Number(item.num));
	            this.mGoods[item.goods_base_id] = vo;
	        }
	    }
	    s_update_goods_list(data) {
	        for (let item of data.goods_list) {
	            let vo = this.mGoods[item.goods_base_id];
	            let newNum = Number(item.num);
	            if (vo && item.num > 0) {
	                if (vo.num < newNum) {
	                    vo.is_new = true;
	                }
	                vo.num = newNum;
	            }
	            else if (vo) {
	                delete this.mGoods[item.goods_base_id];
	            }
	            else {
	                vo = new VOGoods();
	                vo.init(item.goods_base_id, newNum, true);
	                this.mGoods[item.goods_base_id] = vo;
	            }
	        }
	        EventManager.Instance.event(EEventType.BagItemListUpdate);
	    }
	    c_use_goods(goodsBaseId, useNum) {
	        let goodsNum = this.getGoodsNum(goodsBaseId);
	        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(goodsBaseId);
	        if (useNum <= 0 || goodsNum < useNum || baseGoods.use != 1) {
	            LogM.error("c_use_goods error:", useNum, goodsBaseId, goodsNum);
	            return;
	        }
	        NetSocketManager.Instance.SendMsg("c_use_goods", {
	            goods_base_id: goodsBaseId,
	            num: useNum,
	        });
	    }
	    s_use_goods(data) {
	        if (data.code != awesomepackage.e_code.ok) {
	            TipManager.Instance.showMsgById(data.code);
	            return;
	        }
	    }
	    c_del_goods(goodsBaseId, num) {
	        if (goodsBaseId > 0 && num > 0) {
	            NetSocketManager.Instance.SendMsg("c_del_goods", {
	                goods_base_id: goodsBaseId,
	                num: num,
	            });
	        }
	    }
	    s_del_goods(data) {
	        TipManager.Instance.showMsgByType(ETipType.Normal, "删除成功");
	    }
	}

	class UI_Bag extends ui.Panel.Bag.BagUI {
	    constructor() {
	        super();
	        this.mgr = BagManager.Instance;
	        this.tabs.selectHandler = new Laya.Handler(this, this.selectTab);
	        this.btn_use.on(Laya.Event.CLICK, this, this.onUseGoods);
	        this.btn_delete.on(Laya.Event.CLICK, this, this.onDeleteGoods);
	    }
	    onEnable() {
	        EventManager.Instance.on(EEventType.BagItemListClick, this, this.onBagItemListClick);
	        EventManager.Instance.on(EEventType.BagItemListUpdate, this, this.onBagItemListUpdate);
	        this.selectTab(0);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    onBagItemListUpdate() {
	        let tab = this.tabs.selectedIndex;
	        let goodsList = this.mgr.getSortGoodsListByTab(tab);
	        let prefabScript = this.list_bagList;
	        prefabScript.refreshList(goodsList);
	        this.mgr.selectGoodsBaseId = prefabScript.getSelectGoodsBaseId();
	        this.showGoodsDetail();
	    }
	    selectTab(tab) {
	        LogM.debug("UI_Bag selectTab:", tab);
	        this.tabs.selectedIndex = tab;
	        let goodsList = this.mgr.getSortGoodsListByTab(tab);
	        let prefabScript = this.list_bagList;
	        prefabScript.init(goodsList, true);
	        this.bottoms.visible = goodsList.length > 0;
	    }
	    onBagItemListClick(goodsBaseId) {
	        LogM.debug("UI_Bag clickItem:", goodsBaseId);
	        this.mgr.selectGoodsBaseId = goodsBaseId;
	        this.mgr.setIsNew(goodsBaseId, false);
	        this.showGoodsDetail();
	    }
	    showGoodsDetail() {
	        LogM.debug("showGoodsDetail:", this.mgr.selectGoodsBaseId);
	        let goodsBaseId = this.mgr.selectGoodsBaseId;
	        let goodsNum = this.mgr.getGoodsNum(goodsBaseId);
	        if (goodsNum == 0) {
	            return;
	        }
	        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(goodsBaseId);
	        let goodsName = baseGoods.name;
	        let goodsDesc = baseGoods.des;
	        this.label_itemName1.text = goodsName;
	        this.label_desc.text = goodsDesc;
	        this.btn_use.visible = false;
	        this.btn_compose.visible = false;
	        this.box_slider.visible = false;
	        if (baseGoods.use == 1) {
	            this.box_slider.visible = true;
	            this.btn_use.visible = true;
	            let box_slider = this.box_slider;
	            box_slider.init(goodsNum);
	        }
	        else if (baseGoods.compose == 1) {
	            this.btn_compose.visible = true;
	        }
	    }
	    onUseGoods() {
	        let boxSlider = this.box_slider;
	        let useNum = boxSlider.getValue();
	        this.mgr.c_use_goods(this.mgr.selectGoodsBaseId, useNum);
	    }
	    onDeleteGoods() {
	        if (this.mgr.selectGoodsBaseId > 0) {
	            SceneManager.Instance.openScene(ESceneName.UI_BagDel);
	        }
	    }
	}

	class UI_Close extends Laya.Script {
	    onClick() {
	        this.onClickCallback();
	    }
	    onClickCallback() {
	        if (this.owner.parent instanceof Laya.Dialog) {
	            this.owner.parent.close();
	        }
	        else if (this.owner.parent.parent instanceof Laya.Dialog) {
	            this.owner.parent.parent.close();
	        }
	    }
	}

	class UI_BagItemList extends Laya.List {
	    onEnable() {
	        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
	    }
	    onDisable() {
	        this.array = [];
	    }
	    init(_datas, canClick = false) {
	        this.vScrollBarSkin = "";
	        this.array = _datas;
	        if (canClick) {
	            this.selectHandler = new Laya.Handler(this, this.onSelectHandler);
	            if (this.array.length > 0) {
	                this.selectedIndex = 0;
	            }
	        }
	    }
	    refreshList(_datas) {
	        this.array = _datas;
	    }
	    onRenderHandler(cell, index) {
	        if (index >= this.array.length) {
	            return;
	        }
	        let imgItemIcon = cell.getChildByName("img_itemIcon");
	        let imgRedPoint = cell.getChildByName("img_redpoint");
	        let labelName = cell.getChildByName("label_itemName");
	        let labelNum = cell.getChildByName("label_itemNum");
	        let labelLv = cell.getChildByName("label_itemLv");
	        let cellData = this.array[index];
	        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(cellData.id);
	        imgItemIcon.skin = baseGoods.icon;
	        imgRedPoint.visible = cellData.is_new == true;
	        labelName.text = baseGoods.name;
	        labelNum.text = "X" + cellData.num;
	        if (baseGoods.type == EGoodsType.stren) {
	            labelLv.text = "lv." + baseGoods.lv;
	        }
	        else {
	            labelLv.text = "";
	        }
	    }
	    onSelectHandler(index) {
	        let cell = this.selection;
	        let cellData = this.array[index];
	        cellData.is_new = false;
	        let imgRedpoint = cell.getChildByName("img_redpoint");
	        imgRedpoint.visible = false;
	        EventManager.Instance.event(EEventType.BagItemListClick, cellData.id);
	    }
	    getSelectGoodsBaseId() {
	        let select = this.selectedItem;
	        if (select) {
	            return select.id;
	        }
	        return -1;
	    }
	}

	class UI_Slider extends Laya.Box {
	    constructor() {
	        super(...arguments);
	        this.btnSliderAdd = null;
	        this.btnSliderSub = null;
	        this.labelSliderNum = null;
	        this.slider = null;
	    }
	    onEnable() {
	        this.btnSliderAdd = this.getChildByName("btn_sliderAdd");
	        this.btnSliderSub = this.getChildByName("btn_sliderSub");
	        this.labelSliderNum = this.getChildByName("label_sliderNum");
	        this.slider = this.getChildByName("slider");
	        this.btnSliderAdd.on(Laya.Event.CLICK, this, this.onBtnSliderAddClick);
	        this.btnSliderSub.on(Laya.Event.CLICK, this, this.onBtnSliderSubClick);
	    }
	    init(maxNum, minNum = 1, step = 1) {
	        this.slider.max = maxNum;
	        this.slider.min = minNum;
	        this.slider.tick = step;
	        this.slider.value = maxNum;
	        this.labelSliderNum.text = "" + maxNum;
	        this.slider.changeHandler = new Laya.Handler(this, this.onChangeHandler);
	    }
	    onChangeHandler() {
	        this.labelSliderNum.text = "" + this.slider.value;
	    }
	    onBtnSliderAddClick() {
	        let curNum = this.slider.value;
	        if (curNum >= this.slider.max) {
	            return;
	        }
	        let newNum = curNum + this.slider.tick;
	        newNum = newNum >= this.slider.max ? this.slider.max : newNum;
	        this.slider.value = newNum;
	    }
	    onBtnSliderSubClick() {
	        let curNum = this.slider.value;
	        if (curNum <= this.slider.min) {
	            return;
	        }
	        let newNum = curNum - this.slider.tick;
	        newNum = newNum <= this.slider.min ? this.slider.min : newNum;
	        this.slider.value = newNum;
	    }
	    getValue() {
	        return this.slider.value;
	    }
	}

	class UI_BagDel extends ui.Panel.Bag.BagDelUI {
	    constructor() {
	        super();
	        this.mgr = BagManager.Instance;
	        this.btn_del.on(Laya.Event.CLICK, this, this.onDel);
	    }
	    onEnable() {
	        let goodsBaseId = this.mgr.selectGoodsBaseId;
	        let num = this.mgr.getGoodsNum(goodsBaseId);
	        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(goodsBaseId);
	        this.label_itemDesc.text = baseGoods.des;
	        let boxSlider = this.box_slider;
	        boxSlider.init(num);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    onDel() {
	        let goodsBaseId = this.mgr.selectGoodsBaseId;
	        let boxSlider = this.box_slider;
	        let num = boxSlider.getValue();
	        this.mgr.c_del_goods(goodsBaseId, num);
	        this.close();
	    }
	}

	class UI_GoodsDesc extends ui.Panel.Bag.GoodsDescUI {
	    constructor() {
	        super();
	    }
	    onEnable() {
	        EventManager.Instance.on(ESceneName.UI_GoodsDesc, this, this.init);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    init(goodsBaseId) {
	        if (!goodsBaseId) {
	            this.close();
	        }
	        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(goodsBaseId);
	        this.label_title.text = baseGoods.name;
	        this.label_desc.text = baseGoods.des;
	        this.img_icon.skin = baseGoods.icon;
	    }
	}

	class Util {
	    static max(a, b) {
	        if (a >= b)
	            return a;
	        return b;
	    }
	    static min(a, b) {
	        if (a <= b)
	            return a;
	        return b;
	    }
	    static checkPointEqual(a, b) {
	        return a.x === b.x && a.y === b.y;
	    }
	    static getDistance(p1, p2) {
	        let dx = p1.x - p2.x;
	        let dy = p1.y - p2.y;
	        return Math.sqrt(dx * dx + dy * dy);
	    }
	    static getAngle(point1, point2) {
	        return Math.atan2(point2.y - point1.y, point2.x - point1.x) * (180 / Math.PI);
	    }
	    static isChinese(str) {
	        let reg = /^.*[\u4E00-\u9FA5]+.*$/;
	        return reg.test(str);
	    }
	    static string2Byte(str) {
	        let by = new Laya.Byte();
	        by.writeUTFBytes(str);
	        return new Uint8Array(by.buffer);
	    }
	    static byteToString(array) {
	        if (!array || array.length === 0) {
	            return "";
	        }
	        var out, i, len, c;
	        var char2, char3;
	        out = "";
	        len = array.length;
	        i = 0;
	        while (i < len) {
	            c = array[i++];
	            switch (c >> 4) {
	                case 0:
	                case 1:
	                case 2:
	                case 3:
	                case 4:
	                case 5:
	                case 6:
	                case 7:
	                    out += String.fromCharCode(c);
	                    break;
	                case 12:
	                case 13:
	                    char2 = array[i++];
	                    out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
	                    break;
	                case 14:
	                    char2 = array[i++];
	                    char3 = array[i++];
	                    out += String.fromCharCode(((c & 0x0F) << 12) |
	                        ((char2 & 0x3F) << 6) |
	                        ((char3 & 0x3F) << 0));
	                    break;
	            }
	        }
	        return out;
	    }
	    static getLength(str) {
	        let char;
	        let length = 0;
	        for (let i = 0; i < str.length; i++) {
	            char = str[i];
	            if (this.isChinese(char)) {
	                length += 2;
	            }
	            else {
	                length++;
	            }
	        }
	        return length;
	    }
	    static cutOutString(str, cutOutNum) {
	        if (this.getLength(str) <= cutOutNum) {
	            return str;
	        }
	        let newStr = "";
	        let length = 0;
	        let char;
	        for (let i = 0; i < str.length; i++) {
	            char = str[i];
	            if (this.isChinese(char)) {
	                length += 2;
	            }
	            else {
	                length++;
	            }
	            newStr += char;
	            if (length >= cutOutNum) {
	                break;
	            }
	        }
	        return newStr;
	    }
	    static random(min, max) {
	        if (min >= max) {
	            console.error("Random  区间不合法，min必须小于max");
	            return 0;
	        }
	        let num = max - min;
	        return Math.random() * num + min;
	    }
	    static randomInt(min, max) {
	        if (min > max) {
	            console.error("Random  区间不合法，min必须小于max");
	            return 0;
	        }
	        else if (min === max) {
	            return min;
	        }
	        return this.random(min, max + 1) | 0;
	    }
	    static randomArray(arr) {
	        const index = this.random(0, arr.length) | 0;
	        return arr[index];
	    }
	    static formatDiffTime(difftime) {
	        let str = "";
	        let h = difftime / 3600;
	        h = parseInt(h + "");
	        let m = (difftime - h * 3600) / 60;
	        m = parseInt(m + "");
	        let s = difftime - h * 3600 - m * 60;
	        s = parseInt(s + "");
	        if (h > 9) {
	            str += h + ":";
	        }
	        else {
	            str += "0" + h + ":";
	        }
	        if (m > 9) {
	            str += m + ":";
	        }
	        else {
	            str += "0" + m + ":";
	        }
	        if (s > 9) {
	            str += s + "";
	        }
	        else {
	            str += "0" + s;
	        }
	        return str;
	    }
	    static secondToDate(timestamp) {
	        const d = new Date(timestamp * 1000);
	        let year = d.getFullYear();
	        let month = (d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1);
	        var day = (d.getDate() < 10 ? '0' + (d.getDate()) : d.getDate());
	        return year + "年" + month + "月" + d.getDate() + "日";
	    }
	    static secondToDateTime(timestamp, withYear = true) {
	        const d = new Date(timestamp * 1000);
	        let year = d.getFullYear();
	        let month = (d.getMonth() + 1) < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1);
	        var day = (d.getDate() < 10 ? '0' + (d.getDate()) : d.getDate());
	        var hour = (d.getHours() < 10 ? '0' + (d.getHours()) : d.getHours());
	        var min = (d.getMinutes() < 10 ? '0' + (d.getMinutes()) : d.getMinutes());
	        var sec = (d.getSeconds() < 10 ? '0' + (d.getSeconds()) : d.getSeconds());
	        let date = withYear ? year + "年" : "" + month + "月" + d.getDate() + "日";
	        return date + " " + hour + ":" + min + ":" + sec;
	    }
	    static intersection(a, b) {
	        let result = [];
	        for (let e of a) {
	            if (b.indexOf(e) >= 0) {
	                result.push(e);
	            }
	        }
	        return result;
	    }
	    static union(...b) {
	        let result = [];
	        for (let arr of b) {
	            for (let e of arr) {
	                if (result.indexOf(e) < 0) {
	                    result.push(e);
	                }
	            }
	        }
	        return result;
	    }
	    static difference(a, b) {
	        let result = [];
	        for (let e of a) {
	            if (b.indexOf(e) < 0) {
	                result.push(e);
	            }
	        }
	        return result;
	    }
	}

	class VOPlayer {
	    constructor() {
	        this.avatar_url = "";
	        this.gold = 0;
	        this.diamond = 0;
	        this.x = 0;
	        this.y = 0;
	        this.lv = 1;
	        this.exp = 0;
	        this.lv_mon_refresh = 1;
	    }
	    init(data) {
	        this.id = data.player_id;
	        this.nickname = Util.byteToString(data.nickname);
	        this.avatar_url = "";
	        this.gold = Number(data.gold);
	        this.diamond = data.diamond;
	        this.x = data.x;
	        this.y = data.y;
	        this.lv = data.lv;
	        this.exp = Number(data.exp);
	        this.lv_mon_refresh = data.lv_mon_refresh;
	    }
	}

	class PlayerManager extends AbsGameModule {
	    constructor() {
	        super();
	        this.mainPlayer = null;
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_player_info, this, this.s_get_player_info);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_refresh_player_attr, this, this.s_refresh_player_attr);
	    }
	    static get Instance() {
	        return PlayerManager.getInstance();
	    }
	    c_get_player_info() {
	        NetSocketManager.Instance.SendMsg("c_get_player_info", {});
	    }
	    s_get_player_info(data) {
	        let mPlayer = new VOPlayer();
	        mPlayer.init(data);
	        this.mainPlayer = mPlayer;
	    }
	    s_refresh_player_attr(data) {
	        if (!this.mainPlayer) {
	            return;
	        }
	        let mRefresh = {};
	        let value = 0;
	        for (let item of data.attrs) {
	            value = Number(item.attr_value);
	            let et = this.refreshPlayerAttrItem(item.attr_key, value);
	            if (et) {
	                mRefresh[et] = 1;
	            }
	        }
	        for (let et in mRefresh) {
	            EventManager.Instance.event(et);
	        }
	    }
	    refreshPlayerAttrItem(key, value) {
	        let attrEventType = null;
	        switch (key) {
	            case awesomepackage.e_playe_attr_key.gold:
	                this.mainPlayer.gold = value;
	                attrEventType = EEventType.PlayerResUpdate;
	                break;
	            case awesomepackage.e_playe_attr_key.diamond:
	                this.mainPlayer.diamond = value;
	                attrEventType = EEventType.PlayerResUpdate;
	                break;
	            case awesomepackage.e_playe_attr_key.x:
	                this.mainPlayer.x = value;
	                attrEventType = EEventType.PlayerPosUpdate;
	                break;
	            case awesomepackage.e_playe_attr_key.y:
	                this.mainPlayer.y = value;
	                attrEventType = EEventType.PlayerPosUpdate;
	                break;
	            default:
	                LogM.error("refreshPlayerAttrItem not deal", key, value);
	        }
	        return attrEventType;
	    }
	    c_gm(content) {
	        NetSocketManager.Instance.SendMsg("c_gm", { content: content });
	    }
	}

	var EBattleSide;
	(function (EBattleSide) {
	    EBattleSide[EBattleSide["Left"] = 1] = "Left";
	    EBattleSide[EBattleSide["Right"] = 2] = "Right";
	})(EBattleSide || (EBattleSide = {}));
	var EBattleResult;
	(function (EBattleResult) {
	    EBattleResult[EBattleResult["Win"] = 1] = "Win";
	    EBattleResult[EBattleResult["Fail"] = 2] = "Fail";
	})(EBattleResult || (EBattleResult = {}));
	const leftBaseX = 545;
	const rightBaseX = 276;
	const rightBaseY = 307;
	const BattleSidePosMap = {
	    "pos_1_1": { x: 160, y: leftBaseX },
	    "pos_1_2": { x: 240, y: leftBaseX + 60 },
	    "pos_1_3": { x: 320, y: leftBaseX + 120 },
	    "pos_1_4": { x: 80, y: leftBaseX + 60 },
	    "pos_1_5": { x: 160, y: leftBaseX + 120 },
	    "pos_1_6": { x: 240, y: leftBaseX + 180 },
	    "pos_1_7": { x: 0, y: leftBaseX + 120 },
	    "pos_1_8": { x: 80, y: leftBaseX + 180 },
	    "pos_1_9": { x: 160, y: leftBaseX + 240 },
	    "pos_2_1": { x: rightBaseX + 0, y: rightBaseY + 120 },
	    "pos_2_2": { x: rightBaseX + 80, y: rightBaseY + 180 },
	    "pos_2_3": { x: rightBaseX + 160, y: rightBaseY + 240 },
	    "pos_2_4": { x: rightBaseX + 80, y: rightBaseY + 60 },
	    "pos_2_5": { x: rightBaseX + 160, y: rightBaseY + 120 },
	    "pos_2_6": { x: rightBaseX + 240, y: rightBaseY + 180 },
	    "pos_2_7": { x: rightBaseX + 160, y: rightBaseY + 0 },
	    "pos_2_8": { x: rightBaseX + 240, y: rightBaseY + 60 },
	    "pos_2_9": { x: rightBaseX + 320, y: rightBaseY + 120 },
	};
	const BattleAniDuration = 100;
	const BattleAniDelayTime = 1000;

	class UI_Battle extends ui.Panel.Battle.BattleUI {
	    constructor() {
	        super();
	        this.mPlayMulti = 2;
	        this.mBoxSoldierPrefab = null;
	        this.mBattleHpPrefab = null;
	        this.mData = null;
	        this.mRewardList = [];
	        this.mResult = 1;
	        this.mLastAttArmyId = 0;
	        this.mLastDefArmyId = 0;
	        this.mArmys = {};
	        this.mArmySides = {};
	        this.mCurrentRound = -1;
	        this.mShotIndex = 0;
	        this.stopped = false;
	        this.over = false;
	        this.loginNoTip = false;
	        this.btn_back.on(Laya.Event.CLICK, this, this.onClose);
	        this.btn_skip.on(Laya.Event.CLICK, this, this.onSkip);
	        this.btn_speedUp.on(Laya.Event.CLICK, this, this.onSpeedUp);
	        this.btn_resultBack.on(Laya.Event.CLICK, this, () => {
	            this.close();
	        });
	    }
	    onEnable() {
	        EventManager.Instance.on(ESceneName.UI_Battle, this, this.init);
	    }
	    onDisable() {
	        delete this.mData;
	        this.stopped = true;
	        this.over = true;
	        Laya.timer.clearAll(this);
	        EventManager.Instance.offAllCaller(this);
	    }
	    reset() {
	        this.mData = null;
	        this.mRewardList = [];
	        this.mLastAttArmyId = 0;
	        this.mLastDefArmyId = 0;
	        this.stopped = false;
	        this.over = false;
	        this.box_objs.destroyChildren();
	    }
	    onClose() {
	        this.stopped = true;
	        Laya.timer.clearAll(this);
	        if (this.loginNoTip) {
	            this.close();
	            return;
	        }
	        SceneManager.Instance.openAlert("是否退出战斗", "提示", Laya.Handler.create(this, this.onCloseCallback), 1, true, Laya.Handler.create(this, () => {
	            if (!this.over) {
	                this.stopped = false;
	                this.doShot();
	            }
	        }));
	    }
	    onCloseCallback(loginNoTip) {
	        this.loginNoTip = loginNoTip;
	        this.close();
	    }
	    onSkip() {
	        this.stopped = true;
	        this.doBattleEnd();
	    }
	    onSpeedUp() {
	        this.mPlayMulti++;
	        if (this.mPlayMulti > 3) {
	            this.mPlayMulti = 1;
	        }
	        this.btn_speedUp.label = "X" + this.mPlayMulti;
	    }
	    init(_data) {
	        this.reset();
	        this.mData = awesomepackage["p_battle_data"].decode(_data.battle_data);
	        this.mRewardList = _data.reward_list;
	        if (!this.mBoxSoldierPrefab) {
	            this.mBoxSoldierPrefab = Laya.loader.getRes(EPrefabPath.BoxSoldier);
	        }
	        if (!this.mBattleHpPrefab) {
	            this.mBattleHpPrefab = Laya.loader.getRes(EPrefabPath.BattleHp);
	        }
	        this.initBattleData();
	        this.initUI();
	        this.doRounds();
	    }
	    initBattleData() {
	        let left = this.mData.attackers;
	        let right = this.mData.defenders;
	        this.mResult = this.mData.result;
	        for (let a of this.mData.defenders) {
	            if (a.player_id == PlayerManager.Instance.mainPlayer.id) {
	                left = this.mData.defenders;
	                right = this.mData.attackers;
	                this.mResult = this.mData.result == 1 ? EBattleResult.Fail : EBattleResult.Win;
	                break;
	            }
	        }
	        this.initBattleArmy(left, EBattleSide.Left);
	        this.initBattleArmy(right, EBattleSide.Right);
	    }
	    initBattleArmy(armys, side) {
	        for (let army of armys) {
	            this.mArmySides[army.army_id] = side;
	            this.mArmys[army.army_id] = army;
	        }
	    }
	    initUI() {
	        this.btn_speedUp.label = "X" + this.mPlayMulti;
	        this.box_result.visible = false;
	    }
	    getArmySide(armyId) {
	        return this.mArmySides[armyId];
	    }
	    getBattleArmy(armyId) {
	        return this.mArmys[armyId];
	    }
	    getObjItemName(objId) {
	        return "box_item_" + objId;
	    }
	    doBattleEnd() {
	        this.over = true;
	        this.box_result.visible = true;
	        if (this.mResult == EBattleResult.Win) {
	            this.img_resultWin.visible = true;
	            this.img_resultFail.visible = false;
	            this.img_resultTitle.skin = "image/battle/ui_battle_win_jiangli_font.png";
	            this.box_resultFail.visible = false;
	            this.list_winRewards.visible = true;
	            let list_winRewards = this.list_winRewards;
	            list_winRewards.init(this.mRewardList);
	        }
	        else {
	            this.img_resultWin.visible = false;
	            this.img_resultFail.visible = true;
	            this.img_resultTitle.skin = "image/battle/ui_battle_zjzl_font.png";
	            this.box_resultFail.visible = true;
	            this.list_winRewards.visible = false;
	        }
	    }
	    doRounds() {
	        if (this.mData.battle_round_list.length == 0) {
	            this.doBattleEnd();
	            return;
	        }
	        this.mCurrentRound = -1;
	        this.doRound();
	    }
	    doRound() {
	        if (this.stopped)
	            return;
	        this.mCurrentRound++;
	        if (this.mCurrentRound >= this.mData.battle_round_list.length) {
	            this.doBattleEnd();
	            return;
	        }
	        let round = this.mData.battle_round_list[this.mCurrentRound];
	        if (this.mCurrentRound == 0 || this.mLastAttArmyId != round.a_army_id) {
	            this.doRoundArmyInitUI(round.a_army_id);
	        }
	        if (this.mCurrentRound == 0 || this.mLastDefArmyId != round.d_army_id) {
	            this.doRoundArmyInitUI(round.d_army_id);
	        }
	        this.mLastAttArmyId = round.a_army_id;
	        this.mLastDefArmyId = round.d_army_id;
	        this.mShotIndex = -1;
	        this.doShot();
	    }
	    doShot() {
	        if (this.stopped)
	            return;
	        this.mShotIndex++;
	        let round = this.mData.battle_round_list[this.mCurrentRound];
	        if (this.mShotIndex >= round.shot_list.length) {
	            this.doRound();
	            return;
	        }
	        let shot = round.shot_list[this.mShotIndex];
	        let prefabItemName = this.getObjItemName(shot.shot_obj_id);
	        let box_soldier = this.box_objs.getChildByName(prefabItemName);
	        if (!box_soldier) {
	            LogM.error("doShot error, objId not found");
	            return;
	        }
	        box_soldier.shot(this.mPlayMulti);
	        for (let hurtObj of shot.hurt_list) {
	            prefabItemName = this.getObjItemName(hurtObj.be_obj_id);
	            let box_hurt_soldier = this.box_objs.getChildByName(prefabItemName);
	            box_hurt_soldier.hurt(hurtObj, shot.shot_obj_id, this.mPlayMulti);
	        }
	        Laya.timer.once(BattleAniDelayTime / this.mPlayMulti, this, () => {
	            this.doShot();
	        });
	    }
	    doRoundArmyInitUI(armyId) {
	        let army = this.getBattleArmy(armyId);
	        let side = this.getArmySide(armyId);
	        for (let soldier of army.soldier_list) {
	            let prefab = Laya.Pool.getItemByCreateFun(EPrefabPath.BoxSoldier, this.mBoxSoldierPrefab.create, this.mBoxSoldierPrefab);
	            let prefabItemName = this.getObjItemName(soldier.obj_id);
	            prefab.name = prefabItemName;
	            this.box_objs.addChild(prefab);
	            prefab.init(soldier, side, this.mBattleHpPrefab);
	        }
	        let labelName = this.label_leftName;
	        let listDeploy = this.list_leftDeploys;
	        if (side == EBattleSide.Right) {
	            labelName = this.label_rightName;
	            listDeploy = this.list_rightDeploys;
	        }
	        labelName.text = Util.byteToString(army.nickname);
	        let generalList = [];
	        for (let generalInfo of army.general_list) {
	            generalList.push({
	                id: generalInfo.general_base_id,
	                type: 1,
	                lv: generalInfo.lv,
	                star: generalInfo.star,
	            });
	        }
	        listDeploy.init(generalList);
	    }
	}

	class UI_DeployList extends Laya.List {
	    constructor() {
	        super();
	        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
	    }
	    onEnable() {
	    }
	    onDisable() {
	        this.array = [];
	    }
	    init(datas) {
	        this.array = datas;
	    }
	    onRenderHandler(cell, index) {
	        let cellData = this.array[index];
	        let img_itemBg = cell.getChildByName("img_itemBg");
	        let img_itemIcon = cell.getChildByName("img_itemIcon");
	        let box_itemStars = cell.getChildByName("box_itemStars");
	        let label_itemLv = cell.getChildByName("label_itemLv");
	        label_itemLv.visible = cellData.type == 1;
	        box_itemStars.visible = cellData.type == 1;
	        if (cellData.type == 1) {
	            let baseGeneral = GeneralConfigManager.Instance.getBaseGeneral(cellData.id);
	            img_itemIcon.skin = baseGeneral.icon;
	        }
	        else {
	            let baseSoldier = SoldierConfigManager.Instance.getBaseSoldier(cellData.id);
	            img_itemIcon.skin = baseSoldier.icon;
	        }
	    }
	}

	class UI_RewardList extends Laya.List {
	    constructor() {
	        super();
	        this.canClick = false;
	        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
	        this.mouseHandler = new Laya.Handler(this, this.onMouseHandler);
	    }
	    onEnable() {
	    }
	    onDisable() {
	        this.array = [];
	    }
	    init(_datas, canClick = false) {
	        this.vScrollBarSkin = "";
	        let datas = [];
	        for (let item of _datas) {
	            let goodsBaseId = item.goods_base_id;
	            if (goodsBaseId == 0) {
	                goodsBaseId = 1025;
	                if (item.reward_type == awesomepackage.e_res_type.diamond) {
	                    goodsBaseId = 1026;
	                }
	            }
	            datas.push({ goods_base_id: goodsBaseId, num: item.num });
	        }
	        this.array = datas;
	        this.canClick = canClick;
	    }
	    onRenderHandler(cell, index) {
	        if (index >= this.array.length) {
	            return;
	        }
	        let imgItemIcon = cell.getChildByName("img_itemIcon");
	        let labelName = cell.getChildByName("label_itemName");
	        let labelNum = cell.getChildByName("label_itemNum");
	        let labelLv = cell.getChildByName("label_itemLv");
	        let cellData = this.array[index];
	        let baseGoods = GoodsConfigManager.Instance.getBaseGoods(cellData.goods_base_id);
	        imgItemIcon.skin = baseGoods.icon;
	        labelName && (labelName.text = baseGoods.name);
	        labelNum && (labelNum.text = "X" + cellData.num);
	        if (baseGoods.type == EGoodsType.stren) {
	            labelLv && (labelLv.text = "lv." + baseGoods.lv);
	        }
	        else {
	            labelLv && (labelLv.text = "");
	        }
	    }
	    onMouseHandler(e, index) {
	        if (!this.canClick)
	            return;
	        let cellData = this.array[index];
	        if (e.type == Laya.Event.CLICK) {
	            SceneManager.Instance.openScene(ESceneName.UI_GoodsDesc, [cellData.goods_base_id]);
	        }
	    }
	}

	class VOGeneral {
	    constructor() {
	        this.lv = 1;
	        this.exp = 0;
	        this.star = 1;
	        this.state = 0;
	    }
	    init(general) {
	        this.general_base_id = general.general_base_id;
	        this.lv = general.lv;
	        this.exp = general.exp;
	        this.star = general.star;
	        this.state = general.state;
	    }
	}

	class GeneralManager extends AbsGameModule {
	    constructor() {
	        super();
	        this.mGenerals = {};
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_general_list, this, this.s_get_general_list);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_add_general, this, this.s_add_general);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_refresh_general_attr, this, this.s_refresh_general_attr);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_general_add_exp, this, this.s_general_add_exp);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_general_up_star, this, this.s_general_up_star);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_general_compose, this, this.s_general_compose);
	    }
	    static get Instance() {
	        return GeneralManager.getInstance();
	    }
	    getGeneralIdList() {
	        let arr = [1, 2, 3, 4, 5];
	        for (let k in this.mGenerals) {
	            arr.push(Number(k));
	        }
	        return arr;
	    }
	    getGeneral(generalBaseId) {
	        return this.mGenerals[generalBaseId];
	    }
	    c_get_general_list() {
	        NetSocketManager.Instance.SendMsg("c_get_general_list", {});
	    }
	    s_get_general_list(data) {
	        for (let general of data.general_list) {
	            let vo = new VOGeneral();
	            vo.init(general);
	            this.mGenerals[vo.general_base_id] = vo;
	        }
	    }
	    s_add_general(data) {
	        let vo = new VOGeneral();
	        vo.init(data.general_info);
	        this.mGenerals[vo.general_base_id] = vo;
	    }
	    s_refresh_general_attr(data) {
	        let vo = this.mGenerals[data.general_base_id];
	        if (!vo)
	            return;
	        for (let generalAttr of data.attrs) {
	            switch (generalAttr.attr_key) {
	                case awesomepackage.e_general_attr_key.lv:
	                    vo.lv = generalAttr.attr_value;
	                    break;
	                case awesomepackage.e_general_attr_key.exp:
	                    vo.exp = generalAttr.attr_value;
	                    break;
	                case awesomepackage.e_general_attr_key.star:
	                    vo.star = generalAttr.attr_value;
	                    break;
	                case awesomepackage.e_general_attr_key.state:
	                    vo.state = generalAttr.attr_value;
	                    break;
	            }
	        }
	    }
	    c_general_add_exp(generalBaseId, goodsList) {
	        NetSocketManager.Instance.SendMsg("c_general_add_exp", {
	            general_base_id: generalBaseId,
	            goods_list: goodsList,
	        });
	    }
	    s_general_add_exp(data) {
	    }
	    c_general_up_star(generalBaseId) {
	        NetSocketManager.Instance.SendMsg("c_general_up_star", { general_base_id: generalBaseId });
	    }
	    s_general_up_star(data) {
	    }
	    c_general_compose(generalBaseId) {
	        NetSocketManager.Instance.SendMsg("c_general_compose", { general_base_id: generalBaseId });
	    }
	    s_general_compose(data) {
	    }
	}

	class UI_General extends ui.Panel.General.GeneralUI {
	    constructor() {
	        super();
	        this.mgr = GeneralManager.Instance;
	    }
	    onEnable() {
	        this.init();
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    init() {
	        let recruitIdList = this.mgr.getGeneralIdList();
	        let recruitInitY = 153;
	        let recruitHeight = 220 * Math.ceil(recruitIdList.length / 4);
	        let list_recruit = this.list_recruit;
	        list_recruit.height = recruitHeight;
	        list_recruit.init(recruitIdList);
	        let allIds = GeneralConfigManager.Instance.getAllGeneralBaseIds();
	        let noRecruitIdList = Util.difference(allIds, recruitIdList);
	        let noRecruitIdHeight = 220 * Math.ceil(noRecruitIdList.length / 4);
	        let list_noRecruit = this.list_noRecruit;
	        list_noRecruit.height = noRecruitIdHeight;
	        list_noRecruit.y = recruitInitY + recruitHeight + 80;
	        list_noRecruit.init(noRecruitIdList);
	        this.img_NoRecruit.y = recruitInitY + recruitHeight;
	    }
	}

	class UI_GeneralList extends Laya.List {
	    constructor() {
	        super();
	        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
	    }
	    onEnable() {
	    }
	    onDisable() {
	        this.array = [];
	    }
	    init(datas) {
	        this.vScrollBarSkin = "";
	        this.array = datas;
	    }
	    onRenderHandler(cell, index) {
	        let img_itemIcon = cell.getChildByName("img_itemIcon");
	        let img_itemOp = cell.getChildByName("img_itemOp");
	        let label_itemNum = cell.getChildByName("label_itemNum");
	        let label_itemName = cell.getChildByName("label_itemName");
	        let box_stars = cell.getChildByName("box_stars");
	        let generalBaseId = this.array[index];
	        let baseGeneral = GeneralConfigManager.Instance.getBaseGeneral(generalBaseId);
	        let vo = GeneralManager.Instance.getGeneral(generalBaseId);
	        img_itemIcon.skin = baseGeneral.icon;
	        img_itemOp.visible = false;
	        label_itemName.text = baseGeneral.name;
	        let star = 0;
	        if (vo) {
	            label_itemNum.text = "LV." + vo.lv;
	            star = vo.star;
	        }
	        else {
	            label_itemNum.text = "0/10";
	        }
	        for (let i = 1; i <= 5; i++) {
	            if (star >= i) {
	                box_stars.getChildByName("img_star" + i).skin = "image/comp/ui_star_ light.png";
	            }
	            else {
	                box_stars.getChildByName("img_star" + i).skin = "image/comp/ui_star_gray.png";
	            }
	        }
	    }
	}

	class UI_LoginLoading extends ui.Panel.Loading.LoginLoadingUI {
	    constructor() {
	        super();
	    }
	    onEnable() {
	        EventManager.Instance.on(EEventType.LoginLoadingProgress, this, this.onLoginLoadingProgress);
	        EventManager.Instance.on(EEventType.CloseLoginLoadingView, this, this.onCloseLoginLoadingView);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    onLoginLoadingProgress(progress) {
	        LogM.debug("onLoginLoadingProgress", progress);
	        const width = 666 * progress;
	        this.img_progress.width = width;
	    }
	    onCloseLoginLoadingView() {
	        this.close();
	    }
	}

	class AbsStateGame {
	    constructor() {
	        this.initedOnlyOnce = false;
	        this.mods = [];
	    }
	    enter() {
	        if (!this.initedOnlyOnce) {
	            this.initedOnlyOnce = true;
	            this.initOnlyOnce();
	        }
	    }
	    exit() {
	    }
	    initOnlyOnce() {
	    }
	    enterModules() {
	        for (let mod of this.mods) {
	            mod.scene_enter();
	        }
	    }
	    exitModules() {
	        for (let mod of this.mods) {
	            mod.scene_exit();
	        }
	    }
	}

	class ModRefresh {
	}
	class RefreshManager {
	    constructor() {
	        this.refresher = [];
	        Laya.timer.frameLoop(1, this, this.refresh);
	    }
	    static get Instance() {
	        if (!this._instance) {
	            this._instance = new RefreshManager();
	        }
	        return this._instance;
	    }
	    add(refresh, refreshFrameInterval = 1) {
	        this.remove(refresh);
	        let modRefresh = {
	            refresh: refresh,
	            lastExecFrame: Laya.timer.currFrame,
	            refreshFrameInterval: refreshFrameInterval,
	            delta: 0,
	        };
	        this.refresher.push(modRefresh);
	    }
	    remove(refresh) {
	        for (let index in this.refresher) {
	            if (this.refresher[index].refresh === refresh) {
	                this.refresher.splice(Number(index), 1);
	                break;
	            }
	        }
	    }
	    refresh() {
	        let curFrame = Laya.timer.currFrame;
	        let delta = Laya.timer.delta;
	        for (let m of this.refresher) {
	            m.delta += delta;
	            if (curFrame - m.lastExecFrame >= m.refreshFrameInterval) {
	                m.refresh.refresh(m.delta);
	                m.lastExecFrame = curFrame;
	                m.delta = 0;
	            }
	        }
	    }
	}
	RefreshManager._instance = null;

	class StateGameInit extends AbsStateGame {
	    enter() {
	        super.enter();
	        if (Common.OpenTestAccountPanel) {
	        }
	        else {
	            GameStateManager.Instance.switchState(TypeStateGame.Login);
	        }
	    }
	    initOnlyOnce() {
	        RefreshManager.Instance;
	        TimeManager.Instance;
	        RefreshManager.Instance.add(TimeManager.Instance);
	    }
	    exit() {
	        super.exit();
	    }
	}

	class LoginManager extends AbsGameModule {
	    constructor() {
	        super();
	        this.resLoaded = false;
	        this.netConnectSuccess = false;
	        this.serverIp = "192.168.0.101";
	        this.serverPort = 7001;
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_player_login, this, this.s_player_login);
	        EventManager.Instance.on(EEventType.OpenSocketSuccess, this, this.onOpenSocketSuccess);
	    }
	    static get Instance() {
	        return LoginManager.getInstance();
	    }
	    get serverIpPort() {
	        if (this.serverIp) {
	            return this.serverIp + ":" + this.serverPort;
	        }
	        return "";
	    }
	    scene_exit() {
	        this.resLoaded = false;
	        this.netConnectSuccess = false;
	        this.closeView();
	    }
	    openView(sceneName = ESceneName.UI_LoginLoading) {
	        SceneManager.Instance.openScene(sceneName, null, true, Laya.Handler.create(this, this.onOpened));
	    }
	    closeView() {
	        EventManager.Instance.event(EEventType.CloseLoginLoadingView);
	    }
	    onOpened() {
	        this.beginLoadRes();
	        this.startLogin();
	        Laya.timer.loop(100, this, this.checkFinish);
	    }
	    checkFinish() {
	        if (this.resLoaded && this.netConnectSuccess) {
	            EventManager.Instance.event(EEventType.LoginSuccess);
	            Laya.timer.clear(this, this.checkFinish);
	        }
	    }
	    beginLoadRes() {
	        let configArr = ConfigDataManager.Instance.getAssetLoadUrls();
	        var assetArr = [];
	        let allAll = Util.union(assetArr, configArr);
	        Laya.loader.load(allAll, Laya.Handler.create(this, this.onLoadedResComplete), Laya.Handler.create(this, this.onLoadedResProgress, null, false));
	    }
	    onLoadedResComplete() {
	        LogM.debug("onLoadedResComplete");
	        this.resLoaded = true;
	    }
	    onLoadedResProgress(progress) {
	        EventManager.Instance.event(EEventType.LoginLoadingProgress, progress);
	    }
	    startLogin() {
	        let serverInfo = this.serverIpPort;
	        LogM.info("startLogin:" + serverInfo);
	        let proto = "ws://";
	        if (serverInfo.indexOf('.com') >= 0 || serverInfo.indexOf(".cn") >= 0) {
	            proto = 'wss://';
	        }
	        NetSocketManager.Instance.Init(proto + serverInfo);
	    }
	    onOpenSocketSuccess() {
	        LogM.info("onOpenSocketSuccess");
	        this.c_player_login();
	    }
	    c_player_login() {
	        NetSocketManager.Instance.SendMsg("c_player_login", {
	            token: Util.string2Byte("aaaa"),
	            sn: 1,
	        });
	    }
	    s_player_login(data) {
	        if (data.code == awesomepackage.e_code.ok) {
	            this.netConnectSuccess = true;
	            TimeManager.Instance.initServerTime(Number(data.time));
	            return;
	        }
	        LogM.info("s_player_login error" + data.code);
	        TipManager.Instance.showMsgById(data.code);
	    }
	}

	class StateGameLogin extends AbsStateGame {
	    enter() {
	        super.enter();
	        this.enterModules();
	        LoginManager.Instance.openView();
	    }
	    initOnlyOnce() {
	        this.mods.push(LoginManager.Instance);
	        EventManager.Instance.on(EEventType.LoginSuccess, this, this.onLoginSuccess);
	    }
	    onLoginSuccess() {
	        ConfigDataManager.Instance.parse();
	        GameStateManager.Instance.switchState(TypeStateGame.Main);
	    }
	    exit() {
	        super.exit();
	        this.exitModules();
	    }
	}

	class SwitchManager extends Singleton {
	    constructor() {
	        super(...arguments);
	        this.handler = null;
	        this.skPath = "res/yunwu/a_yunwu.sk";
	        this.templet = null;
	        this.skIndex = 0;
	    }
	    static get Instance() {
	        return SwitchManager.getInstance();
	    }
	    openView(handler = null) {
	        this.handler = handler;
	        if (!this.templet) {
	            let templet = new Laya.Templet();
	            templet.on(Laya.Event.COMPLETE, this, this.completeTemplet, [templet]);
	            templet.loadAni(this.skPath);
	        }
	        else {
	            this.createSK();
	        }
	    }
	    closeView() {
	        LogM.debug("云雾散开--------------------->1");
	        if (!this.sk) {
	            return;
	        }
	        LogM.debug("云雾散开--------------------->2");
	        this.sk.on(Laya.Event.STOPPED, this, this.completePlayerHandler1);
	        this.sk.play("open", false);
	    }
	    completePlayerHandler1() {
	        this.clearSK();
	    }
	    completeTemplet(templet) {
	        this.templet = templet;
	        this.createSK();
	    }
	    createSK() {
	        this.clearSK();
	        this.sk = this.templet.buildArmature(0);
	        Laya.stage.addChild(this.sk);
	        this.sk.zOrder = 1001;
	        this.sk.pos(Laya.stage.width / 2, Laya.stage.height / 2);
	        this.sk.on(Laya.Event.STOPPED, this, this.completePlayerHandler);
	        this.sk.play("close", false);
	        Laya.stage.on(Laya.Event.CLICK, this, this._stopSwitchClick);
	    }
	    _stopSwitchClick(e) {
	        e.stopPropagation();
	    }
	    completePlayerHandler() {
	        this.runHandler();
	    }
	    runHandler() {
	        if (this.handler) {
	            this.handler.run();
	            this.handler = null;
	        }
	    }
	    clearSK() {
	        if (this.sk) {
	            this.sk.removeSelf();
	            this.sk.destroy(true);
	        }
	        this.sk = null;
	        Laya.stage.off(Laya.Event.CLICK, this, this._stopSwitchClick);
	    }
	}

	class LobbyManager extends AbsGameModule {
	    constructor() {
	        super(...arguments);
	        this.initedGame = false;
	    }
	    static get Instance() {
	        return LobbyManager.getInstance();
	    }
	    clear() {
	        this.initedGame = false;
	    }
	    openView(initedGame = false) {
	        this.initedGame = initedGame;
	        SceneManager.Instance.openScene(ESceneName.UI_Lobby, null, false, Laya.Handler.create(this, this.openViewCallback));
	    }
	    openViewCallback() {
	        this.refreshView();
	        SwitchManager.Instance.closeView();
	    }
	    refreshView(initedGame = false) {
	        this.initedGame = initedGame || this.initedGame;
	        if (this.initedGame) {
	            EventManager.Instance.event(EEventType.LobbyRefreshView);
	        }
	    }
	}

	class MailManager extends AbsGameModule {
	    constructor() {
	        super();
	        this.mMails = {};
	        this.mTabIds = {};
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_mail_list, this, this.s_mail_list);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_add_mail, this, this.s_add_mail);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_delete_mails, this, this.s_delete_mails);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_mail_reward, this, this.s_get_mail_reward);
	    }
	    static get Instance() {
	        return MailManager.getInstance();
	    }
	    clear() {
	        this.mMails = {};
	        this.mTabIds = {};
	    }
	    getMailListByTab(tab) {
	        let tabIds = this.mTabIds[tab];
	        if (!tabIds) {
	            return [];
	        }
	        let tabMails = [];
	        for (let mailId of tabIds) {
	            tabMails.push(this.mMails[mailId]);
	        }
	        return tabMails;
	    }
	    getMailIdListByTab(tab) {
	        let tabIds = this.mTabIds[tab];
	        if (!tabIds) {
	            return [];
	        }
	        return tabIds;
	    }
	    getMailTitle(mail) {
	        let title = Util.byteToString(mail.title);
	        if (title.length > 0) {
	            return title;
	        }
	        return MailConfigManager.Instance.getMailTitle(mail.mail_template_id);
	    }
	    getMailContent(mail) {
	        let content = Util.byteToString(mail.content);
	        if (content.length > 0) {
	            return content;
	        }
	        return MailConfigManager.Instance.getMailContent(mail.mail_template_id);
	    }
	    c_mail_list() {
	        NetSocketManager.Instance.SendMsg("c_mail_list", {});
	    }
	    s_mail_list(data) {
	        for (let mail of data.mail_list) {
	            this.mMails[Number(mail.mail_id)] = mail;
	            if (!this.mTabIds[mail.tab])
	                this.mTabIds[mail.tab] = [];
	            this.mTabIds[mail.tab].push(Number(mail.mail_id));
	        }
	        for (let k in this.mTabIds) {
	            this.mTabIds[k].sort((a, b) => b - a);
	        }
	    }
	    s_add_mail(data) {
	        let mail = data.mail_info;
	        this.mMails[Number(mail.mail_id)] = mail;
	        if (!this.mTabIds[mail.tab])
	            this.mTabIds[mail.tab] = [];
	        this.mTabIds[mail.tab].unshift(Number(mail.mail_id));
	        EventManager.Instance.event(EEventType.MailListUpdate);
	    }
	    c_read_mails(mailIds) {
	        for (let mailId of mailIds) {
	            this.mMails[mailId].is_read = true;
	        }
	        NetSocketManager.Instance.SendMsg("c_read_mails", { mail_id_list: mailIds });
	        EventManager.Instance.event(EEventType.MailRead);
	    }
	    c_delete_mails(mailIds) {
	        NetSocketManager.Instance.SendMsg("c_delete_mails", { mail_id_list: mailIds });
	    }
	    s_delete_mails(data) {
	        let mail;
	        let mailId;
	        for (let mailIdStr of data.mail_id_list) {
	            mailId = Number(mailIdStr);
	            let mail = this.mMails[mailId];
	            if (!mail)
	                continue;
	            delete this.mMails[mailId];
	            let tabIds = this.mTabIds[mail.tab];
	            let indexOf = tabIds.indexOf(mailId);
	            if (indexOf >= 0) {
	                this.mTabIds[mail.tab].splice(indexOf, 1);
	            }
	        }
	        EventManager.Instance.event(EEventType.MailListUpdate);
	    }
	    c_save_mail(mailId) {
	        let toTab = 3;
	        NetSocketManager.Instance.SendMsg("c_save_mail", { mail_id: mailId });
	        let mail = this.mMails[mailId];
	        if (!mail || mail.tab == toTab)
	            return;
	        let tabIds = this.mTabIds[mail.tab];
	        let indexOf = tabIds.indexOf(mailId);
	        if (indexOf >= 0) {
	            this.mTabIds[mail.tab].splice(indexOf, 1);
	        }
	        mail.tab = toTab;
	        if (!this.mTabIds[mail.tab])
	            this.mTabIds[mail.tab] = [];
	        this.mTabIds[mail.tab].push(Number(mail.mail_id));
	        this.mTabIds[toTab].sort((a, b) => b - a);
	        EventManager.Instance.event(EEventType.MailListUpdate);
	    }
	    c_get_mail_reward(mailId) {
	        NetSocketManager.Instance.SendMsg("c_get_mail_reward", { mail_id: mailId });
	    }
	    s_get_mail_reward(data) {
	        if (data.code != awesomepackage.e_code.ok) {
	            TipManager.Instance.showMsgById(data.code);
	            return;
	        }
	        if (data.mail_id > 0) {
	            let mailId = Number(data.mail_id);
	            if (this.mMails[mailId]) {
	                this.mMails[mailId].is_reward = true;
	                this.mMails[mailId].is_read = true;
	                EventManager.Instance.event(EEventType.MailRewardSuccess, mailId);
	            }
	        }
	        else {
	            let mailList = this.getMailListByTab(0);
	            for (let mail of mailList) {
	                if (mail.reward_list.length > 0 && !mail.is_reward) {
	                    mail.is_reward = true;
	                    mail.is_read = true;
	                }
	            }
	        }
	        SceneManager.Instance.openRewardPopup(data.reward_list);
	    }
	}

	class RankManager extends AbsGameModule {
	    static get Instance() {
	        return RankManager.getInstance();
	    }
	    constructor() {
	        super();
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_player_rank_list, this, this.s_get_player_rank_list);
	    }
	    c_get_player_rank_list(rankType, nowPage = 1) {
	        NetSocketManager.Instance.SendMsg("c_get_player_rank_list", {
	            rank_type: rankType,
	            nowPage: nowPage,
	        });
	    }
	    s_get_player_rank_list(data) {
	    }
	    c_get_guild_rank_list(nowPage = 1) {
	        NetSocketManager.Instance.SendMsg("c_get_guild_rank_list", {
	            nowPage: nowPage,
	        });
	    }
	    s_get_guild_rank_list(data) {
	    }
	}

	class BattleManager extends AbsGameModule {
	    static get Instance() {
	        return BattleManager.getInstance();
	    }
	    constructor() {
	        super();
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_start_battle, this, this.s_start_battle);
	    }
	    s_start_battle(data) {
	        let assets = [
	            { url: EPrefabPath.BoxSoldier, type: Laya.Loader.PREFAB },
	            { url: EPrefabPath.BattleHp, type: Laya.Loader.PREFAB },
	        ];
	        Laya.loader.load(assets, Laya.Handler.create(this, this.onLoadedBattleResComplete, [data]));
	    }
	    onLoadedBattleResComplete(data) {
	        SceneManager.Instance.openScene(ESceneName.UI_Battle, [data]);
	    }
	}

	class StateGameMain extends AbsStateGame {
	    enter() {
	        super.enter();
	        this.enterModules();
	        let prevLoginState = GameStateManager.Instance.prevState == TypeStateGame.Login;
	        LobbyManager.Instance.openView(!prevLoginState);
	        if (prevLoginState) {
	            this.sendInitGameProto();
	        }
	    }
	    initOnlyOnce() {
	        this.mods.push(PlayerManager.Instance);
	        this.mods.push(LobbyManager.Instance);
	        this.mods.push(BagManager.Instance);
	        this.mods.push(GeneralManager.Instance);
	        this.mods.push(MailManager.Instance);
	        this.mods.push(RankManager.Instance);
	        this.mods.push(BattleManager.Instance);
	    }
	    exit() {
	        super.exit();
	        this.exitModules();
	    }
	    sendInitGameProto() {
	        PlayerManager.Instance.c_get_player_info();
	        BagManager.Instance.c_get_bag_goods_list();
	        GeneralManager.Instance.c_get_general_list();
	        MailManager.Instance.c_mail_list();
	        LobbyManager.Instance.refreshView(true);
	    }
	}

	class TiledUtil {
	    static getEventDistance2(points) {
	        let distance = 0;
	        if (points && points.length == 2) {
	            let dx = points[0].stageX - points[1].stageX;
	            let dy = points[0].stageY - points[1].stageY;
	            distance = Math.sqrt(dx * dx + dy * dy);
	        }
	        return distance;
	    }
	    static getLogicPosByMapGridPos(gx, gy) {
	        if (typeof gx !== "number") {
	            gy = gx.y;
	            gx = gx.x;
	        }
	        if (gy % 2 == 0) {
	            gx = 2 * gx;
	        }
	        else {
	            gx = 2 * gx + 1;
	        }
	        return new Laya.Point(gx, gy);
	    }
	    static getMapGridPosByLogicPos(lx, ly) {
	        if (typeof lx !== "number") {
	            ly = lx.y;
	            lx = lx.x;
	        }
	        if (ly % 2 == 0) {
	            lx = Math.floor(lx / 2);
	        }
	        else {
	            lx = Math.floor((lx - 1) / 2);
	        }
	        return new Laya.Point(lx, ly);
	    }
	}

	class AniManager extends Singleton {
	    static get Instance() {
	        return AniManager.getInstance();
	    }
	    newAnimation(loop = false) {
	        let ani = Laya.Pool.getItemByClass(EPoolName.Animation, Laya.Animation);
	        ani.loop = loop;
	        return ani;
	    }
	    recover(ani) {
	        if (!ani)
	            return;
	        ani.removeSelf();
	        ani.clear();
	        Laya.Pool.recover(EPoolName.Animation, ani);
	    }
	}

	var EWorldCityType;
	(function (EWorldCityType) {
	    EWorldCityType[EWorldCityType["Invalid"] = -1] = "Invalid";
	    EWorldCityType[EWorldCityType["Empty"] = 0] = "Empty";
	    EWorldCityType[EWorldCityType["Useless"] = 1] = "Useless";
	    EWorldCityType[EWorldCityType["Main_City_Normal"] = 2] = "Main_City_Normal";
	    EWorldCityType[EWorldCityType["Main_City_Appear"] = 3] = "Main_City_Appear";
	    EWorldCityType[EWorldCityType["Monster1"] = 4] = "Monster1";
	    EWorldCityType[EWorldCityType["Monster2"] = 5] = "Monster2";
	    EWorldCityType[EWorldCityType["Monster3"] = 6] = "Monster3";
	    EWorldCityType[EWorldCityType["EliteMonster"] = 7] = "EliteMonster";
	    EWorldCityType[EWorldCityType["_Boss1"] = 8] = "_Boss1";
	    EWorldCityType[EWorldCityType["_Resources1"] = 9] = "_Resources1";
	    EWorldCityType[EWorldCityType["_Resources2"] = 10] = "_Resources2";
	    EWorldCityType[EWorldCityType["_StaticCity"] = 11] = "_StaticCity";
	    EWorldCityType[EWorldCityType["GuildLand"] = 12] = "GuildLand";
	    EWorldCityType[EWorldCityType["_CenterCity"] = 13] = "_CenterCity";
	    EWorldCityType[EWorldCityType["RobResources"] = 14] = "RobResources";
	    EWorldCityType[EWorldCityType["LongCheng"] = 15] = "LongCheng";
	    EWorldCityType[EWorldCityType["MaxCount"] = 16] = "MaxCount";
	})(EWorldCityType || (EWorldCityType = {}));
	const MonsterMinLv = 5;
	const MonsterMaxLv = 70;

	class WorldItem {
	    constructor() {
	        this.vo = null;
	        this.citySize = 1;
	        this.itemAnchorPos = new Laya.Point(0, 0);
	        this.img_city = null;
	        this.img_nameBg = null;
	        this.img_lvBg = null;
	        this.label_name = null;
	        this.label_lv = null;
	        this.ani_fire = null;
	        this.ani_protect = null;
	        this.img_city = new Laya.Image();
	        this.img_city.anchorX = 0.5;
	        this.img_city.anchorY = 0.5;
	        this.img_nameBg = new Laya.Image("image/worldMap/label_bg.png");
	        this.img_nameBg.anchorX = 0.5;
	        this.img_nameBg.anchorY = 0.5;
	        this.img_nameBg.pivot(0.5, 0.5);
	        this.img_nameBg.alpha = 0.5;
	        this.img_lvBg = new Laya.Image("image/worldMap/label_bg.png");
	        this.img_lvBg.anchorX = 0.5;
	        this.img_lvBg.anchorY = 0.5;
	        this.img_lvBg.zOrder = 1;
	        this.img_lvBg.alpha = 0.5;
	        this.label_name = new Laya.Label();
	        this.label_name.width = 160;
	        this.label_name.anchorX = 0.5;
	        this.label_name.anchorY = 0.5;
	        this.label_name.font = "Arial";
	        this.label_name.fontSize = 20;
	        this.label_name.color = "#53b2da";
	        this.label_name.align = "center";
	        this.label_name.valign = "middle";
	        this.label_name.zOrder = 2;
	        this.label_lv = new Laya.Label();
	        this.label_lv.width = 160;
	        this.label_lv.anchorX = 0.5;
	        this.label_lv.anchorY = 0.5;
	        this.label_lv.font = "Arial";
	        this.label_lv.fontSize = 24;
	        this.label_lv.bold = true;
	        this.label_lv.color = "#ffb03f";
	        this.label_lv.align = "center";
	        this.label_lv.valign = "middle";
	        this.label_lv.zOrder = 2;
	    }
	    init(vo) {
	        this.vo = vo;
	        this.nMapBlockX = Math.floor(this.vo.x / Common.MapBlockGridWidth);
	        this.nMapBlockY = Math.floor(this.vo.y / Common.MapBlockGridHeight);
	        this.citySize = WorldConfigManager.Instance.getCitySize(vo.type);
	        this.itemAnchorPos = WorldManager.Instance.getItemAnchorPixelPos(vo.x, vo.y, this.citySize);
	        this.img_lvBg.visible = true;
	        this.label_lv.visible = true;
	        this.label_name.color = "#53b2da";
	        this.label_lv.color = "#ffb03f";
	        switch (vo.type) {
	            case EWorldCityType.Monster1:
	            case EWorldCityType.Monster2:
	            case EWorldCityType.Monster3:
	                this.init_mon(true);
	                break;
	            case EWorldCityType.EliteMonster:
	                this.init_mon(false);
	                break;
	            case EWorldCityType.Main_City_Normal:
	            case EWorldCityType.Main_City_Appear:
	            case EWorldCityType.LongCheng:
	                this.init_main_city(vo);
	                break;
	            case EWorldCityType.GuildLand:
	                this.init_guild_land(vo);
	                break;
	        }
	        this.img_city.skin = WorldConfigManager.Instance.getCityIcon(vo.type);
	        this.img_city.pos(this.itemAnchorPos.x, this.itemAnchorPos.y);
	        WorldManager.Instance.mCitysSp.addChild(this.img_city);
	        let charWidth = 12;
	        this.label_name.x = this.itemAnchorPos.x;
	        this.label_name.y = this.itemAnchorPos.y + 20;
	        this.img_nameBg.width = Util.getLength(this.label_name.text) * charWidth;
	        this.img_nameBg.x = this.label_name.x;
	        this.img_nameBg.y = this.label_name.y;
	        WorldManager.Instance.mCityLabelsSp.addChild(this.img_nameBg);
	        WorldManager.Instance.mCityLabelsSp.addChild(this.label_name);
	        if (this.label_lv.visible) {
	            this.label_lv.x = this.itemAnchorPos.x;
	            this.label_lv.y = this.itemAnchorPos.y + 40;
	            this.img_lvBg.width = Util.getLength(this.label_lv.text) * charWidth;
	            this.img_lvBg.x = this.label_lv.x;
	            this.img_lvBg.y = this.label_lv.y;
	            WorldManager.Instance.mCityLabelsSp.addChild(this.img_lvBg);
	            WorldManager.Instance.mCityLabelsSp.addChild(this.label_lv);
	        }
	    }
	    init_mon(IsNormalMon) {
	        let baseCityInfo = WorldConfigManager.Instance.getBaseCityTypeInfo(this.vo.type);
	        this.label_name.text = baseCityInfo.name;
	        if (IsNormalMon) {
	            let lv = this.vo.lv + PlayerManager.Instance.mainPlayer.lv_mon_refresh;
	            lv = lv < MonsterMinLv ? MonsterMinLv : lv;
	            lv = lv > MonsterMaxLv ? 70 : MonsterMaxLv;
	            this.label_lv.text = "LV." + lv;
	        }
	        else {
	            this.label_lv.text = "LV." + this.vo.lv;
	        }
	    }
	    init_main_city(vo) {
	        let player = PlayerManager.Instance.mainPlayer;
	        if (vo.player_id == player.id) {
	            this.label_name.text = PlayerManager.Instance.mainPlayer.nickname + "(" + player.x + "," + player.y + ")";
	            this.img_lvBg.visible = false;
	            this.label_lv.visible = false;
	            this.label_name.color = "#00ff00";
	        }
	        else {
	            this.label_lv.text = "LV." + vo.lv;
	            let nickname = Util.cutOutString(Util.byteToString(vo.nickname), 8);
	            if (vo.guild_name) {
	                this.label_name.text = "[" + Util.byteToString(vo.guild_name) + "]" + nickname;
	            }
	        }
	        if (vo.state === awesomepackage.e_city_state.fire || vo.state === awesomepackage.e_city_state.smoking) {
	            this.createAniFire();
	        }
	        if (vo.protect_end_time > TimeManager.Instance.serverTime) {
	            this.createAniProtect();
	        }
	    }
	    createAniFire() {
	        this.ani_fire = AniManager.Instance.newAnimation(true);
	        this.ani_fire.loadAtlas(EAniPath.WorldFire, null, EAniPath.WorldFire);
	        this.ani_fire.zOrder = 2;
	        this.ani_fire.pos(this.itemAnchorPos.x, this.itemAnchorPos.y);
	        this.ani_fire.pivot(110 / 2, 110 / 2);
	        this.ani_fire.scale(this.citySize, this.citySize);
	        this.ani_fire.play();
	        WorldManager.Instance.mCitysSp.addChild(this.ani_fire);
	    }
	    createAniProtect() {
	        this.ani_protect = AniManager.Instance.newAnimation(true);
	        this.ani_protect.loadAtlas(EAniPath.WorldProtect, null, EAniPath.WorldProtect);
	        this.ani_protect.zOrder = 3;
	        this.ani_protect.pos(this.itemAnchorPos.x, this.itemAnchorPos.y);
	        this.ani_protect.pivot(150 / 2, 150 / 2);
	        this.ani_protect.scale(this.citySize, this.citySize);
	        this.ani_protect.play();
	        WorldManager.Instance.mCitysSp.addChild(this.ani_protect);
	    }
	    init_guild_land(vo) {
	        this.img_lvBg.visible = false;
	        this.label_lv.visible = false;
	        this.label_name.text = "[" + Util.byteToString(vo.guild_name) + "]" + Util.byteToString(vo.guild_full_name);
	    }
	    init_rob(vo) {
	        this.img_lvBg.visible = false;
	        this.label_lv.visible = false;
	        this.label_name.text = "物资";
	        this.label_name.color = "#ffe79e";
	    }
	    recover() {
	        this.vo = null;
	        this.img_city && this.img_city.removeSelf();
	        this.img_nameBg && this.img_nameBg.removeSelf();
	        this.img_lvBg && this.img_lvBg.removeSelf();
	        this.label_name && this.label_name.removeSelf();
	        this.label_lv && this.label_lv.removeSelf();
	        Laya.Pool.recover(EPoolName.WorldItem, this);
	        AniManager.Instance.recover(this.ani_fire);
	        AniManager.Instance.recover(this.ani_protect);
	        this.ani_fire = null;
	        this.ani_protect = null;
	    }
	    destroy() {
	        this.img_city && this.img_city.destroy();
	        this.img_nameBg && this.img_nameBg.destroy();
	        this.img_lvBg && this.img_lvBg.destroy();
	        this.label_name && this.label_name.destroy();
	        this.label_lv && this.label_lv.destroy();
	    }
	}

	class WorldItemManager extends Singleton {
	    constructor() {
	        super();
	        this.cityItems = {};
	        this.mPoss = {};
	        this.lastSendCenterBlock = new Laya.Point(0, 0);
	        this.lastRecvCenterBlock = new Laya.Point(0, 0);
	    }
	    static get Instance() {
	        return WorldItemManager.getInstance();
	    }
	    scene_enter() {
	        this.lastRecvCenterBlock.setTo(0, 0);
	        this.lastSendCenterBlock.setTo(0, 0);
	        this.cityItems = {};
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_pvp_citys, this, this.s_get_pvp_citys);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_del_city, this, this.s_del_city);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_refresh_city_info, this, this.s_refresh_city_info);
	    }
	    scene_exit() {
	        EventManager.Instance.offAllCaller(this);
	        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_get_pvp_citys, this.s_get_pvp_citys);
	        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_del_city, this.s_del_city);
	        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_refresh_city_info, this.s_refresh_city_info);
	        for (let key in this.cityItems) {
	            this.cityItems[key].recover();
	        }
	        this.cityItems = {};
	        this.mPoss = {};
	        let objs = Laya.Pool.getPoolBySign(EPoolName.WorldItem);
	        for (let item of objs) {
	            item.destroy();
	        }
	        Laya.Pool.clearBySign(EPoolName.WorldItem);
	    }
	    spawnWorldItem(cityInfo, isCover = false) {
	        let virutalId = this.makeItemVirtual(cityInfo.x, cityInfo.y);
	        let oldCityItem = this.cityItems[virutalId];
	        if (!oldCityItem) {
	            this.spawnWorldItemNew(cityInfo);
	        }
	        else if (isCover || cityInfo.type != oldCityItem.vo.type) {
	            this.delWorldItem(oldCityItem);
	            this.spawnWorldItemNew(cityInfo);
	        }
	    }
	    spawnWorldItemNew(cityInfo) {
	        let virutalId = this.makeItemVirtual(cityInfo.x, cityInfo.y);
	        let item = Laya.Pool.getItemByClass(EPoolName.WorldItem, WorldItem);
	        item.init(cityInfo);
	        this.cityItems[virutalId] = item;
	        this.addMapPos(item);
	    }
	    delWorldItem(item) {
	        this.delMapPos(item);
	        let virutalId = this.makeItemVirtual(item.vo.x, item.vo.y);
	        delete this.cityItems[virutalId];
	        item.recover();
	    }
	    clearWorldItems(forceClearAll = false) {
	        let block = this.lastRecvCenterBlock;
	        for (let index in this.cityItems) {
	            let item = this.cityItems[index];
	            if (Math.abs(item.nMapBlockX - block.x) <= 1 && Math.abs(item.nMapBlockY - block.y) <= 1) {
	                continue;
	            }
	            this.delWorldItem(item);
	        }
	    }
	    getWorldItemByPos(x, y) {
	        let virutalId = this.mPoss[this.makeItemVirtual(x, y)];
	        return this.cityItems[virutalId];
	    }
	    makeItemVirtual(x, y) {
	        return x * 10000 + y;
	    }
	    addMapPos(item) {
	        let size = WorldConfigManager.Instance.getCitySize(item.vo.type);
	        let baseVPos = this.makeItemVirtual(item.vo.x, item.vo.y);
	        let vposs = this.getCitySizeVirtualList(item.vo.x, item.vo.y, size);
	        for (let vpos of vposs) {
	            this.mPoss[vpos] = baseVPos;
	        }
	    }
	    delMapPos(item) {
	        let size = WorldConfigManager.Instance.getCitySize(item.vo.type);
	        let vposs = this.getCitySizeVirtualList(item.vo.x, item.vo.y, size);
	        for (let vpos of vposs) {
	            delete this.mPoss[vpos];
	        }
	    }
	    getCitySizeVirtualList(x, y, size) {
	        let resust = [];
	        if (size == 1) {
	            resust.push(this.makeItemVirtual(x, y));
	        }
	        else if (size == 2) {
	            resust.push(this.makeItemVirtual(x, y));
	            resust.push(this.makeItemVirtual(x + 1, y + 1));
	            resust.push(this.makeItemVirtual(x + 2, y));
	            resust.push(this.makeItemVirtual(x + 1, y - 1));
	        }
	        else if (size == 3) {
	            resust.push(this.makeItemVirtual(x, y));
	            resust.push(this.makeItemVirtual(x + 1, y + 1));
	            resust.push(this.makeItemVirtual(x + 2, y));
	            resust.push(this.makeItemVirtual(x + 1, y - 1));
	            resust.push(this.makeItemVirtual(x + 2, y + 2));
	            resust.push(this.makeItemVirtual(x + 3, y + 1));
	            resust.push(this.makeItemVirtual(x + 2, y - 2));
	            resust.push(this.makeItemVirtual(x + 3, y - 1));
	            resust.push(this.makeItemVirtual(x + 4, y));
	        }
	        return resust;
	    }
	    checkFetchPvpCitys() {
	        let curCenterBlock = WorldManager.Instance.getCurCenterBlockPos();
	        if (Util.checkPointEqual(this.lastSendCenterBlock, curCenterBlock)) {
	            return;
	        }
	        let forceFullRefresh = true;
	        if (this.lastSendCenterBlock.x != 0 || this.lastSendCenterBlock.y != 0) {
	            if (Util.checkPointEqual(this.lastSendCenterBlock, this.lastRecvCenterBlock)) {
	                let diffBlockX = Math.abs(this.lastSendCenterBlock.x - curCenterBlock.x);
	                let diffBlockY = Math.abs(this.lastSendCenterBlock.y - curCenterBlock.y);
	                if (diffBlockX <= 1 && diffBlockY <= 1) {
	                    forceFullRefresh = false;
	                }
	            }
	        }
	        this.c_get_pvp_citys(curCenterBlock.x, curCenterBlock.y, forceFullRefresh);
	    }
	    c_get_pvp_citys(CenterBlockX, CenterBlockY, ForceFullRefresh = true) {
	        if (!WorldManager.Instance.mapLoaded)
	            return;
	        this.lastSendCenterBlock.x = CenterBlockX;
	        this.lastSendCenterBlock.y = CenterBlockY;
	        let message = { "center_block_x": CenterBlockX, "center_block_y": CenterBlockY, "force_full_refresh": ForceFullRefresh };
	        NetSocketManager.Instance.SendMsg("c_get_pvp_citys", message);
	    }
	    s_get_pvp_citys(data) {
	        if (!WorldManager.Instance.mapLoaded)
	            return;
	        if (data.center_block_x != this.lastSendCenterBlock.x || data.center_block_y != this.lastSendCenterBlock.y) {
	            return;
	        }
	        this.lastRecvCenterBlock.x = data.center_block_x;
	        this.lastRecvCenterBlock.y = data.center_block_y;
	        this.clearWorldItems();
	        for (let index = 0; index < data.citys.length; index++) {
	            this.spawnWorldItem(data.citys[index]);
	        }
	    }
	    s_refresh_city_info(data) {
	        if (!WorldManager.Instance.mapLoaded)
	            return;
	        let cityInfo = data.city_info;
	        this.spawnWorldItem(cityInfo, true);
	    }
	    s_del_city(data) {
	        if (!WorldManager.Instance.mapLoaded)
	            return;
	        let virutalId = this.makeItemVirtual(data.x, data.y);
	        let item = this.cityItems[virutalId];
	        if (item) {
	            this.delWorldItem(item);
	        }
	    }
	    clickItem(x, y) {
	        let item = this.getWorldItemByPos(x, y);
	        if (!item) {
	            return false;
	        }
	        switch (item.vo.type) {
	            case EWorldCityType.Monster1:
	            case EWorldCityType.Monster2:
	            case EWorldCityType.Monster3:
	            case EWorldCityType.EliteMonster:
	                SceneManager.Instance.openScene(ESceneName.UI_WorldMonster, [item], false, Laya.Handler.create(this, this.clickItemCallback, [item]));
	                break;
	            case EWorldCityType.Main_City_Normal:
	            case EWorldCityType.Main_City_Appear:
	            case EWorldCityType.LongCheng:
	                SceneManager.Instance.openScene(ESceneName.UI_WorldPlayerCity, [item], false, Laya.Handler.create(this, this.clickItemCallback, [item]));
	                break;
	        }
	        return true;
	    }
	    clickItemCallback(item, dialog) {
	        if (!item || !dialog) {
	            return;
	        }
	        console.log("2222222222222222");
	        let vo = item.vo;
	        let scale = WorldManager.Instance.worldTileMap.scale;
	        const checkX = 100;
	        const checkY = 100;
	        const fix = 20;
	        const dialogWidth = dialog.width;
	        const dialogHeight = dialog.height;
	        const stageWidth = Laya.stage.width;
	        const stageHeight = Laya.stage.height;
	        const itemHalfWidth = Common.TileWidth * item.citySize / 2 * scale;
	        const itemHalfHeight = Common.TileWidth * item.citySize / 2 * scale;
	        let itemCenterAnchorPos = WorldManager.Instance.getItemAnchorPixelPos(vo.x, vo.y, item.citySize);
	        let itemCenterScreenX = (itemCenterAnchorPos.x - WorldManager.Instance.mapX) * scale;
	        let itemCenterScreenY = (itemCenterAnchorPos.y - WorldManager.Instance.mapY) * scale;
	        console.log(itemCenterScreenX, itemCenterScreenY);
	        if (itemCenterScreenX < checkX && itemCenterScreenY - dialogHeight / 2 >= checkY && stageHeight - itemCenterScreenY - dialogHeight / 2 >= checkY) {
	            dialog.x = itemCenterScreenX + itemHalfWidth + fix;
	            dialog.y = itemCenterScreenY - dialogHeight / 2;
	            return;
	        }
	        if (stageWidth - itemCenterScreenX < checkX && itemCenterScreenY - dialogHeight / 2 >= checkY && stageHeight - itemCenterScreenY - dialogHeight / 2 >= checkY) {
	            dialog.x = itemCenterScreenX - itemHalfWidth - dialogWidth - fix;
	            dialog.y = itemCenterScreenY - dialogHeight / 2;
	            return;
	        }
	        if (itemCenterScreenY >= stageHeight / 2) {
	            dialog.y = itemCenterScreenY - itemHalfHeight - dialogHeight;
	        }
	        else {
	            dialog.y = itemCenterScreenY + itemHalfHeight;
	        }
	        if (itemCenterScreenX <= dialogWidth / 2) {
	            dialog.x = fix;
	        }
	        else if (stageWidth - itemCenterScreenX < dialogWidth / 2) {
	            dialog.x = stageWidth - dialogWidth - fix;
	        }
	        else {
	            dialog.x = itemCenterScreenX - dialogWidth / 2;
	        }
	    }
	}

	class WorldLine {
	    constructor() {
	        this.color = "green";
	        this.totalTime = 0;
	        this.toPosSize = 1;
	        this.lineDis = 0;
	        this.lineAngle = 0;
	        this.isArrive = false;
	        this.animSpeed = 0;
	        this.sp_line = null;
	        this.ani_fight = null;
	        this.ani_army = null;
	        this.sp_army = null;
	        this.sp_line = new Laya.Sprite();
	        this.sp_army = new Laya.Sprite();
	    }
	    init(vo) {
	        this.vo = vo;
	        this.vo.op == 1 && (this.vo.arriveTime -= 1);
	        this.isArrive = false;
	        this.color = "#FFFF00";
	        if (this.vo.playerId === PlayerManager.Instance.mainPlayer.id) {
	            this.color = "#00FF00";
	        }
	        this.startPos = WorldManager.Instance.getItemAnchorPixelPos(this.vo.startX, this.vo.startY, WorldConfigManager.Instance.getCitySize(this.vo.start_city_type));
	        this.toPos = WorldManager.Instance.getItemAnchorPixelPos(this.vo.toX, this.vo.toY, WorldConfigManager.Instance.getCitySize(this.vo.to_city_type));
	        this.lineDis = Util.getDistance(this.startPos, this.toPos);
	        this.lineAngle = Util.getAngle(this.startPos, this.toPos);
	        this.totalTime = this.vo.arriveTime - this.vo.startTime;
	        if (this.totalTime > 0) {
	            this.animSpeed = this.lineDis / this.totalTime;
	        }
	        this.sp_line.graphics.clear();
	        this.sp_line.graphics.drawLine(this.startPos.x, this.startPos.y, this.toPos.x, this.toPos.y, this.color);
	        WorldManager.Instance.mFightLineSp.addChild(this.sp_line);
	    }
	    recover() {
	        AniManager.Instance.recover(this.ani_fight);
	        AniManager.Instance.recover(this.ani_army);
	        if (this.sp_line) {
	            this.sp_line.removeSelf();
	            this.sp_line.graphics.clear();
	        }
	        if (this.sp_army) {
	            this.sp_army.removeSelf();
	        }
	        Laya.Pool.recover(EPoolName.WorldLine, this);
	        this.ani_fight = null;
	        this.ani_army = null;
	    }
	    destroy() {
	        this.sp_line && this.sp_line.destroy();
	        this.sp_army && this.sp_army.destroy();
	    }
	    updateAnim() {
	        if (this.lineDis <= 0 || this.totalTime <= 0 || this.isArrive) {
	            return;
	        }
	        if (TimeManager.Instance.serverTime >= this.vo.arriveTime) {
	            if (this.vo.op == 1) {
	                this.playerFightAni();
	            }
	            this.isArrive = true;
	            return;
	        }
	        if (!this.ani_army) {
	            this.createAniArmy();
	        }
	        let curAnimPos = this.getCurAnimPos();
	        this.sp_army.pos(curAnimPos.x, curAnimPos.y);
	    }
	    getCurAnimPos() {
	        let curAnimPos = new Laya.Point();
	        if (TimeManager.Instance.serverTime >= this.vo.arriveTime) {
	            curAnimPos = this.toPos;
	        }
	        else {
	            let moveLength = this.animSpeed * (TimeManager.Instance.serverMillTime - this.vo.startTime * 1000) / 1000;
	            let moveXLength = Math.cos(this.lineAngle * (Math.PI / 180)) * moveLength;
	            let moveYLength = Math.sin(this.lineAngle * (Math.PI / 180)) * moveLength;
	            curAnimPos.x = this.startPos.x + moveXLength;
	            curAnimPos.y = this.startPos.y + moveYLength;
	        }
	        return curAnimPos;
	    }
	    createAniArmy() {
	        let angle = Util.getAngle(this.startPos, this.toPos);
	        angle < 0 && (angle += 360);
	        let index = angle / 45;
	        if (index >= 7.5)
	            index = 0;
	        index = Math.round(index) + 1;
	        let atlasPath = "res/atlas/image/worldMap/armyAni/0" + index + ".atlas";
	        Laya.loader.load(atlasPath, Laya.Handler.create(this, this.doCreateArmyAnim, [atlasPath]));
	    }
	    doCreateArmyAnim(atlasPath) {
	        if (this.ani_army)
	            return;
	        this.ani_army = AniManager.Instance.newAnimation(true);
	        this.ani_army.loadAtlas(atlasPath, null, atlasPath);
	        this.ani_army.pivot(150, 150);
	        this.ani_army.play();
	        this.sp_army.addChild(this.ani_army);
	        WorldManager.Instance.mArmySp.addChild(this.sp_army);
	    }
	    playerFightAni() {
	        this.ani_fight = AniManager.Instance.newAnimation();
	        this.ani_fight.loadAtlas(EAniPath.WorldFight, null, EAniPath.WorldFight);
	        this.ani_fight.pivot(250 / 2, 220 / 2);
	        this.ani_fight.pos(this.toPos.x, this.toPos.y);
	        this.ani_fight.interval = 120;
	        let toCitySize = WorldConfigManager.Instance.getCitySize(this.vo.to_city_type);
	        this.ani_fight.scale(toCitySize, toCitySize);
	        WorldManager.Instance.mArmySp.addChild(this.ani_fight);
	        this.ani_fight.play();
	    }
	}

	class VOWorldLine {
	    constructor() {
	        this.armyId = 0;
	        this.startX = 0;
	        this.startY = 0;
	        this.toX = 0;
	        this.toY = 0;
	        this.startTime = 0;
	        this.arriveTime = 0;
	        this.playerId = 0;
	        this.op = 0;
	        this.nickname = "";
	    }
	    init(bytes1) {
	        let bytes = bytes1.slice(0);
	        let dv = new DataView(bytes.buffer);
	        let start = 0;
	        this.armyId = dv.getUint32(start);
	        this.startX = dv.getUint16(start + 4);
	        this.startY = dv.getUint16(start + 6);
	        this.toX = dv.getUint16(start + 8);
	        this.toY = dv.getUint16(start + 10);
	        this.startTime = dv.getUint32(start + 12);
	        this.arriveTime = dv.getUint32(start + 16);
	        this.playerId = dv.getUint32(start + 20);
	        this.op = dv.getUint8(start + 24);
	        this.start_city_type = dv.getUint8(start + 25);
	        this.to_city_type = dv.getUint8(start + 26);
	        this.nickname = Util.byteToString(bytes.subarray(27));
	    }
	    static getArmyId(bytes1) {
	        let bytes = bytes1.slice(0);
	        let dv = new DataView(bytes.buffer);
	        let start = 0;
	        return dv.getUint32(start);
	    }
	}

	class WorldLineManager extends Singleton {
	    constructor() {
	        super();
	        this.armyIdList = [];
	        this.lines = {};
	    }
	    static get Instance() {
	        return WorldLineManager.getInstance();
	    }
	    scene_enter() {
	        this.armyIdList = [];
	        this.lines = {};
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_pvp_lines, this, this.s_pvp_lines);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_pvp_add_line, this, this.s_pvp_add_line);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_pvp_del_line, this, this.s_pvp_del_line);
	        NetSocketManager.Instance.RegisterNet(awesomepackage.serviceNo.s_get_pvp_lines, this, this.s_get_pvp_lines);
	        Laya.timer.frameLoop(1, this, this.loopFrame);
	    }
	    scene_exit() {
	        EventManager.Instance.offAllCaller(this);
	        Laya.timer.clearAll(this);
	        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_pvp_lines, this.s_pvp_lines);
	        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_pvp_add_line, this.s_pvp_add_line);
	        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_pvp_del_line, this.s_pvp_del_line);
	        NetSocketManager.Instance.UnRegisterNet(awesomepackage.serviceNo.s_get_pvp_lines, this.s_get_pvp_lines);
	        for (let key in this.lines) {
	            this.lines[key].recover();
	        }
	        this.lines = {};
	        let objs = Laya.Pool.getPoolBySign(EPoolName.WorldLine);
	        for (let item of objs) {
	            item.destroy();
	        }
	        Laya.Pool.clearBySign(EPoolName.WorldLine);
	    }
	    loopFrame() {
	        for (let armyId in this.lines) {
	            let line = this.lines[armyId];
	            line.updateAnim();
	        }
	    }
	    s_pvp_lines(data) {
	        if (!WorldManager.Instance.loadWorldMap)
	            return;
	        let newArmyIdList = data.army_id_list;
	        for (let armyId of this.armyIdList) {
	            if (newArmyIdList.indexOf(armyId) === -1) {
	                this.delLine(armyId);
	            }
	        }
	        this.armyIdList = newArmyIdList;
	        let filterArmyIdList = [];
	        for (let armyId of this.armyIdList) {
	            if (!this.lines[armyId]) {
	                filterArmyIdList.push(armyId);
	            }
	        }
	        this.c_get_pvp_lines(filterArmyIdList);
	    }
	    c_get_pvp_lines(armyIdList) {
	        if (!WorldManager.Instance.loadWorldMap)
	            return;
	        if (!armyIdList || armyIdList.length == 0) {
	            return;
	        }
	        let message = { "army_id_list": armyIdList };
	        NetSocketManager.Instance.SendMsg("c_get_pvp_lines", message);
	    }
	    s_pvp_add_line(data) {
	        if (!WorldManager.Instance.loadWorldMap)
	            return;
	        let voLine = new VOWorldLine();
	        voLine.init(data.line);
	        let armyId = voLine.armyId;
	        if (this.armyIdList.indexOf(armyId) === -1) {
	            this.armyIdList.push(armyId);
	        }
	        this.addLine(voLine);
	    }
	    s_pvp_del_line(data) {
	        if (!WorldManager.Instance.loadWorldMap)
	            return;
	        this.delLine(data.army_id);
	    }
	    s_get_pvp_lines(data) {
	        if (!WorldManager.Instance.loadWorldMap)
	            return;
	        let voLine;
	        for (let lineBytes of data.lines) {
	            let armyId = VOWorldLine.getArmyId(lineBytes);
	            if (this.armyIdList.indexOf(armyId) >= 0) {
	                voLine = new VOWorldLine();
	                voLine.init(lineBytes);
	                this.addLine(voLine);
	            }
	        }
	    }
	    gmAddLine() {
	        let vo = new VOWorldLine();
	        vo.armyId = 1;
	        vo.startX = 13;
	        vo.startY = 877;
	        vo.toX = 40;
	        vo.toY = 1000;
	        vo.startTime = TimeManager.Instance.serverTime;
	        vo.arriveTime = TimeManager.Instance.serverTime + 120;
	        vo.playerId = 1;
	        vo.op = 1;
	        vo.start_city_type = 4;
	        vo.to_city_type = 4;
	        vo.nickname = "aaaa";
	        this.addLine(vo);
	    }
	    addLine(voLine) {
	        if (!WorldManager.Instance.loadWorldMap)
	            return;
	        let oldLine = this.lines[voLine.armyId];
	        if (!oldLine) {
	            let line = Laya.Pool.getItemByClass(EPoolName.WorldLine, WorldLine);
	            line.init(voLine);
	            this.lines[voLine.armyId] = line;
	        }
	        else {
	            oldLine.init(voLine);
	        }
	    }
	    delLine(armyId) {
	        let line = this.lines[armyId];
	        if (line) {
	            delete this.lines[armyId];
	            line.recover();
	        }
	    }
	}

	class WorldManager extends AbsGameModule {
	    constructor() {
	        super(...arguments);
	        this.mapLoaded = false;
	        this.worldTileMap = null;
	        this.itemLayer = null;
	        this.directionImge = null;
	        this.jumpHandler = null;
	        this._mapX = 0;
	        this._mapY = 0;
	        this.nMinMapX = 0;
	        this.nMinMapY = 0;
	        this.nMaxMapX = 0;
	        this.nMaxMapY = 0;
	        this.lastDistance = 0;
	        this.mLastMouseX = 0;
	        this.mLastMouseY = 0;
	        this.isScaleMap = false;
	        this.isMoveClick = false;
	    }
	    static get Instance() {
	        return WorldManager.getInstance();
	    }
	    scene_enter() {
	        this.mapLoaded = false;
	        WorldItemManager.Instance.scene_enter();
	        WorldLineManager.Instance.scene_enter();
	    }
	    scene_exit() {
	        this.mapLoaded = false;
	        EventManager.Instance.offAllCaller(this);
	        WorldItemManager.Instance.scene_exit();
	        WorldLineManager.Instance.scene_exit();
	    }
	    loadWorldMap() {
	        this.worldTileMap = new Laya.TiledMap();
	        var assetArr = [
	            { url: Common.WorldTileMapJson, type: Laya.Loader.JSON },
	            { url: EAniPath.WorldFire, type: Laya.Loader.ATLAS },
	            { url: EAniPath.WorldProtect, type: Laya.Loader.ATLAS },
	            { url: EAniPath.WorldFight, type: Laya.Loader.ATLAS },
	        ];
	        Laya.loader.load(assetArr, Laya.Handler.create(this, this.loadWorldMapCallback));
	    }
	    loadWorldMapCallback() {
	        let viewRect = new Laya.Rectangle(0, 0, Laya.stage.width, Laya.stage.height);
	        let gridSize = new Laya.Point(640, 464);
	        this.worldTileMap.createMap(Common.WorldTileMapJson, viewRect, Laya.Handler.create(this, this.onMapLoaded), null, gridSize);
	    }
	    onMapLoaded() {
	        LogM.debug("onWorldMapLoaded");
	        this.mapLoaded = true;
	        this.worldTileMap.setViewPortPivotByScale(0, 0);
	        this.worldTileMap.scale = Common.DefaultMapScale;
	        this.itemLayer = this.worldTileMap.getLayerByName("wujian");
	        this.mapSprite = this.worldTileMap.mapSprite();
	        this.nMaxMapX = this.worldTileMap.width - Laya.stage.width / this.worldTileMap.scale;
	        this.nMaxMapY = this.worldTileMap.height - Laya.stage.height / this.worldTileMap.scale;
	        this.createContainer();
	        this.createTipSprite();
	        EventManager.Instance.on(EEventType.MapMouseClick, this, this.onMapMouseClick);
	        EventManager.Instance.on(EEventType.MapMouseDown, this, this.onMapMouseDown);
	        if (this.jumpHandler) {
	            this.jumpHandler.run();
	            this.jumpHandler = null;
	        }
	        else {
	            this.jumpTo(PlayerManager.Instance.mainPlayer.x, PlayerManager.Instance.mainPlayer.y);
	        }
	        LobbyManager.Instance.openView(true);
	        WorldLineManager.Instance.gmAddLine();
	    }
	    createContainer() {
	        this.mTipSp = new Laya.Sprite();
	        this.mCitysSp = new Laya.Sprite();
	        this.mCityLabelsSp = new Laya.Sprite();
	        this.mFightLineSp = new Laya.Sprite();
	        this.mDirectionSp = new Laya.Sprite();
	        this.mArmySp = new Laya.Sprite();
	        this.mapSprite.addChild(this.mTipSp);
	        this.mapSprite.addChild(this.mCitysSp);
	        this.mapSprite.addChild(this.mCityLabelsSp);
	        this.mapSprite.addChild(this.mFightLineSp);
	        this.mapSprite.addChild(this.mDirectionSp);
	        this.mapSprite.addChild(this.mArmySp);
	    }
	    createTipSprite() {
	    }
	    createDirectionImage() {
	        this.directionImge = new Laya.Image();
	        this.directionImge.skin = "res/worldTiledMap/direction.png";
	        let label = new Laya.Label();
	        label.x = 42;
	        label.y = 46;
	        label.fontSize = 21;
	        label.rotation = 90;
	        label.pivot(30, 12);
	        label.color = "#ffffff";
	        label.width = 60;
	        label.align = "center";
	        label.valign = "middle";
	        this.directionImge.addChild(label);
	        this.directionImge.on(Laya.Event.CLICK, this, this.clickDirectionImge);
	        this.mDirectionSp.addChild(this.directionImge);
	    }
	    updateContainerLocation() {
	        this.mTipSp.pos(this.worldTileMap.x, this.worldTileMap.y);
	        this.mCitysSp.pos(this.worldTileMap.x, this.worldTileMap.y);
	        this.mCityLabelsSp.pos(this.worldTileMap.x, this.worldTileMap.y);
	        this.mFightLineSp.pos(this.worldTileMap.x, this.worldTileMap.y);
	        this.mDirectionSp.pos(this.worldTileMap.x, this.worldTileMap.y);
	        this.mArmySp.pos(this.worldTileMap.x, this.worldTileMap.y);
	    }
	    get mapX() {
	        return this._mapX;
	    }
	    get mapY() {
	        return this._mapY;
	    }
	    set mapX(mapX) {
	        this._mapX = mapX > this.nMaxMapX ? this.nMaxMapX : mapX;
	        this._mapX = mapX < this.nMinMapX ? this.nMinMapX : mapX;
	    }
	    set mapY(mapY) {
	        this._mapY = mapY > this.nMaxMapY ? this.nMaxMapY : mapY;
	        this._mapY = mapY < this.nMinMapY ? this.nMinMapY : mapY;
	    }
	    jumpTo(logicX, logicY) {
	        if (GameStateManager.Instance.curState == TypeStateGame.World) {
	            let moveViewPos = this.getMoveViewPositionByCenterPos(logicX, logicY);
	            this.setMoveView(moveViewPos.x, moveViewPos.y);
	        }
	        else {
	            this.jumpHandler = Laya.Handler.create(this, this.jumpTo, [logicX, logicY]);
	            GameStateManager.Instance.switchState(TypeStateGame.World);
	        }
	    }
	    setMoveView(viewX, viewY) {
	        this.mapX = viewX;
	        this.mapY = viewY;
	        this.worldTileMap.moveViewPort(this.mapX, this.mapY);
	        this.updateContainerLocation();
	        WorldItemManager.Instance.checkFetchPvpCitys();
	    }
	    getCurCenterBlockPos() {
	        var posResult = new Laya.Point(0, 0);
	        let curLogicCenterPos = this.getLogicPosByStagePos(Laya.stage.width / 2, Laya.stage.height / 2);
	        posResult.x = Math.floor(curLogicCenterPos.x / Common.MapBlockGridWidth);
	        posResult.y = Math.floor(curLogicCenterPos.y / Common.MapBlockGridHeight);
	        return posResult;
	    }
	    getLogicPosByStagePos(stageX, stageY) {
	        var p = new Laya.Point(0, 0);
	        this.itemLayer.getTilePositionByScreenPos(stageX, stageY, p);
	        return TiledUtil.getLogicPosByMapGridPos(p);
	    }
	    getMoveViewPositionByCenterPos(CenterLogicX, CenterLogicY) {
	        let p = this.getItemAnchorPixelPos(CenterLogicX, CenterLogicY);
	        p.x = Math.round(p.x - Laya.stage.width / this.worldTileMap.scale / 2);
	        p.y = Math.round(p.y - Laya.stage.height / this.worldTileMap.scale / 2);
	        return p;
	    }
	    getItemAnchorPixelPos(logicX, logicY, itemSize = 1) {
	        let p = new Laya.Point(0, 0);
	        p.x = (logicX + itemSize) * Common.TileWidth / 2;
	        p.y = (logicY + 1) * Common.TileHeight / 2;
	        return p;
	    }
	    clickDirectionImge() {
	    }
	    onMapMouseClick() {
	        if (this.isMoveClick) {
	            return;
	        }
	        let pos = this.getLogicPosByStagePos(Laya.stage.mouseX, Laya.stage.mouseY);
	        if (WorldItemManager.Instance.clickItem(pos.x, pos.y)) {
	            return;
	        }
	    }
	    onMapMouseDown(e) {
	        this.isMoveClick = false;
	        this.isScaleMap = false;
	        var touches = e.touches;
	        if (touches && touches.length == 2) {
	            this.isScaleMap = true;
	            this.lastDistance = TiledUtil.getEventDistance2(touches);
	        }
	        else if (!touches || touches.length == 1) {
	            this.mLastMouseX = Laya.stage.mouseX;
	            this.mLastMouseY = Laya.stage.mouseY;
	        }
	        else {
	            return;
	        }
	        Laya.stage.on(Laya.Event.MOUSE_UP, this, this.onMouseUp);
	        Laya.stage.on(Laya.Event.MOUSE_OUT, this, this.onMouseUp);
	        Laya.stage.on(Laya.Event.MOUSE_MOVE, this, this.onMouseMove);
	    }
	    onMouseUp() {
	        Laya.stage.off(Laya.Event.MOUSE_MOVE, this, this.onMouseMove);
	        Laya.stage.off(Laya.Event.MOUSE_UP, this, this.onMouseUp);
	        Laya.stage.off(Laya.Event.MOUSE_OUT, this, this.onMouseUp);
	        if (this.isScaleMap) {
	            return;
	        }
	        let moveX = Laya.stage.mouseX - this.mLastMouseX;
	        let moveY = Laya.stage.mouseY - this.mLastMouseY;
	        if (moveX == 0 && moveY == 0) {
	            return;
	        }
	        this.mapX = this.mapX - moveX / this.worldTileMap.scale;
	        this.mapY = this.mapY - moveY / this.worldTileMap.scale;
	    }
	    onMouseMove(e) {
	        if (this.isScaleMap) {
	            let distance = TiledUtil.getEventDistance2(e.touches);
	            const factor = 0.001;
	            let scaleValue = (distance - this.lastDistance) * factor;
	            let resultScale = this.worldTileMap.scale + scaleValue;
	            if (resultScale < Common.MinMapScale) {
	                resultScale = Common.MinMapScale;
	            }
	            else if (resultScale > Common.MaxMapScale) {
	                resultScale = Common.MaxMapScale;
	            }
	            this.worldTileMap.scale = resultScale;
	            this.nMaxMapX = this.worldTileMap.width - Laya.stage.width / this.worldTileMap.scale;
	            this.nMaxMapY = this.worldTileMap.height - Laya.stage.height / this.worldTileMap.scale;
	            this.lastDistance = distance;
	            this.isMoveClick = true;
	        }
	        else {
	            let moveX = (Laya.stage.mouseX - this.mLastMouseX) / this.worldTileMap.scale;
	            let moveY = (Laya.stage.mouseY - this.mLastMouseY) / this.worldTileMap.scale;
	            let tmpMapX = this.mapX - moveX;
	            let tmpMapY = this.mapY - moveY;
	            if (Math.abs(moveX) > 10 || Math.abs(moveY) > 10) {
	                this.isMoveClick = true;
	            }
	            tmpMapX = tmpMapX < this.nMinMapX ? this.nMinMapX : tmpMapX;
	            tmpMapX = tmpMapX > this.nMaxMapX ? this.nMaxMapX : tmpMapX;
	            tmpMapY = tmpMapY < this.nMinMapY ? this.nMinMapY : tmpMapY;
	            tmpMapY = tmpMapY > this.nMaxMapY ? this.nMaxMapY : tmpMapY;
	            this.worldTileMap.moveViewPort(tmpMapX, tmpMapY);
	        }
	        this.updateContainerLocation();
	        WorldItemManager.Instance.checkFetchPvpCitys();
	    }
	}

	class StateGameWorld extends AbsStateGame {
	    enter() {
	        super.enter();
	        this.enterModules();
	        WorldManager.Instance.loadWorldMap();
	    }
	    initOnlyOnce() {
	        this.mods.push(WorldManager.Instance);
	    }
	    exit() {
	        super.exit();
	        this.exitModules();
	    }
	}

	var TypeStateGame;
	(function (TypeStateGame) {
	    TypeStateGame["Init"] = "init";
	    TypeStateGame["Login"] = "login";
	    TypeStateGame["Main"] = "main";
	    TypeStateGame["World"] = "world";
	})(TypeStateGame || (TypeStateGame = {}));
	class GameStateManager {
	    constructor() {
	        this.states = {};
	        this._stateCurrent = null;
	        this._curState = null;
	        this._prevState = null;
	        this._switchingState = null;
	    }
	    static get Instance() {
	        if (!this._instance) {
	            this._instance = new GameStateManager();
	        }
	        return this._instance;
	    }
	    start() {
	        console.log("game start!!!");
	        this.initStates();
	        this.switchState(TypeStateGame.Init);
	    }
	    switchState(type) {
	        if (type == this._switchingState) {
	            return;
	        }
	        if (type == this._curState) {
	            return;
	        }
	        if (type !== TypeStateGame.Init && type !== TypeStateGame.Login) {
	            this.addSwitchAnim(type);
	            return;
	        }
	        this.doSwitchState(type);
	    }
	    addSwitchAnim(type) {
	        this._switchingState = type;
	        SwitchManager.Instance.openView(Laya.Handler.create(this, this.doSwitchState, [type]));
	        return;
	    }
	    doSwitchState(type) {
	        this._switchingState = null;
	        if (type == this._curState) {
	            return;
	        }
	        if (this._stateCurrent) {
	            this._stateCurrent.exit();
	            LogM.info("state:" + this._curState + " exit");
	        }
	        this._prevState = this._curState;
	        this._curState = type;
	        this._stateCurrent = this.states[type];
	        LogM.info("state:" + this._curState + " enter");
	        this._stateCurrent.enter();
	    }
	    initStates() {
	        this.states[TypeStateGame.Init] = new StateGameInit();
	        this.states[TypeStateGame.Login] = new StateGameLogin();
	        this.states[TypeStateGame.Main] = new StateGameMain();
	        this.states[TypeStateGame.World] = new StateGameWorld();
	    }
	    get curState() {
	        return this._curState;
	    }
	    get prevState() {
	        return this._prevState;
	    }
	}
	GameStateManager._instance = null;

	class UI_Lobby extends ui.Panel.Lobby.LobbyUI {
	    constructor() {
	        super();
	        this.btn_chuzheng.on(Laya.Event.CLICK, this, this.onClickChuzheng);
	        this.btn_huicheng.on(Laya.Event.CLICK, this, this.onClickHuicheng);
	        this.btn_search.on(Laya.Event.CLICK, this, this.onClickSearch);
	        this.btn_build.on(Laya.Event.CLICK, this, this.onClickBuild);
	        this.btn_bag.on(Laya.Event.CLICK, this, this.onClickBag);
	        this.btn_mail.on(Laya.Event.CLICK, this, this.onClickMail);
	        this.btn_general.on(Laya.Event.CLICK, this, this.onClickGeneral);
	        this.mapBox.on(Laya.Event.CLICK, this, this.onMapBoxClick);
	        this.mapBox.on(Laya.Event.MOUSE_DOWN, this, this.onMapBoxMouseDown);
	    }
	    onEnable() {
	        this.zOrder = 1;
	        Laya.stage.addChild(this);
	        EventManager.Instance.on(EEventType.LobbyRefreshView, this, this.onLobbyRefreshView);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    onLobbyRefreshView() {
	        this.refreshVisable();
	    }
	    refreshVisable() {
	        let InLobby = GameStateManager.Instance.curState == TypeStateGame.Main;
	        this.btn_search.visible = !InLobby;
	        this.btn_build.visible = InLobby;
	        this.btn_chuzheng.visible = InLobby;
	        this.btn_huicheng.visible = !InLobby;
	    }
	    onMapBoxClick(e) {
	        EventManager.Instance.event(EEventType.MapMouseClick, e);
	        e.stopPropagation();
	    }
	    onMapBoxMouseDown(e) {
	        EventManager.Instance.event(EEventType.MapMouseDown, e);
	        e.stopPropagation();
	    }
	    onClickChuzheng() {
	        GameStateManager.Instance.switchState(TypeStateGame.World);
	    }
	    onClickHuicheng() {
	        GameStateManager.Instance.switchState(TypeStateGame.Main);
	    }
	    onClickSearch() {
	    }
	    onClickBuild() {
	    }
	    onClickBag() {
	        SceneManager.Instance.openScene(ESceneName.UI_Bag);
	    }
	    onClickMail() {
	        SceneManager.Instance.openScene(ESceneName.UI_Mail);
	    }
	    onClickGeneral() {
	        SceneManager.Instance.openScene(ESceneName.UI_General);
	    }
	}

	class UI_RewardPopup extends ui.Panel.Lobby.RewardPopupUI {
	    constructor() {
	        super();
	        this.btn_sure.on(Laya.Event.CLICK, this, this.onSure);
	    }
	    onEnable() {
	        EventManager.Instance.on(ESceneName.UI_RewardPopup, this, this.init);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    init(rewardList) {
	        this.list_rewards.hScrollBarSkin = "";
	        if (rewardList.length == 1) {
	            this.list_rewards.pos(320, 544);
	        }
	        else if (rewardList.length == 2) {
	            this.list_rewards.pos(250, 544);
	        }
	        else if (rewardList.length == 3) {
	            this.list_rewards.pos(177, 544);
	        }
	        else {
	            this.list_rewards.pos(105, 544);
	        }
	        this.list_rewards.init(rewardList);
	    }
	    onSure() {
	        this.close();
	    }
	}

	var ETip;
	(function (ETip) {
	    ETip["MailNoAttchment"] = "\u6CA1\u6709\u9644\u4EF6\u53EF\u4EE5\u9886\u53D6";
	    ETip["MailHaveRewardCannotDel"] = "\u90AE\u4EF6\u4E2D\u6709\u5956\u52B1\u672A\u9886\u53D6\uFF0C\u8BF7\u9886\u53D6\u540E\u5220\u9664";
	    ETip["MailAllIsDel"] = "\u662F\u5426\u5220\u9664\u5F53\u524D\u5217\u8868\u4E2D\u5168\u90E8\u90AE\u4EF6";
	    ETip["MailSaveSuccess"] = "\u5F52\u6863\u6210\u529F";
	})(ETip || (ETip = {}));

	class UI_Mail extends ui.Panel.Mail.MailUI {
	    constructor() {
	        super();
	        this.mgr = MailManager.Instance;
	        this.tab = 0;
	        this.tabs.selectHandler = new Laya.Handler(this, this.onSelectTab);
	        this.btn_batchDel.on(Laya.Event.CLICK, this, this.onBatchDel);
	        this.btn_allRead.on(Laya.Event.CLICK, this, this.onAllRead);
	        this.btn_batchGetAttchment.on(Laya.Event.CLICK, this, this.onBatchGetAttchment);
	    }
	    onEnable() {
	        EventManager.Instance.on(EEventType.MailListUpdate, this, this.onMailListUpdate);
	        this.tabs.selectedIndex = 0;
	    }
	    onDisable() {
	        this.tab = 0;
	        this.tabs.selectedIndex = -1;
	        EventManager.Instance.offAllCaller(this);
	    }
	    onSelectTab(tabIndex) {
	        LogM.debug("onSelectTab", tabIndex);
	        this.tab = tabIndex;
	        this.btn_batchGetAttchment.visible = tabIndex == 0;
	        this.btn_allRead.visible = tabIndex != 3;
	        this.refresh();
	    }
	    refresh() {
	        LogM.debug("Mail Refresh");
	        let mailList = this.mgr.getMailListByTab(this.tab);
	        this.box_noMail.visible = mailList.length === 0;
	        let boxList = this.list_mails;
	        boxList.init(mailList);
	    }
	    onMailListUpdate() {
	        LogM.debug("onMailListUpdate");
	        let mailList = this.mgr.getMailListByTab(this.tab);
	        let mailIdList = this.mgr.getMailIdListByTab(this.tab);
	        this.refresh();
	    }
	    onBatchDel() {
	        let mailList = this.mgr.getMailListByTab(this.tab);
	        let canDel = true;
	        for (let mail of mailList) {
	            if (!mail.is_reward && mail.reward_list.length > 0) {
	                canDel = false;
	                break;
	            }
	        }
	        if (!canDel) {
	            SceneManager.Instance.openAlert(ETip.MailHaveRewardCannotDel);
	            return;
	        }
	        SceneManager.Instance.openAlert(ETip.MailAllIsDel, "确认删除", Laya.Handler.create(this, this.onBatchDelCallback), 1);
	    }
	    onBatchDelCallback() {
	        LogM.debug("onBatchDelCallback");
	        let mailIdList = this.mgr.getMailIdListByTab(this.tab);
	        this.mgr.c_delete_mails(mailIdList);
	    }
	    onAllRead() {
	        let mailIdList = this.mgr.getMailIdListByTab(this.tab);
	        this.mgr.c_read_mails(mailIdList);
	    }
	    onBatchGetAttchment() {
	        if (this.tab !== 0) {
	            return;
	        }
	        let mailList = this.mgr.getMailListByTab(this.tab);
	        let haveAttachment = false;
	        for (let mail of mailList) {
	            if (!mail.is_reward && mail.reward_list.length > 0) {
	                haveAttachment = true;
	                break;
	            }
	        }
	        if (!haveAttachment) {
	            TipManager.Instance.showNormalMsg(ETip.MailNoAttchment);
	            return;
	        }
	        this.mgr.c_get_mail_reward(0);
	    }
	}

	class UI_MailList extends Laya.List {
	    constructor() {
	        super(...arguments);
	        this.openIndex = -1;
	    }
	    onEnable() {
	        this.renderHandler = new Laya.Handler(this, this.onRenderHandler);
	        this.mouseHandler = new Laya.Handler(this, this.onMouseHandler);
	        EventManager.Instance.on(EEventType.MailRead, this, this.onMailRead);
	        EventManager.Instance.on(EEventType.MailPageUpdate, this, this.onMailPageUpdate);
	    }
	    onDisable() {
	        this.array = [];
	        this.openIndex = -1;
	        EventManager.Instance.offAllCaller(this);
	    }
	    init(_datas) {
	        this.vScrollBarSkin = "";
	        this.array = _datas;
	        this.checkOpenMailMessageUpdate();
	    }
	    onRenderHandler(cell, index) {
	        if (index >= this.array.length) {
	            return;
	        }
	        this.renderMailItem(cell, index);
	    }
	    onMouseHandler(e, index) {
	        if (e.type == Laya.Event.CLICK) {
	            let mailItem = this.array[index];
	            this.openMailDetail(mailItem, index);
	        }
	    }
	    openMailDetail(mail, index) {
	        this.openIndex = index;
	        let sceneName;
	        switch (MailConfigManager.Instance.getMailType(mail.mail_template_id)) {
	            case EMailType.SystemMail:
	                sceneName = ESceneName.UI_MailSystem;
	                break;
	            case EMailType.BattleMail:
	                break;
	            case EMailType.GuildMail:
	                break;
	        }
	        SceneManager.Instance.openScene(sceneName, [mail, this.array.length, index]);
	    }
	    checkOpenMailMessageUpdate() {
	        if (this.openIndex == -1)
	            return;
	        if (this.openIndex > this.array.length - 1) {
	            this.openIndex = this.array.length - 1;
	        }
	        let mail = this.array[this.openIndex];
	        EventManager.Instance.event(EEventType.MailMessageUpdate, [mail, this.array.length, this.openIndex]);
	    }
	    onMailRead() {
	        this.refresh();
	    }
	    onMailPageUpdate(openIndex) {
	        this.openIndex = openIndex;
	        this.checkOpenMailMessageUpdate();
	    }
	    renderMailItem(cell, index) {
	        let img_itemBg = cell.getChildByName("img_itemBg");
	        let img_itemAttachment = cell.getChildByName("img_itemAttachment");
	        let img_itemRead = cell.getChildByName("img_itemRead");
	        let img_itemRedpoint = cell.getChildByName("img_itemRedpoint");
	        let label_itemTitle = cell.getChildByName("label_itemTitle");
	        let label_itemContent = cell.getChildByName("label_itemContent");
	        let label_itemTime = cell.getChildByName("label_itemTime");
	        let mailItem = this.array[index];
	        label_itemTitle.text = MailManager.Instance.getMailTitle(mailItem);
	        label_itemContent.text = MailManager.Instance.getMailContent(mailItem);
	        label_itemTime.changeText(Util.secondToDateTime(mailItem.send_time, false));
	        if (mailItem.is_read) {
	            img_itemBg.skin = "image/mail/ui_mail_item_bg_01.png";
	            img_itemRead.visible = true;
	            img_itemRedpoint.visible = false;
	        }
	        else {
	            img_itemBg.skin = "image/mail/ui_mail_item_bg.png";
	            img_itemRead.visible = false;
	            img_itemRedpoint.visible = true;
	        }
	        if (!mailItem.reward_list || mailItem.reward_list.length == 0) {
	            img_itemAttachment.visible = false;
	        }
	        else {
	            img_itemAttachment.visible = true;
	        }
	    }
	}

	class UI_MailScript extends Laya.Script {
	    constructor() {
	        super(...arguments);
	        this.mail = null;
	        this.index = 0;
	        this.box_pages = null;
	        this.box_btns = null;
	        this.btn_del = null;
	        this.btn_getAttachment = null;
	        this.btn_save = null;
	        this.btn_pageLeft = null;
	        this.btn_pageRight = null;
	    }
	    onEnable() {
	        this.box_pages = this.owner.getChildByName("box_pages");
	        this.box_btns = this.owner.getChildByName("box_btns");
	        if (this.box_btns) {
	            this.btn_del = this.box_btns.getChildByName("btn_del");
	            this.btn_getAttachment = this.box_btns.getChildByName("btn_getAttachment");
	            this.btn_save = this.box_btns.getChildByName("btn_save");
	        }
	        if (this.box_pages) {
	            this.btn_pageLeft = this.box_pages.getChildByName("btn_pageLeft");
	            this.btn_pageRight = this.box_pages.getChildByName("btn_pageRight");
	        }
	        this.btn_del && this.btn_del.on(Laya.Event.CLICK, this, this.onDel);
	        this.btn_getAttachment && this.btn_getAttachment.on(Laya.Event.CLICK, this, this.onGetAttachment);
	        this.btn_save && this.btn_save.on(Laya.Event.CLICK, this, this.onSave);
	        this.btn_pageLeft && this.btn_pageLeft.on(Laya.Event.CLICK, this, this.onPageLeft);
	        this.btn_pageRight && this.btn_pageRight.on(Laya.Event.CLICK, this, this.onPageRight);
	        EventManager.Instance.on(EEventType.MailRewardSuccess, this, this.onMailRewardSuccess);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    init(mail, mailNum, index) {
	        if (!mail.is_read) {
	            MailManager.Instance.c_read_mails([Number(mail.mail_id)]);
	        }
	        this.mail = mail;
	        this.index = index;
	        if (this.btn_getAttachment) {
	            this.btn_getAttachment.visible = mail.reward_list && mail.reward_list.length > 0;
	            this.btn_getAttachment.disabled = mail.is_reward;
	            this.btn_getAttachment.label = mail.is_reward ? "已领取" : "领取";
	        }
	        if (this.box_pages) {
	            this.btn_pageLeft.visible = index > 0;
	            this.btn_pageRight.visible = index < mailNum - 1;
	        }
	    }
	    onDel() {
	        if (!this.mail.is_reward && this.mail.reward_list.length > 0) {
	            SceneManager.Instance.openAlert(ETip.MailHaveRewardCannotDel);
	            return;
	        }
	        MailManager.Instance.c_delete_mails([Number(this.mail.mail_id)]);
	    }
	    onGetAttachment() {
	        if (this.mail.is_reward || !this.mail.reward_list || this.mail.reward_list.length == 0) {
	            return;
	        }
	        MailManager.Instance.c_get_mail_reward(Number(this.mail.mail_id));
	    }
	    onSave() {
	        MailManager.Instance.c_save_mail(Number(this.mail.mail_id));
	        TipManager.Instance.showNormalMsg(ETip.MailSaveSuccess);
	    }
	    onPageLeft() {
	        EventManager.Instance.event(EEventType.MailPageUpdate, [this.index - 1]);
	    }
	    onPageRight() {
	        EventManager.Instance.event(EEventType.MailPageUpdate, [this.index + 1]);
	    }
	    onMailRewardSuccess(mailId) {
	        if (this.mail.mail_id != mailId) {
	            return;
	        }
	        this.btn_getAttachment.label = "已领取";
	        this.btn_getAttachment.disabled = this.mail.is_reward;
	    }
	}

	class UI_MailSystem extends ui.Panel.Mail.MailSystemUI {
	    constructor() {
	        super();
	    }
	    onEnable() {
	        EventManager.Instance.on(ESceneName.UI_MailSystem, this, this.init);
	        EventManager.Instance.on(EEventType.MailMessageUpdate, this, this.init);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    init(mail, mailNum, index) {
	        if (mailNum == 0) {
	            this.close();
	            return;
	        }
	        this.list_rewardList.init(mail.reward_list, true);
	        this.getComponent(UI_MailScript).init(mail, mailNum, index);
	        this.label_mailTitle.text = MailManager.Instance.getMailTitle(mail);
	        this.label_mailContent.text = MailManager.Instance.getMailContent(mail);
	        this.label_pageIndex.text = (index + 1) + "/" + mailNum;
	        this.label_mailTime.text = Util.secondToDateTime(mail.send_time, false);
	    }
	}

	class UI_Alert extends ui.Panel.Tip.AlertUI {
	    constructor() {
	        super();
	        this.handlerYes = null;
	        this.handlerNo = null;
	        this.loginNoTip = false;
	        this.btn_yes.on(Laya.Event.CLICK, this, this.onYes);
	        this.btn_yes1.on(Laya.Event.CLICK, this, this.onYes);
	        this.btn_no.on(Laya.Event.CLICK, this, this.onNo);
	        this.btn_close.on(Laya.Event.CLICK, this, this.onNo);
	        this.btn_tip.on(Laya.Event.CLICK, this, this.onTip);
	    }
	    onEnable() {
	        EventManager.Instance.on(ESceneName.UI_Alert, this, this.init);
	    }
	    onDisable() {
	        this.handlerYes = null;
	        this.handlerNo = null;
	        this.loginNoTip = false;
	        EventManager.Instance.offAllCaller(this);
	    }
	    init(title, text, handlerYes, type, showTip, handlerNo, yes, no) {
	        this.label_title.text = title;
	        this.label_text.text = text;
	        this.handlerYes = handlerYes;
	        this.handlerNo = handlerNo;
	        if (type == 0) {
	            this.btn_no.visible = false;
	            this.btn_yes.visible = false;
	            this.btn_yes1.visible = true;
	        }
	        else {
	            this.btn_no.visible = true;
	            this.btn_yes.visible = true;
	            this.btn_yes1.visible = false;
	        }
	        this.height = 428;
	        if (!showTip) {
	            this.height = 380;
	        }
	        this.btn_tip.visible = showTip;
	        this.btn_yes.label = yes;
	        this.btn_yes1.label = yes;
	        this.btn_no.label = no;
	        this.img_gou.visible = false;
	    }
	    onYes() {
	        this.handlerYes && this.handlerYes.runWith(this.loginNoTip);
	        this.close();
	    }
	    onNo() {
	        this.handlerNo && this.handlerNo.run();
	        this.close();
	    }
	    onTip() {
	        this.img_gou.visible = !this.img_gou.visible;
	        this.loginNoTip = this.img_gou.visible;
	    }
	}

	class UI_Tip extends ui.Panel.Tip.TipUI {
	    constructor() {
	        super();
	        this.mStartY = 0;
	        this.zOrder = 1;
	    }
	    onEnable() {
	        this.mStartY = this.tips_1.y;
	        Laya.timer.clearAll(this);
	        this.doStartMsg();
	    }
	    onDisable() {
	    }
	    doStartMsg() {
	        let text = TipManager.Instance.cacheNormalMsg.pop();
	        if (!text) {
	            this.close();
	            return;
	        }
	        this.tips_1.text = text;
	        this.tips_1.y = this.mStartY - 100;
	        this.tips_1.alpha = 0.3;
	        this.tips_1.visible = true;
	        Laya.Tween.to(this.tips_1, { y: this.mStartY, alpha: 1.0 }, 200, Laya.Ease.circOut, Laya.Handler.create(this, this.doStartMsgComplete), 0);
	    }
	    doStartMsgComplete() {
	        if (TipManager.Instance.cacheNormalMsg.length == 0) {
	            Laya.timer.once(2000, this, this.checkClose);
	            return;
	        }
	        this.doStartMsg();
	    }
	    checkClose() {
	        if (TipManager.Instance.cacheNormalMsg.length == 0) {
	            this.close();
	            return;
	        }
	        this.doStartMsg();
	    }
	}

	class UI_WorldMonster extends ui.Panel.World.WorldMonsterUI {
	    constructor() {
	        super();
	        this.logicX = 0;
	        this.logicY = 0;
	        this._lv = 0;
	        this.type = 0;
	        this.isPopupCenter = false;
	        this.btn_attack.on(Laya.Event.CLICK, this, this.onAttack);
	        this.btn_share.on(Laya.Event.CLICK, this, this.onShare);
	        this.btn_save.on(Laya.Event.CLICK, this, this.onSave);
	    }
	    onEnable() {
	        EventManager.Instance.on(ESceneName.UI_WorldMonster, this, this.init);
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	    set lv(lv) {
	        if (this.isNormalMon(this.type)) {
	            this._lv = lv + PlayerManager.Instance.mainPlayer.lv_mon_refresh;
	            this._lv = this._lv < MonsterMinLv ? MonsterMinLv : this._lv;
	            this._lv = this._lv > MonsterMaxLv ? MonsterMaxLv : this._lv;
	        }
	        else {
	            this._lv = lv;
	        }
	    }
	    get lv() {
	        return this._lv;
	    }
	    init(item) {
	        if (!item || (!this.isNormalMon(item.vo.type) && item.vo.type != EWorldCityType.EliteMonster)) {
	            this.close();
	            return;
	        }
	        let vo = item.vo;
	        this.type = vo.type;
	        this.lv = vo.lv;
	        this.logicX = vo.x;
	        this.logicY = vo.y;
	        let baseCityInfo = WorldConfigManager.Instance.getBaseCityTypeInfo(this.type);
	        this.img_icon.skin = baseCityInfo.icon;
	        this.label_name.text = baseCityInfo.name;
	        this.label_lv.text = this.lv + "";
	        this.label_power.text = WorldConfigManager.Instance.getPvpArmyPower(this.type, this.lv) + "";
	        this.label_time.text = "00:00:20";
	        let listRewardGoods = [];
	        for (let goodsBaseId of WorldConfigManager.Instance.getPvpArmyRewards(this.type, this.lv)) {
	            listRewardGoods.push({ goods_base_id: goodsBaseId });
	        }
	        this.list_reward.init(listRewardGoods);
	    }
	    isNormalMon(type) {
	        return type == EWorldCityType.Monster1 || type == EWorldCityType.Monster2 || type == EWorldCityType.Monster3;
	    }
	    onAttack() {
	    }
	    onShare() {
	    }
	    onSave() {
	    }
	}

	class UI_WorldPlayerCity extends ui.Panel.World.WorldPlayerCityUI {
	    constructor() {
	        super();
	        this.isPopupCenter = false;
	        this.isShowEffect = false;
	    }
	    onEnable() {
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	}

	class UI_WorldRobCity extends ui.Panel.World.WorldRobCityUI {
	    constructor() {
	        super();
	        this.isPopupCenter = false;
	        this.isShowEffect = false;
	    }
	    onEnable() {
	    }
	    onDisable() {
	        EventManager.Instance.offAllCaller(this);
	    }
	}

	class UI_BoxSoldier extends Laya.Box {
	    constructor() {
	        super(...arguments);
	        this.soldier = null;
	        this.mBattleHpPrefab = null;
	        this.img_soldier = null;
	        this.img_soldierNumBg = null;
	        this.pr_hp = null;
	        this.label_soldierNum = null;
	        this.label_hp = null;
	        this.curHP = 0;
	        this.curNum = 0;
	    }
	    onEnable() {
	        this.img_soldier = this.getChildByName("img_soldier");
	        this.img_soldierNumBg = this.getChildByName("img_soldierNumBg");
	        this.pr_hp = this.getChildByName("pr_hp");
	        this.label_soldierNum = this.getChildByName("label_soldierNum");
	        this.label_hp = this.getChildByName("label_hp");
	    }
	    onDisable() {
	        this.soldier = null;
	    }
	    recover() {
	        this.removeSelf();
	        Laya.Pool.recover(EPrefabPath.BoxSoldier, this);
	    }
	    init(soldier, side, mBattleHpPrefab) {
	        this.soldier = soldier;
	        this.side = side;
	        this.mBattleHpPrefab = mBattleHpPrefab;
	        let baseSoldier = SoldierConfigManager.Instance.getBaseSoldier(soldier.soldier_base_id);
	        this.curHP = Number(soldier.hp_lim);
	        this.curNum = soldier.num;
	        if (side == EBattleSide.Left) {
	            this.img_soldier.skin = baseSoldier.icon;
	            this.img_soldierNumBg.skin = "image/battle/ui_soldier_num_blue_bg.png";
	            this.pr_hp.skin = "image/comp/pr_green.png";
	        }
	        else {
	            this.img_soldier.skin = baseSoldier.def_icon;
	            this.img_soldierNumBg.skin = "image/battle/ui_soldier_num_red_bg.png";
	            this.pr_hp.skin = "image/comp/pr_red.png";
	        }
	        this.refresh();
	        let pos = BattleSidePosMap["pos_" + side + "_" + soldier.pos];
	        this.x = pos.x;
	        this.y = pos.y;
	    }
	    refresh() {
	        this.pr_hp.value = this.curHP / Number(this.soldier.hp_lim);
	        this.label_soldierNum.text = this.curNum + "/" + this.soldier.num;
	        this.label_hp.text = this.curHP + "";
	    }
	    shot(mPlayMulti) {
	        let curX = this.x;
	        let curY = this.y;
	        let muti = this.side == EBattleSide.Left ? -1 : 1;
	        let aniTime = BattleAniDuration / mPlayMulti;
	        Laya.Tween.to(this, { x: curX - 50 * muti, y: curY + 50 * muti }, aniTime, Laya.Ease.quadInOut);
	        Laya.Tween.to(this, { x: curX, y: curY }, aniTime, Laya.Ease.quadInOut, null, aniTime);
	    }
	    hurt(hurtInfo, shotObj, mPlayMulti) {
	        if (hurtInfo.num == 0) {
	            this.recover();
	            return;
	        }
	        this.curNum = hurtInfo.num;
	        this.curHP = Number(hurtInfo.hp);
	        let curX = this.x;
	        let muti = this.side == EBattleSide.Left ? -1 : 1;
	        let aniTime = BattleAniDuration / mPlayMulti;
	        Laya.Tween.to(this, { x: curX + 50 * muti }, aniTime, Laya.Ease.quadInOut);
	        Laya.Tween.to(this, { x: curX }, aniTime, Laya.Ease.quadInOut, null, aniTime);
	        let prefab = Laya.Pool.getItemByCreateFun(EPrefabPath.BattleHp, this.mBattleHpPrefab.create, this.mBattleHpPrefab);
	        prefab.text = "-" + hurtInfo.hurt;
	        prefab.x = this.label_hp.x;
	        prefab.y = this.label_hp.y;
	        this.addChild(prefab);
	        Laya.Tween.to(prefab, { y: this.label_hp.y - 50 }, 500, Laya.Ease.quadInOut, Laya.Handler.create(this, () => {
	            prefab.removeSelf();
	            Laya.Pool.recover(EPrefabPath.BattleHp, prefab);
	        }));
	        this.refresh();
	    }
	}

	class GameConfig {
	    constructor() {
	    }
	    static init() {
	        var reg = Laya.ClassUtils.regClass;
	        reg("Module/Bag/UI_Bag.ts", UI_Bag);
	        reg("Module/Script/UI_Close.ts", UI_Close);
	        reg("Module/Bag/UI_BagItemList.ts", UI_BagItemList);
	        reg("Module/Script/UI_Slider.ts", UI_Slider);
	        reg("Module/Bag/UI_BagDel.ts", UI_BagDel);
	        reg("Module/Bag/UI_GoodsDesc.ts", UI_GoodsDesc);
	        reg("Module/Battle/UI_Battle.ts", UI_Battle);
	        reg("Module/Script/UI_DeployList.ts", UI_DeployList);
	        reg("Module/Script/UI_RewardList.ts", UI_RewardList);
	        reg("Module/General/UI_General.ts", UI_General);
	        reg("Module/General/UI_GeneralList.ts", UI_GeneralList);
	        reg("Module/Login/UI_LoginLoading.ts", UI_LoginLoading);
	        reg("Module/Lobby/UI_Lobby.ts", UI_Lobby);
	        reg("Module/Lobby/UI_RewardPopup.ts", UI_RewardPopup);
	        reg("Module/Mail/UI_Mail.ts", UI_Mail);
	        reg("Module/Mail/UI_MailList.ts", UI_MailList);
	        reg("Module/Mail/UI_MailSystem.ts", UI_MailSystem);
	        reg("Module/Mail/UI_MailScript.ts", UI_MailScript);
	        reg("Module/Tip/UI_Alert.ts", UI_Alert);
	        reg("Module/Tip/UI_Tip.ts", UI_Tip);
	        reg("Module/World/UI_WorldMonster.ts", UI_WorldMonster);
	        reg("Module/World/UI_WorldPlayerCity.ts", UI_WorldPlayerCity);
	        reg("Module/World/UI_WorldRobCity.ts", UI_WorldRobCity);
	        reg("Module/Battle/UI_BoxSoldier.ts", UI_BoxSoldier);
	    }
	}
	GameConfig.width = 750;
	GameConfig.height = 1334;
	GameConfig.scaleMode = "fixedwidth";
	GameConfig.screenMode = "vertical";
	GameConfig.alignV = "middle";
	GameConfig.alignH = "center";
	GameConfig.startScene = "Panel/World/WorldMonster.scene";
	GameConfig.sceneRoot = "";
	GameConfig.debug = false;
	GameConfig.stat = true;
	GameConfig.physicsDebug = false;
	GameConfig.exportSceneToJson = true;
	GameConfig.init();

	class Main {
	    constructor() {
	        if (window["Laya3D"])
	            Laya3D.init(GameConfig.width, GameConfig.height);
	        else
	            Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
	        Laya["Physics"] && Laya["Physics"].enable();
	        Laya["DebugPanel"] && Laya["DebugPanel"].enable();
	        Laya.stage.scaleMode = GameConfig.scaleMode;
	        Laya.stage.screenMode = GameConfig.screenMode;
	        Laya.stage.alignV = GameConfig.alignV;
	        Laya.stage.alignH = GameConfig.alignH;
	        Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
	        if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
	            Laya.enableDebugPanel();
	        if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
	            Laya["PhysicsDebugDraw"].enable();
	        if (GameConfig.stat)
	            Laya.Stat.show();
	        Laya.alertGlobalError = true;
	        Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
	    }
	    onVersionLoaded() {
	        Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
	    }
	    onConfigLoaded() {
	        GameStateManager.Instance.start();
	        UIConfig.popupBgAlpha = 0;
	    }
	}
	new Main();

}());
